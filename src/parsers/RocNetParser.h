/*
 * RocNetParser.h
 *
 *  Created on: 10/2/2015
 *      Author: mserrano
 */

//Frozen due to bugs in rocnet protocol definition:
//http://forum.rocrail.net/viewtopic.php?t=9693&p=91338

//the solution is to use the locoDepot to get protocols and use them instead of the protocol received

#ifndef ROCNETPARSER_H_
#define ROCNETPARSER_H_

#include <arpa/inet.h>



#include "CommandParser.h"

class RocNetParser: public CommandParser {
public:
	RocNetParser();
	virtual ~RocNetParser();
	virtual void openConnection();
private:
	struct sockaddr_in addr;
	int rocnetsock;
	void connection_handler();
};


#endif /* ROCNETPARSER_H_ */
