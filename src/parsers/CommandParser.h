/*
 * CommandParser.h
 *
 *  Created on: 10/2/2015
 *      Author: mserrano
 */

#ifndef COMMANDPARSER_H_
#define COMMANDPARSER_H_

#include <string>
#include <memory>

using namespace std;

class CommandParser {

private:
public:
//	static string type;
//	static long port;
//	static int maxclients;

	string type;
	//long port;
	unsigned int port;
	int maxclients;
	string locopath;
//	CommandParser *next;	//To implement a queue
	std::shared_ptr<CommandParser> next;

	CommandParser();
	virtual ~CommandParser();
	virtual void openConnection();
};

#endif /* COMMANDPARSER_H_ */
