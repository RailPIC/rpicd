/*
 * mcs2.cpp
 *
 *  Created on: 28/03/2016
 *      Author: agonia
 */

#include "mcs2Parser.h"

#include "CommandParser.h"
#include "../Logger.h"
#include "../Config.h"
#include "../LocoDepot.h"
#include "../RocClient.h"
#include "../RpicdGateway.h"
#include "../rpic/commands.h"
#include "../Rpi.h"
extern "C" {
#include "../errors.h"
}
#include <iostream>
#include <unistd.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <thread>         // std::thread

#include <stdio.h>
#include <stdlib.h>

#include "../xml/tinyxml.h"

#ifdef WINDOWS
    #include <direct.h>
    #define GetCurrentDir _getcwd

	#include "tools/fakewiringPi.h"
#else
    #include <unistd.h>
    #define GetCurrentDir getcwd
#endif

using namespace std;

#define BUFFSIZE_MCS2 20


mcs2Parser::mcs2Parser() {

	//DEFAULT VALUES
	maxclients= MAXCLIENTS;
	port=MCS2PORTrx;
	portTX=MCS2PORTtx;
	locopath="";
	minutesActive=0;
	minutesLastStatus=0;
}

mcs2Parser::~mcs2Parser() {
	// TODO Auto-generated destructor stub
}

void mcs2Parser::openConnection(){

	//This is the UDP connection originally working with mcs2Parser.
	//The repository includes a non concluded development (branch devTCP) to migrate to TCP.
	//The development was not completed sine I found a nice way to make the UDP connection work with broadcast messages
	//TCP connection in addition seesm to send more than one CAN message per packet... which the original development in UDP
	//is not supporting. There is the docubt if by adpating the packet reception to analyse each packet and look for more than
	//one CAN message may lead to broken communications in case of a message being lost of mistreated (the client may be waiting for a response that will never arrvies
	//This was observed during tests. The wrong responses being sent was causing clients to not syncronise the expected mesage with the recieved ones
	//Example: if a packet with two CAN messages was received, mcs2parse was only responing to the first one. Client was expecting response to both... with different lenghts in the response.
	//Comunicaation was broken with a decalage in the bytes received.

	bool connectionREADY=false;
	//Delay hard coded
	int delaywindowus=100000; //100ms
	int maxtry=600; //60 sec
	int tries=0;

	//char cCurrentPath[FILENAME_MAX];

	portTX=port-1;

	Logger::insertLog("MCS2PARSER: PortTX "+std::to_string(portTX));
	Logger::insertLog("MCS2PARSER: Port "+std::to_string(port));


	//Logger::insertLog("**********************MCS2 Parser***********************");
	Logger::insertLog("MCS2PARSER: Init MCS2PARSER");

    if (locopath.compare("")){
    	//GetCurrentDir(cCurrentPath, sizeof(cCurrentPath));
    	Logger::insertLog("MCS2PARSER: Parsing locopath: "+locopath);
    	//do something to recover locopath
    	//GetCurrentDir(locopath, sizeof(locopath));
    	//TiXmlDocument doc(CONFIGFILE);
    	TiXmlDocument doc(locopath.c_str());
    	//http://www.grinninglizard.com/tinyxmldocs/tutorial0.html

    	TiXmlHandle hDoc(&doc);
    	TiXmlHandle hRoot(0);
    	if (!doc.LoadFile()) {exit(EXIT_FAILURE);}


    	TiXmlElement *msg;

    	//msg=hDoc.FirstChildElement("lclist").Element()->FirstChildElement( "lclist" );
    	//msg=hDoc.FirstChildElement("lclist").Element();
    	//msg=hDoc.FirstChildElement().Element();
    	msg=hDoc.FirstChildElement().Element()->FirstChildElement("lclist");
  //  	TiXmlPrinter printer;
  //  	msg->Accept( &printer );
  //  	std::string stringBuffer = printer.CStr();
  //  	cout<<stringBuffer.c_str()<<endl;
    	RocClient::lclistCMD(msg);

    	msg=hDoc.FirstChildElement().Element()->FirstChildElement("carlist");
    	//TiXmlPrinter printer;
  //  	msg->Accept( &printer );
 //   	stringBuffer = printer.CStr();
  //  	cout<<stringBuffer.c_str()<<endl;
    	RocClient::carlistCMD(msg);


    }

    if (!(LocoDepot::init)){
    	Logger::insertLog("MCS2PARSER-WARNING: MCS2 Parser requires a LocoDepot plan");
    	//exit(EXIT_FAILURE);
    }

    //To make easier the access to a locoDepot, I could create a map table mapping address+protocol to ID.
    //other option is look at loco plan each time with a new fucntion findloco(int address, int protool).. and store in each loco the family protocl too
    //The advantage is that if the locodepot is updated, the loco used is also updated...
    //
    //mapIDGen();

	struct ip_mreq mreq;

     //u_int yes=1;            /*** MODIFICATION TO ORIGINAL */
      int yes=1;

     /* create what looks like an ordinary UDP socket */
     if ((mcs2sock=socket(AF_INET,SOCK_DGRAM,0)) < 0) {
     	Logger::insertLog("MCS2PARSER-ERROR: Not possible to create a socket");
     	exit(EXIT_FAILURE);
     }

     if ((mcs2sockTX=socket(AF_INET,SOCK_DGRAM,0)) < 0) {
     	Logger::insertLog("MCS2PARSER-ERROR: Not possible to create a socket for TX");
     	exit(EXIT_FAILURE);
     }


/**** MODIFICATION TO ORIGINAL */
    /* allow multiple sockets to use the same PORT number */
    if (setsockopt(mcs2sock,SOL_SOCKET,SO_REUSEADDR,&yes,sizeof(yes)) < 0) {
    	Logger::insertLog("MCS2PARSER-ERROR: Not possible to reuse port");
    	exit(EXIT_FAILURE);
       }

    if (setsockopt(mcs2sockTX,SOL_SOCKET,SO_REUSEADDR,&yes,sizeof(yes)) < 0) {
    	Logger::insertLog("MCS2PARSER-ERROR: Not possible to reuse portTX");
    	exit(EXIT_FAILURE);
       }

    /*** END OF MODIFICATION TO ORIGINAL */

     /* set up destination address */
     memset(&addr,0,sizeof(addr));
     addr.sin_family=AF_INET;
     addr.sin_addr.s_addr=htonl(INADDR_ANY); /* N.B.: differs from sender */
     addr.sin_port=htons(port);

     memset(&addrTX,0,sizeof(addrTX));
     addrTX.sin_family=AF_INET;
  //   addrTX.sin_addr.s_addr=inet_addr("127.0.0.1");  //To send only messages to localhost
     addrTX.sin_addr.s_addr=inet_addr("224.0.0.1");
     addrTX.sin_port=htons(portTX);
     //It seems Linux is well registered in multicast group (224.0.0.1.. I have not being able to make it work with windows
     //To check the multicas group is well registered it is possible to activate the pings to multicast and check with ping:
     //sysctl net.ipv4.icmp_echo_ignore_broadcasts=0
     //With sysctl net.ipv4.icmp_echo_ignore_broadcasts it is possible to see the current policy
     //in docker it is necessary to run the container in privileged mode
     //Note soem routers are blocking this traffic.. though in most cases it is blocked to outside network.
     //The traffic is naturally only possiuble in the active subnet

     /* bind to receive address */
     if (bind(mcs2sock,(struct sockaddr *) &addr,sizeof(addr)) < 0) {
		Logger::insertLog("MCS2PARSER-ERROR: Not possible to bind socket");
		exit(EXIT_FAILURE);
	 }

     /* use setsockopt() to request that the kernel join a multicast group */
     mreq.imr_multiaddr.s_addr=inet_addr("224.0.0.1");
     mreq.imr_interface.s_addr=htonl(INADDR_ANY);
/* ORIGINAL
     if (setsockopt(mcs2sock,IPPROTO_IP,IP_ADD_MEMBERSHIP,&mreq,sizeof(mreq)) < 0) {
 		Logger::insertLog("ERROR: Not possible to set socket");
 		exit(EXIT_FAILURE);
     }
*/
	 while ((tries<maxtry)&&(!connectionREADY)){
	     if (setsockopt(mcs2sock,IPPROTO_IP,IP_ADD_MEMBERSHIP,&mreq,sizeof(mreq)) < 0) {
			 	 usleep(delaywindowus);
			 	 tries++;
			 	 //Logger::insertLog("ERROR: Failed to connect with server");
		 	     //exit(EXIT_FAILURE);
		 }else{
			 connectionREADY=true;
		 }
	 }

	 if (connectionREADY){
		 std::thread mcs2thread (&mcs2Parser::connection_handler,this);
		 mcs2thread.detach();
	 }else{
  		 Logger::insertLog("MCS2PARSER-ERROR: Not possible to set socket");
		 exit(EXIT_FAILURE);
	 }


     //http://stackoverflow.com/questions/13888453/stdthread-unresolved-overloaded-function-type-error
/*ORIGINAL
	 std::thread rocnetthread (&mcs2Parser::connection_handler,this);
	 rocnetthread.detach();
*/

}
/*
void mcs2Parser::mapIDGen(){
	Loco *lc;


}
*/
void mcs2Parser::connection_handler() {
    int addrlen, nbytes, i, j, result;
    unsigned char buffer[BUFFSIZE_MCS2];

	/* now just enter a read-print loop */

	Logger::insertLog("MCS2PARSER: MCS2 Connection ready to receive data");


//	Rpi::setpicready();		//Needed to retrieve specific info for mcs2parser
	//result = RpicdGateway::sendconfcommand(ZENTRAL_READ, NULL);
	result = RpicdGateway::sendconfcommand(ZENTRAL_READ);
	if (result == OK) {
//#ifdef __arm__
#if defined(__arm__) || defined(__aarch64__)
		unsigned char *UID = RpicdGateway::getresponse();
#else
		//Testing purposes when no actual connection to rpic
		unsigned char tempo[5];
		tempo[0]=0xFF; //checksu ficticio
		tempo[1]='R';
		tempo[2]='P';
		tempo[3]='I';
		tempo[4]='C';
		unsigned char *UID = tempo;
#endif
		char hex[3]; //2+null character by sprintf
		string hexmessage = "";
		for (i=0;i<4;i++){
			zentralUID[i]=*(UID+i+1); //UID0 is checksum
			sprintf(hex, "%.2X", zentralUID[i]);
			hexmessage = hexmessage + hex + " ";

		}
		Logger::insertLog("MCS2PARSER: getUID sent OK. UID received: "+hexmessage);

		//Not sure if the following is OK or zentral bytes should be inverted: z0z1z2z3 vsz3z2z1z0
		storedHash[0]  = zentralUID[3]^zentralUID[1];
		storedHash[1]  = zentralUID[2]^zentralUID[0];
		storedHash[0] |= 0x03;	//0bxxxxxx11
		storedHash[1] &= 0x7F;	//0b0xxxxxxx
		zUID = (zentralUID[0] << 24) + (zentralUID[1] << 16) + (zentralUID[2] << 8) + zentralUID[3];
		//zUID = (zentralUID[3] << 24) + (zentralUID[2] << 16) + (zentralUID[1] << 8) + zentralUID[0];
		printf("UID: %lu \n",zUID);
	}
	//Not possible here.. PIC is reset after
	//Logger::insertLog("MCS2PARSER: Checking for MMX locos and init RPIC queue");
	//LocoDepot::initMMX();

	//Under ceratin cricunstances... RPI maybe alreay ready, and it is not desireable to get it off. In any case, the final
	//outcome of the configuration process is to set the rpicreayd
	//Rpi::setpiciddle();
	j=0;
	while (true) {


     addrlen=sizeof(addr);
	  if ((nbytes=recvfrom(mcs2sock,buffer,BUFFSIZE_MCS2,0,(struct sockaddr *) &addr,(socklen_t*)&addrlen)) < 0) {
			Logger::insertLog("MCS2PARSER-ERROR: Error receiving data");
			exit(EXIT_FAILURE);
	  }
/*
	  printf("Data IN %i:",j);
	  for (i=0;i<nbytes;i++){
		  printf("%X ",buffer[i]);
	  }
	  printf("\n");
	  */
	  j++;

	  //if (message4me(buffer)) parseMgr(buffer);
	  //All messages need in principle to be parsed, as loco commands or accesory commands need to be process
	  parseMgr(buffer);

    }

}

//int mcs2Parser::sendResponse(unsigned char *msg, unsigned char length){

int mcs2Parser::sendResponse(unsigned char prio, unsigned char cmd, unsigned char rsp, unsigned char* data, unsigned char length, unsigned int hash){
	//to be used to sedn info back to external world (not to pic)
	//Response is sent thorugh the TX socket.

	  int i = 0;
	  unsigned char msg[BUFFSIZE_MCS2];
//Current Format of the header is working. Commented old heading (inspired by msgbos library at Rocrail
	  msg[0]  = (prio << 1);
	//  msg[0] |= (cmd >> 7);
	 // msg[1]  = ((cmd & 0x7F) << 1 );
	  msg[1]  = cmd;
	  msg[1] |= rsp;
	//Hash calculated according to CS2CAN protocol manual
	  if (hash==NOHASH){   //
//		  msg[2]  = zentralUID[3]^zentralUID[1];
//		  msg[3]  = zentralUID[2]^zentralUID[0];
//		  msg[2] |= 0x03;	//0bxxxxxx11
//		  msg[3] &= 0x7F;	//0b0xxxxxxx
		  msg[2]=storedHash[0];
		  msg[3]=storedHash[1];
	  }else{
		  msg[2]=byte_of(hash,1);
		  msg[3]=byte_of(hash,0);
	  }

	  msg[4]  = length;
	  for(i = 0; i < length; i++ )
	    msg[5+i]  = data[i];

	int addrlen;
	addrlen=sizeof(addrTX);


	if (sendto(mcs2sockTX,msg,length+5,0,(struct sockaddr *) &addrTX,(socklen_t)addrlen)==-1) {
		Logger::insertLog("MCS2PARSER-ERROR sending data");
				exit(EXIT_FAILURE);
		  }
/*
	printf("Data OUT:");
	 for (i=0;i<length+5;i++){
	  printf("%X ",msg[i]);
	 }
	printf("\n");
**/
return OK;

}

bool mcs2Parser::message4me(unsigned char* msg){
	unsigned long destination=(msg[5] << 24) + (msg[6] << 16) + (msg[7] << 8) + msg[8];
	//printf("destination %lu \n",destination);
	if ((destination==zUID) || (destination==0)) return true;
	else return false;
}

void mcs2Parser::parseMgr(unsigned char *msg) {

	int result=OK;
	char bufhex[2];

	sprintf(bufhex, "%X", msg[1]);

	//I am not treating priotity nor neching integreity

	switch ( msg[1] ) {
	case ID_SYSTEM:
		  result=sysCMD(msg);
		  break;
	case ID_LOCO_DIRECTION:
		result=dirCMD(msg);
		break;
	case ID_LOCO_VELOCITY:
		result=speedCMD(msg);
		break;
	  break;
	case ID_LOCO_FUNCTION:
		result=fnCMD(msg);
		break;
	case ID_ACC_SWITCH:
		result=accCMD(msg);
		break;
	case ID_LOCO_READ_CONFIG:
		result=readCMD(msg);
		break;
	case ID_LOCO_WRITE_CONFIG:
		result=writeCMD(msg);
		break;
	case ID_LOCO_DISCOVERY:
		result=lok_discoveryCMD(msg);
		break;
	case CAN_ID_PING:{
		unsigned char data[8];
		for (int i=0;i<4;i++)
			data[i]=zentralUID[i];

		string str=VERSION;
		int point=str.find(".");
		data[4]=stoi(str.substr(0,point));
		data[5]=stoi(str.substr(point+1));
		data[6]=0x00;	//Hard coded a gelisbox ID
		data[7]=0x10;
//		data[6]=0xFF;	//Hard coded a CS2
//		data[7]=0xFF;

		result=sendResponse(0,CAN_ID_PING,BIT_RESPONSE,data,8);
		Logger::insertLog("MCS2PARSER: PING CMD received. Response sent", PING_LOG);
		result=OK;
		break;}
	case ID_CAN_BOOT_BOUND:
		Logger::insertLog("MCS2PARSER: BOOTLOADER CMD Not implemented. Not even response sent");
		//There is no documentation on what to response. Same as PING?
		break;
	case ID_SYS_STAT_DATA:{
		if (message4me(msg)){
			unsigned char index=msg[9];
			result=sendStatusConfig(index);
			Logger::insertLog("MCS2PARSER: SYS_STATUS DATA CMD received. Response sent");
		}
		break;}
	default:
		Logger::insertLog("MCS2PARSER: ["+string(bufhex)+"] is a CMD not recognised");
	  break;
	}

	if (result!=OK) Logger::insertLog("MCS2PARSER: Problem parsing ["+string(bufhex)+"] CMD. "+errorHandler(result));
//No control on return;
}

int mcs2Parser::sysCMD (unsigned char *msg){
	int command=EMPTY; //EMPRY is a code of locos 255... not used by command (4 bits, up to 15)
	string message="";
	int logflag=NOFLAG_LOG;
	char bufhex[2];
	//int result;

	unsigned int response=RPICDGATEWAYERROR;
	sprintf(bufhex, "%X", msg[9]);

	unsigned char data[BUFFSIZE_MCS2];
	for (int i=0;i<4;i++)
		data[i]=zentralUID[i];
	//Not all system messages require zentraUID... Marked hereafter the ones require further work:
	//-CMD_SYSSUB_EMBREAK requires loco address received
	//-CMD_SYSSUB_STOPCYCLE requires loco address received
	//.CMD_SYSSUB_LOCOPROT requires loco address received
	data[4]=msg[9];
	data[5]=msg[10];	//Potentially not needed by some commands
	data[6]=msg[11];
	data[7]=msg[12];

	unsigned char sys_message[MAXBYTES];
	string protos;
	unsigned char resp_length=0;

	string ID;
	int addrL,addrH, addr, offset, protf;
	Loco *lc;

	unsigned char sys_conf=SYSTEMCMD;

	switch ( msg[9] ) {
		case CMD_SYSSUB_GO:
			  if (message4me(msg))
			  {
				  command=CYCLEON;
				  message="Power ON";
				  sys_message[0]=0;
				  //sys_message[MAXBYTES]=0;
				  Rpi::setOUTsteady();
				  resp_length=5;
			  }
			  break;
		case CMD_SYSSUB_EMBREAK:
			  Rpi::setOUTiddle();	//No one sending this message yet.
			  //Logger::insertLog("MCS2PARSER: Emergency BREAK");
			  message="Emergency Break. Command not yet implemented.";
			  //The comand should actually be translated into a specific command for a loco address to stop
			  //return OK;
			  //-CMD_SYSSUB_EMBREAK requires loco address received in data[0]-data[3]
			  resp_length=5;
				for (int i=0;i<resp_length;i++)
						data[i]=msg[5+i];

			  break;
		case CMD_SYSSUB_STOPCYCLE:	//remove a specific locos from queue... not implemented
			//-CMD_SYSSUB_STOPCYCLE requires loco address received in data[0]-data[3]
			message="Remove loco form Cycle. Command not yet implemented.";
			//-CMD_SYSSUB_STOPCYCLE requires loco address received in data[0]-data[3]
			resp_length=5;
				for (int i=0;i<resp_length;i++)
						data[i]=msg[5+i];
			break;
		case CMD_SYSSUB_STOP:		//Stop power
		case CMD_SYSSUB_HALT:		//Send message speed 0 to all locos .. not  implemented
			if (message4me(msg))
			{
			command=CYCLEOFF;
			  message="Power OFF";
			  sys_message[0]=0;
			  resp_length=5;
			/*	for (int i=0;i<resp_length;i++)
						data[i]=msg[5+i];
*/
			}
			  break;
		case CMD_SYSSUB_LOCOPROT:
			//.CMD_SYSSUB_LOCOPROT requires loco address received
			addrL=msg[8];
			addrH=msg[7]*256;
			addr=addrH+addrL;
			offset=0;
			//protf;
			if (addrH<LOCOMMXOFFSET){
				offset=LOCOMMOFFSET;
				protf=LOCMMCMD;
			}
			else if (addrH<LOCODCCOFFSET){
				offset=LOCOMMXOFFSET;
				protf=LOCMMXCMD;
			}
			else {
				offset=LOCODCCOFFSET;
				protf=LOCNMRACMD;
			}

			addr=addr-offset;

			lc=LocoDepot::findLoco(addr,protf);

			if(lc){
				ID=lc->getID();
			}else{
				return LOCONOTFOUND;
			}
			message="CHANGE PROTOCOL for loco "+ID+" is not yer implemented by RPIC";
			resp_length=6;
			for (int i=0;i<resp_length;i++)
					data[i]=msg[5+i];

			break;
		case CMD_SYSSUB_SWTIME:
			if (message4me(msg))
			{
			//Logger::insertLog("MCS2PARSER: SW TIME request of "+to_string((msg[10]*256+msg[11])*10)+" ms, RPIC is not implementing this command yet");
			message="SW TIME request of "+to_string((msg[10]*256+msg[11])*10)+" ms, RPIC is not implementing this command yet";
			resp_length=7;
//			for (int i=0;i<resp_length;i++)
//					data[i]=msg[5+i];
			}
			break;
		case CMD_SYSSUB_ENAPROT:
			if (message4me(msg))
			{
			if (msg[10]&&0b00000001)
				protos+="MM2 ";
			if (msg[10]&&0b00000010)
				protos+="MFX ";
			if (msg[10]&&0b00000100)
				protos+="DCC";

			message="ENABLE PROTOCOL "+protos+".";

			response = RpicdGateway::sendconfcommand(ENABLEPROT_READ);
			if (response == OK) {
				Logger::insertLog("MCS2PARSER: getEnable sent OK");
				unsigned char *prot_read = RpicdGateway::getresponse();

				command=ENABLEPROT_WRITE;
				sys_message[0] = 2;
				sys_message[1] = (prot_read[1]&0b11111000)|msg[10];		//We use whatever is on the PIC for the MSB and only replace the last 3 LSB
			}	//port_read[0] is the checksum
			//Actually RPIC supports also MM1,,, so, this command would never be complete. BEFORE sending an rpicdgateway command
			//it would be necessary to check locodepot and base on it activate more bits in protocols

			//Moreover. The command seems to ve recieved when the PIC is not ready yet

			resp_length=6;
//			for (int i=0;i<resp_length;i++)
//					data[i]=msg[5+i];

			}
			break;
		case CMD_SYSSUB_NEWREGNR:
			if (message4me(msg)){
			command=SESSION_N_WRITE;

			message="SET SESSION NUMBER "+to_string(msg[10]*256+msg[11]);

			sys_message[0] = 3;
			sys_message[1] = msg[10];
			sys_message[2] = msg[11];

			resp_length=7;
			}
			break;
		case CMD_SYSSUB_STATUS:
			if (message4me(msg)){
			message="REPORT STATUS CHANNEL:"+ to_string(msg[10])+".";
			logflag=STATUS_LOG;
			resp_length=8;

			switch (msg[10]){

			case 1: //Amp in mA 5Amd=1024
				data[6]=0;
				data[7]=0xFF;  //1,25A aproc
				break;
			case 2: //Amn in ma in PT (for CS2) not for RPIC
			data[6]=0x00;
			data[7]=0xFF;
				break;
			case 3: //Volt in mv  5V=1024
				data[6]=0x03;
				data[7]=0xFF;  //5V
				break;
			case 4: //T in C
				{data[6]=0x00;
				data[7]=35;  //2default 35 c
//#ifdef __arm__
#if defined(__arm__) || defined(__aarch64__)
				char    line[256];
				FILE *  fp = fopen("/sys/class/thermal/thermal_zone0/temp", "r");
				if (fp == NULL) {printf("ERROR\n"); }
				else  if (fgets(line, sizeof(line), fp) != NULL)
				    //printf("%d\n",atoi(line)/1000);
					data[7]=atoi(line)/1000;
				if (fp) fclose(fp);
#endif
				break;}
			default:
				data[6]=0x00;
				data[7]=0x00;	//It should be an error
			}
			minutesLastStatus=minutesActive;
			}
			break;
		case CMD_SYSSUB_GKENNUNG:
			if (message4me(msg)){
			message="GET ID command not yet implemented";
			//Not sure what is expected with this command...
			//I am sending a response.. as usual.. but it seems no one is expecting it.
			resp_length=5;
			}
			break;
		case CMD_SYSSUB_CLOCK:
			if (message4me(msg)){
			resp_length=8;
			minutesActive++;
			message="Minutes active: "+to_string(minutesActive);
			logflag=CLOCK_LOG;
			//AS far as I can understand: msg[10]=hour msg[11]=minute msg[12]=factor... Not sure for what purpose
			//Assuming the message is received once per minute
			if (minutesActive-minutesLastStatus>ESPONTANEUSPING){
				//This is a workaound in case there is no master sending pings and RPIC has not being registered by any system capable of launching and treating status data
				//Sending an espontanosu ping will triger the registration process
				//Currently the difference is in 2 monutes (ESPONTANEUSPING). If two minutes without Status messages, this will be triggered
				unsigned char ping [BUFFSIZE_MCS2];
				ping[1]=CAN_ID_PING;
				parseMgr(ping);
			}
			}
			break;
		case CMD_SYSSUB_RESET:   //Not implemented
		default:
			//char bufhex[2];
			//sprintf(bufhex, "%X", msg[9]);
			printf("MIerda comando ");
			printf("%X ",msg[9]);
			printf("\n");
			return UNKWNOWCMD;
		  break;
	}

	if (command!=EMPTY){
		if (sys_conf==SYSTEMCMD)
			response=RpicdGateway::sendsyscommand(command,sys_message);
		if (sys_conf==CONFIGCMD)
			response=RpicdGateway::sendconfcommand(command,sys_message);
	}
	else response=OK;

	if (response==OK){
		if (message.compare("")){
			if (logflag==NOFLAG_LOG) Logger::insertLog("MCS2PARSER: SYSTEM COMMAND "+message);
			else Logger::insertLog("MCS2PARSER: SYSTEM COMMAND "+message,logflag);
		}
		if (resp_length>0)
			response=sendResponse(0,ID_SYSTEM,BIT_RESPONSE,data,resp_length);
	}

	return response;
}
int mcs2Parser::speedCMD (unsigned char *msg){
	Loco* lc;

	unsigned int response=RPICDGATEWAYERROR;
	unsigned char data[BUFFSIZE_MCS2];
	int resp_length=msg[4];

	for (int i=0;i<resp_length;i++)
		data[i]=msg[5+i];

	if (resp_length==4)
		Logger::insertLog("MCS2PARSER: RPICD is not implementing yet the query of speed. Unknown loco is being delivered as response");

	string ID;
	int addrL=msg[8];
	int addrH=msg[7]*256;
	int speed=msg[9]*256+msg[10];
	int addr=addrH+addrL;
	int offset=0;
	int protf;
	int spcnt;
	bool dir;

	//Comparea to get protfamily and addr

	if (addrH<LOCOMMXOFFSET){
		offset=LOCOMMOFFSET;
		protf=LOCMMCMD;
	}
	else if (addrH<LOCODCCOFFSET){
		offset=LOCOMMXOFFSET;
		protf=LOCMMXCMD;
	}
	else {
		offset=LOCODCCOFFSET;
		protf=LOCNMRACMD;
	}

	addr=addr-offset;

	lc=LocoDepot::findLoco(addr,protf);

	if(lc){
		ID=lc->getID();
		spcnt=lc->getSpeedCount();
		dir=lc->getTDir();

		speed=speed*spcnt/1000;

		if (speed==128) speed--;
		if (!dir) speed=speed*(-1);

		if (speed==lc->getTSpeed()) return OK;	//If no change in target speed...no need to send command

		lc->updateDriver(speed,dir);
		Logger::insertLog("MCS2PARSER: Loco ["+ID+"]  found. Updating speed to "+to_string(speed));
	}else{
		return LOCONOTFOUND;
	}

	response=sendResponse(0,ID_LOCO_VELOCITY,BIT_RESPONSE,data,resp_length);


//return OK;
	return response;

}
int mcs2Parser::fnCMD (unsigned char *msg){

	unsigned int response=RPICDGATEWAYERROR;
	unsigned char data[BUFFSIZE_MCS2];
	int resp_length=msg[4];

	for (int i=0;i<resp_length;i++)
		data[i]=msg[5+i];

	if (resp_length==5){
		Logger::insertLog("MCS2PARSER: RPICD is not implementing yet the query of function.");
		//resp_length++;
		//Evaluate function and icnlude it in response
	}

	Loco* lc;

	string ID;
	int addrL=msg[8];
	int addrH=msg[7]*256;
	int fn=msg[9];
	bool fvalue=msg[10];
	//int speed;  //no need because no speedcurve is launched tochange fn in MM
	int addr=addrH+addrL;
	int offset=0;
	int protf;
	int protocol;
	int fgroup;
		//bool dir;
	//Comparea to get protfamily and addr

	if (addrH<LOCOMMXOFFSET){
		offset=LOCOMMOFFSET;
		protf=LOCMMCMD;
	}
	else if (addrH<LOCODCCOFFSET){
		offset=LOCOMMXOFFSET;
		protf=LOCMMXCMD;
	}
	else {
		offset=LOCODCCOFFSET;
		protf=LOCNMRACMD;
	}

	addr=addr-offset;

	lc=LocoDepot::findLoco(addr,protf);

	if(lc){
		ID=lc->getID();

		if (fvalue==lc->getF(fn)) return OK; //If no change in f no need to send command again

		lc->setF(fn,fvalue);
		protocol=lc->getProtocol();
		if ((protocol<LOCODCCN14)||((protocol>LOCODCCLfx)&&(protocol<LOCOMMX_FN))){
			//MM and MMX are treated as speed commands (they have all info) except for the MMX_Fn
			lc->updateDriver(lc->getTSpeed(),lc->getTDir());
			response=OK;
		}else if (protocol==LOCOMMX_FN){
			response=RpicdGateway::sendfxcommand(lc,fn);
		}else{
			//LOCODCCfx
			if (fn<5) fgroup=0;
			else if (fn<9) fgroup=1;
			else if (fn<13) fgroup=2;
			else if (fn<17) fgroup=3;
			else if (fn<21) fgroup=4;
			else if (fn<25) fgroup=5;
			else if (fn<29) fgroup=6;
			else fgroup=0;
			response=RpicdGateway::sendfxcommand(lc,fgroup);
		}
	}else{
		return LOCONOTFOUND;
	}

	if (response==OK) Logger::insertLog("MCS2PARSER: Loco ["+ID+"]  found. Updating f" + to_string(fn) +" to "+to_string(fvalue));

	response=sendResponse(0,ID_LOCO_FUNCTION,BIT_RESPONSE,data,resp_length);

	return response;
}
int mcs2Parser::dirCMD (unsigned char *msg){

	unsigned int response=RPICDGATEWAYERROR;
	unsigned char data[BUFFSIZE_MCS2], dataSpeed[BUFFSIZE_MCS2];
	int resp_length=msg[4];
	int spcnt;

	for (int i=0;i<resp_length;i++){
		data[i]=msg[5+i];
		dataSpeed[i]=msg[5+i];
	}

	if (resp_length==4){
		Logger::insertLog("MCS2PARSER: RPICD is not implementing yet the query of direction.");
		//resp_length++
		//data[4]=0  //This should be the actual dir..
	}

	Loco* lc;

	string ID;
	int addrL=msg[8];
	int addrH=msg[7]*256;
	int addr=addrH+addrL;
	int offset=0;
	int protf;
	bool dir;
	int speed;
	if (msg[9]==1) dir=true;
	else dir=false;
//	unsigned int response=RPICDGATEWAYERROR;
	//msg[9]=1 forward; =2 reverse

	//Comparea to get protfamily and addr

	if (addrH<LOCOMMXOFFSET){
		offset=LOCOMMOFFSET;
		protf=LOCMMCMD;
	}
	else if (addrH<LOCODCCOFFSET){
		offset=LOCOMMXOFFSET;
		protf=LOCMMXCMD;
	}
	else {
		offset=LOCODCCOFFSET;
		protf=LOCNMRACMD;
	}

	addr=addr-offset;

	lc=LocoDepot::findLoco(addr,protf);

	if(lc){
		ID=lc->getID();

		if (dir==lc->getTDir()) return OK; //No repeat msg if no real change

		speed=abs(lc->getTSpeed());

		spcnt=lc->getSpeedCount();
		int speed1000=speed*1000/spcnt;
		dataSpeed[4]=byte_of(speed1000,1);
		dataSpeed[5]=byte_of(speed1000,0);

		//if (dir!=lc->getDirForward()) speed=speed*(-1);	//I think both expressions are OK, but this once keep consistence if there is a problem
		if (!dir) speed=speed*(-1);
		//speed=speed*(-1);

		lc->updateDriver(speed,dir);
		Logger::insertLog("MCS2PARSER: Loco ["+ID+"]  found. Updating dir to "+ (dir ? "forward" : "reverse"));
	}else{
		return LOCONOTFOUND;
	}

	response=sendResponse(0,ID_LOCO_DIRECTION,BIT_RESPONSE,data,resp_length);
	response=sendResponse(0,ID_LOCO_VELOCITY,BIT_RESPONSE,dataSpeed,6);
	//This  next command is necessary to sync the actual speed in any throtle that could update the speed to zero (they should not do it)
	//usleep(100000);
	//response=sendResponse(0,ID_LOCO_VELOCITY,0,dataSpeed,6);
	//response=OK;
//return OK;
	return response;
}
int mcs2Parser::accCMD (unsigned char *msg){

	unsigned int response=RPICDGATEWAYERROR;
	unsigned char data[BUFFSIZE_MCS2];
	int resp_length=msg[4];

	for (int i=0;i<resp_length;i++)
		data[i]=msg[5+i];

	int addrL=msg[8];
	int addrH=msg[7]*256;
	int addr=addrH+addrL;
	int offset=0;
	int protf;

	int deco;
	int port;
	int gate;
	bool value;
	//No control if bad offset
	if (addrH<ACCDCCOFFSET){
		offset=ACCMMOFFSET;
		protf=ACCMMCMD;
	}
	else{
		offset=ACCDCCOFFSET;
		protf=ACCNMRACMD;
	}

	deco=(addr-offset)/4+1;
	port=(addr-offset) % 4 +1;
	gate=msg[9];
	value=msg[10];


	if (addr==PTACCADDR){
		if (gate&&value) {
			Rpi::setprogtrack();
			Logger::insertLog("MCS2PARSER: Activating PT");
		}
		else {
			Rpi::clearprogtrack();
			Logger::insertLog("MCS2PARSER: Deactivating PT");
		}
	}else{
		response=RpicdGateway::sendacccommand(deco,port,gate,value, protf);
		if (response==OK) Logger::insertLog("MCS2PARSER: ACC "+to_string(deco)+" Port "+to_string(port)+" Gate "+to_string(gate)+" Value "+to_string(value));
	}

	response=sendResponse(0,ID_ACC_SWITCH,BIT_RESPONSE,data,resp_length);

	return response;
}
int mcs2Parser::readCMD (unsigned char *msg){
	unsigned int response=RPICDGATEWAYERROR;
	//No control over future protocols

	bool PTneeded=false; //because service mode

	string outcome;
	int addrL=msg[8];
	int addrH=msg[7]*256;
	int addr=addrH+addrL;

	int cv=msg[10]+256*msg[9];
	int value=msg[11];
	int command;
	int offset=0;

	//it seams read cmd does not have byte 12 defining ways of reading.. only for writing.. I am keeping them so far

	char dcc_ctl=(msg[12]>>4)&0b00000011;
	//unsigned char PTready=0;

	if (msg[12]<128) PTneeded=true;

	//msg[12]>128 means bit7=1, which means PT needed....because of service mode or not

	if ((addr==0)||(addr==0xC000)){
		//check if address para ver si es pom o no.. si es pom... terminar
		PTneeded=true;		//SERVICE MODE always in PT
		switch (dcc_ctl){
		case 1:
			command=NMRA_SERVICE_MODE_REGISTER_read;
			outcome="NMRA_SERVICE_MODE_REGISTER_read";
			break;
		case 2:
			command=NMRA_SERVICE_MODE_DIRECT_bit;
			outcome="NMRA_SERVICE_MODE_DIRECT_bit";
			break;
		case 0:
		default:
			command=NMRA_SERVICE_MODE_DIRECT_verify;
			outcome="NMRA_SERVICE_MODE_DIRECT_verify";
			break;
		}
		//So far if addr=0; only NMRAmessages are sent. No way to detect type of protocol,
		//No address to check locoDepot... no offset.
	//	PTready=PTREADY_NMRA;
	}else if (addr<ACCMMOFFSET){
		offset=LOCOMMOFFSET;
	//	PTready=PTREADY_MM;
		//addr=addr-offset;
		return PROGMODEMMNOTSUPPORTED;
	}else if (addr<ACCDCCOFFSET){
		offset=ACCMMOFFSET;
	//	PTready=PTREADY_ACC_MM;
		//addr=addr-offset;
		return PROGMODEACCMMNOTSUPPORTED;
	}else if (addr<LOCOMMXOFFSET){
		offset=ACCDCCOFFSET;
	//	PTready=PTREADY_ACC_NMRA;
		//addr=addr-offset;
		switch (dcc_ctl){
		case 1:
			return PROGMODEOPMODEREGISTERNOTSUPPORTED;
			break;
		case 2:
			command=ACCNMRAbit;
			outcome="ACCNMRAbit "+to_string(addr-offset);
			break;
		case 0:
		default:
			command=ACCNMRAverify;
			outcome="ACCNMRAverify address "+to_string(addr-offset);
			break;
		}
	}else if (addr<LOCODCCOFFSET){
		offset=LOCOMMXOFFSET;
	//	PTready=PTREADY_MMX;
		//	addr=addr-offset;
		return PROGMODEMMXNOTSUPPORTED;
	}else{
		offset=LOCODCCOFFSET;
	//	PTready=PTREADY_NMRA;
		//addr=addr-offset;
		Loco *lc;
		lc=LocoDepot::findLoco(addr-offset,LOCNMRACMD);
		if (lc->getProtocol()<LOCODCCL14) command=LOCODCCNverify;
		else command=LOCODCCLverify;

		switch (dcc_ctl){
		case 1:
			return PROGMODEOPMODEREGISTERNOTSUPPORTED;
			break;
		case 2:
			command++; //command read+1=bit mode
			outcome="LOCODCCbit address"+to_string(addr-offset);
			break;
		case 0:
		default:
			outcome="LOCODCCverify address "+to_string(addr-offset);
			break;
		}
	}

	addr=addr-offset;

	if (PTneeded) {
		Rpi::setprogtrack();
		//response=RpicdGateway::sendsyscommand(PTready,NULL);
		if (response==OK) Logger::insertLog("MCS2PARSER: Activating PT");
		else Logger::insertLog("MCS2PARSER: Activating PT Failed");
	}

	response=RpicdGateway::sendprogcommand(command,cv, value, addr);
	if (response==OK) Logger::insertLog("MCS2PARSER: READ CV: "+to_string(cv)+" Command: "+outcome);

	//In here protocol to check verify... if it is not verifying... fast read though all CV until it is possible to provide an asnwer to rocrail
	//If fast read is nt working... then cv per cv.

	//Not clear how to manage PT status in rpic




	if ((PTneeded)&&(Rpi::readprogtrack())) {
		Rpi::clearprogtrack();
		//response=RpicdGateway::sendsyscommand(PTOFF,NULL);
		if (response==OK) Logger::insertLog("MCS2PARSER: Deactivating PT");
		else Logger::insertLog("MCS2PARSER: Deactivating PT failed");
	}

	return response;
}
int mcs2Parser::writeCMD (unsigned char *msg){
//No control over future protocols
	unsigned int response=RPICDGATEWAYERROR;

	bool PTneeded=false; //service mode should go on PT
	//unsigned char PTready=0;		//This is used to send a mesage to rpic to clear the cycle for a PT message. it is not yet implemented

	string outcome;
	int addrL=msg[8];
	int addrH=msg[7]*256;
	int addr=addrH+addrL;

	int cv=msg[10]+256*msg[9];
	int value=msg[11];
	int command;
	int offset=0;

	char dcc_ctl=(msg[12]>>4)&0b00000011;

	if (msg[12]<128) PTneeded=true;

	if ((addr==0)||(addr==0xC000)){
	//Address 0xC000 is the DCC (0xC000 offset) broadcast (addr 0). Unfortunatelly some programs send addr=0 as DCC by default.
	//This makes impossible to identify a pure sevice mode packet (without address)
	//check if address para ver si es pom o no.. si es pom... terminar
	PTneeded=true;		//SERVICE MODE always in PT
		switch (dcc_ctl){
		case 1:
			command=NMRA_SERVICE_MODE_REGISTER_write;
			outcome="NMRA_SERVICE_MODE_REGISTER_write";
			break;
		case 2:
			command=NMRA_SERVICE_MODE_DIRECT_bit;
			outcome="NMRA_SERVICE_MODE_DIRECT_bit";
			break;
		case 0:
		default:
			command=NMRA_SERVICE_MODE_DIRECT_write;
			outcome="NMRA_SERVICE_MODE_DIRECT_write";
			break;
		}
		//So far if addr=0; only NMRAmessages are sent. No way to detect type of protocol,
		//No address to check locoDepot... no offset.
	//	PTready=PTREADY_NMRA;
	}else if (addr<ACCMMOFFSET){
		offset=LOCOMMOFFSET;
	//	PTready=PTREADY_MM;
		//addr=addr-offset;
		return PROGMODEMMNOTSUPPORTED;
	}else if (addr<ACCDCCOFFSET){
		offset=ACCMMOFFSET;
	//	PTready=PTREADY_ACC_MM;
		//addr=addr-offset;
		return PROGMODEACCMMNOTSUPPORTED;
	}else if (addr<LOCOMMXOFFSET){
		offset=ACCDCCOFFSET;
	//	PTready=PTREADY_ACC_NMRA;
		//addr=addr-offset;
		switch (dcc_ctl){
		case 1:
			return PROGMODEOPMODEREGISTERNOTSUPPORTED;
			break;
		case 2:
			command=ACCNMRAbit;
			outcome="ACCNMRAbit "+to_string(addr-offset);
			break;
		case 0:
		default:
			command=ACCNMRAwrite;
			outcome="ACCNMRAwrite address "+to_string(addr-offset);
			break;
		}
	}else if (addr<LOCODCCOFFSET){
		offset=LOCOMMXOFFSET;
	//	PTready=PTREADY_MMX;
	//	addr=addr-offset;
		return PROGMODEMMXNOTSUPPORTED;
	}else{
		offset=LOCODCCOFFSET;
	//	PTready=PTREADY_NMRA;
		//addr=addr-offset;
		Loco *lc;
		lc=LocoDepot::findLoco(addr-offset,LOCNMRACMD);
//		if (lc!=NULL){
		if (lc->getProtocol()<LOCODCCL14) command=LOCODCCNwrite;
		else command=LOCODCCLwrite;
//		}else{
			//Just for testing avoid crash
//			command=LOCODCCNwrite;
//			printf("*******AVOIDING CRASH********");
//  		    printf("\n");
//		}
	//	outcome="LOCODCCwrite address "+to_string(addr-offset);

		switch (dcc_ctl){
		case 1:
			return PROGMODEOPMODEREGISTERNOTSUPPORTED;
			break;
		case 2:
			command=command+2; //command write+2=bit mode
			outcome="LOCODCCbit address"+to_string(addr-offset);
			break;
		case 0:
		default:
			outcome="LOCODCCwrite address "+to_string(addr-offset);
			break;
		}
	}

	if (PTneeded) {
		Rpi::setprogtrack();
		Logger::insertLog("MCS2PARSER: Activating PT");
/*		response=RpicdGateway::sendsyscommand(PTready,NULL);
		if (response==OK) Logger::insertLog("MCS2PARSER: Activating PT");
		else Logger::insertLog("MCS2PARSER: Activating PT Failed");
*/	}

	addr=addr-offset;

	response=RpicdGateway::sendprogcommand(command,cv, value, addr);
	if (response==OK) Logger::insertLog("MCS2PARSER: WRITE CV: "+to_string(cv)+" Value: "+to_string(value)+" Command: "+outcome);

	if ((PTneeded)&&(Rpi::readprogtrack())) {
		Rpi::clearprogtrack();
		Logger::insertLog("MCS2PARSER: Deactivating PT");
/*		response=RpicdGateway::sendsyscommand(PTOFF,NULL);
		if (response==OK) Logger::insertLog("MCS2PARSER: Deactivating PT");
		else Logger::insertLog("MCS2PARSER: Deactivating PT failed");
*/	}

	return response;
}
int mcs2Parser::lok_discoveryCMD (unsigned char *msg){

unsigned char sys_message[6]; //5 butes datos +a uno guardar longitud
//int i;
//int j;
unsigned char UID[4];
//unsigned char mask=0;
//unsigned long uuid=0;
//unsigned char mask=0;
	//look length
unsigned int response=RPICDGATEWAYERROR;

if (msg==NULL) {
	sys_message[0]=0;	//no message, means function called directly from web
}else{
sys_message[0]=msg[4];
}//length message
	switch (sys_message[0]){
	case 0:
		for (int i=0;i<4;i++){
			UID[i]=0; //Init UID to zero
		}
		return lok_discoveryRR(UID,0);

		//Get all records... I am translating this command into getting entire UID.
		//Docs do not clarify how the entire UID is transmitted back, probably with a length 5 message?

/*
		//printf("Inside lok\n");
		//ver algoritmo recorrer de MSB a LSB y con mask de 0 a 32
		sys_message[0]=6;
		for (i=1;i<5;i++){
			sys_message[i]=0; //init to zero
		}

		sys_message[5]=0;
		//Acroding to docs with mask 0 and 0x0 2 messages
		for (i=0;i<2;i++){
			response=RpicdGateway::sendsyscommand(DISCOVERY,sys_message);
			usleep(100000);		//delay 0f 100ms from one command to another according to doc.. no claro
		}

		//If ther is any response.. then keep looking
		for (i=0;i<4;i++){
			for (j=0;j<8;j++){
				//primero se prueba bit clear
				sys_message[5]=i*8+j+1;  //no sé si más 1
				response=RpicdGateway::sendsyscommand(DISCOVERY,sys_message);
				usleep(100000);		//delay 0f 100ms from one command to another according to doc
				//saca datos por aquí
				//FALTA TRATAR RESPUESTA!!! y guardar resultado... si no es correcta, se pone el bit
				//If
				setbit(sys_message[1+i],7-j);
				//Y se prueba con misma mask
				response=RpicdGateway::sendsyscommand(DISCOVERY,sys_message);
				usleep(100000);		//delay 0f 100ms from one command to another according to doc
				//saca datos por aquí
				//si no es correcto.. algo va mal, no 0, ni 1. abortar
			}

		}
		//printf("Inside lok2\n");
		//for testing
	*/	//return OK;
		break;
	case 1:
		//This should repsonse with last UID reported or with 0.. probably case 0 too?
		break;
	case 5:
		for (int i=1;i<6;i++){
			sys_message[i]=msg[4+i];
		}
		sys_message[0]=sys_message[0]+1; //+length byte
		response=RpicdGateway::sendsyscommand(DISCOVERY,sys_message);
		if (response==OK) Logger::insertLog("MCS2PARSER: SYSTEM COMMAND LOK DISCOVERY");
		//FALTA TRATAR RESPUESTA!!!

		return response;
		break;

	//case 6:  //6 is only a resposne.. not a rquest.
	default:
		return MMXDISCOVERYMODENA;
	break;

	}
	return MMXDISCOVERYMODENA;
}

int mcs2Parser::lok_discoveryRR (unsigned char *UID,unsigned char range){

	int bytepos=range/8;
	int bitpos=range%8;
	unsigned char data[BUFFSIZE_MCS2];

	range++; //We increment rage for next iteration

	int result;
	unsigned char *response;
	bool found=false;

	unsigned char *UID_1;

	unsigned char sys_message[7]; //5 butes datos +a uno guardar longitud+1 ajuste

	sys_message[0]=7; //length message
	//sys_message[0]=0x00; //just to follow a common schema in rpic
	//sys_message[1]=range;
	sys_message[1]=0x00; //just to follow a common schema in rpic
	sys_message[2]=range;


	//0 branch
	clrbit(UID[bytepos],bitpos);
	for (int i=0;i<4;i++)
		sys_message[i+3]=UID[i];

	result=RpicdGateway::sendsyscommand(DISCOVERY,sys_message);

	if (result==OK){
		response=RpicdGateway::getresponse();
		if (response[1]){
			found=true;
		//If we have a response, we keep clear UID bit and start exploring a new branch
			if (range<32) {
				usleep(100000);		//delay 0f 100ms from one command to another according to doc
				lok_discoveryRR(UID,range);
			}else{
				char hex[3]; //2+null character by sprintf
				string hexmessage = "";
				for (int i=0;i<4;i++){
					sprintf(hex, "%.2X", UID[i]);
					hexmessage = hexmessage + hex + " ";
					data[i]=UID[i];
				}
				data[4]=range;
				if (LocoDepot::findLoco(UID)==NULL)
				{
					LocoDepot::newLoco("NR",UID);
					Logger::insertLog("MCS2PARSER: Loco found: "+hexmessage);
				}else{
					Logger::insertLog("MCS2PARSER-ERROR: Loco already in Depot: "+hexmessage);
				}
				result=sendResponse(0,ID_LOCO_DISCOVERY,BIT_RESPONSE,data,5);
				//send response sys
				return OK;
			}
		}

	}else{
		return result;
	}

	//1 branch
	if (found) {
		unsigned char newUID[4];
		for (int i=0;i<4;i++)
			newUID[i]=UID[i];
		UID_1=newUID;
	}else UID_1=UID;
	setbit(UID_1[bytepos],bitpos);

	for (int i=0;i<4;i++)
		sys_message[i+3]=UID_1[i];

	result=RpicdGateway::sendsyscommand(DISCOVERY,sys_message);

	if (result==OK){
		response=RpicdGateway::getresponse();
		if (response[1]){
		//	found=true;
		//If we have a response, we keep clear UID bit and start exploring a new branch
			if (range<32) {
				usleep(100000);		//delay 0f 100ms from one command to another according to doc
				lok_discoveryRR(UID_1,range);
			}else{
				char hex[3]; //2+null character by sprintf
				string hexmessage = "";
				for (int i=0;i<4;i++){
					sprintf(hex, "%.2X", UID_1[i]);
					hexmessage = hexmessage + hex + " ";
					data[i]=UID_1[i];
				}
				data[4]=range;
				if (LocoDepot::findLoco(UID_1)==NULL)
				{
					LocoDepot::newLoco("NR",UID_1);
					Logger::insertLog("MCS2PARSER: Loco found: "+hexmessage);
					}else{
					Logger::insertLog("MCS2PARSER-ERROR: Loco already in Depot: "+hexmessage);
				}
				result=sendResponse(0,ID_LOCO_DISCOVERY,BIT_RESPONSE,data,5);
				return OK;
			}
		}

	}else{
		return result;
	}

//return OK;

}

int mcs2Parser::sendStatusConfig(unsigned char index){

	unsigned char data[8];

	for (int i=0;i<4;i++)
		data[i]=zentralUID[i];
	data[4]=index;

	switch (index){
		case 0:
			data[5]=3; //3 packets
			sendResponse(0,ID_SYS_STAT_DATA,BIT_RESPONSE,data,6,NOHASH);
			//packet 1
			//3 meassures in first two bytes two blanck bytes and 4 bytes with serial number based on uix 'RPIC'
			//These seems to be a bug or a misundesrtanding on the protocol. It clearly states the first byte number of meassures and the seond number of canals
			//On v1.0 of the protocol byte 1=0. In any case, any value on byte 0=to a number of canals hereafter makes the canal to be reconfigures. For instance with byt11=1 power=1.
			//This was causing chanel 3 (the original normal configuration) to stop working
			//Unfortunatelly this bug does not allow to register RPIC with 0 measures (much more accurate than sending 3 measures with no useful data)
			//data[0]=3; data[1]=3; data[2]=0; data[3]=0; data[4]=0xAA;data[5]=0xAA;data[6]=0xAA;data[7]=0xAA;
			data[0]=0; data[1]=1; data[2]=0; data[3]=0; data[4]=zentralUID[3];data[5]=zentralUID[2];data[6]=zentralUID[1];data[7]=zentralUID[0];
			sendResponse(0,ID_SYS_STAT_DATA,BIT_RESPONSE,data,8,1);
			//packet 2
			//Artickle number string 8 bytes, (string terminate \0)
			//data[0]='6'; data[1]='0'; data[2]='1'; data[3]='1'; data[4]='2';data[5]=0;data[6]=0;data[7]=0;
			//data[0]='6'; data[1]='0'; data[2]='1'; data[3]='7'; data[4]='3';data[5]=0;data[6]=0;data[7]=0;
			data[0]='R'; data[1]='A'; data[2]='I'; data[3]='L'; data[4]=zentralUID[2];data[5]=zentralUID[1];data[6]=zentralUID[0];data[7]=0;
			sendResponse(0,ID_SYS_STAT_DATA,BIT_RESPONSE,data,8,2);
			//packet 3
			//ADevice name (string terminate \0)
			//data[0]='T'; data[1]='R'; data[2]='A'; data[3]='C'; data[4]='K';data[5]='0';data[6]='1';data[7]=0;
			data[0]=zentralUID[3]; data[1]=zentralUID[2]; data[2]=zentralUID[1]; data[3]=zentralUID[0]; data[4]=0;data[5]=0;data[6]=0;data[7]=0;
			sendResponse(0,ID_SYS_STAT_DATA,BIT_RESPONSE,data,8,3);
			break;
		case 1:
		//Amperes... Actually. RPIC can not read Current. Any definition could fit
			data[5]=5; //5 packets
			sendResponse(0,ID_SYS_STAT_DATA,BIT_RESPONSE,data,6,NOHASH);
			//packet 1
			// canal 1, potencia -3=0xFD; 4 colors,Nullpunkt (0 2 bytes, not 00 0f)
			data[0]=1; data[1]=0xFD; data[2]=0x30; data[3]=0xF0; data[4]=0xE0;data[5]=0xC0;data[6]=0;data[7]=0;
			sendResponse(0,ID_SYS_STAT_DATA,BIT_RESPONSE,data,8,1);
			//packet 2
			// Limits for colours and max value (last two bytes)
			data[0]=0x04; data[1]=0x00; data[2]=0x06; data[3]=0x00; data[4]=0x07;data[5]=0x68;data[6]=0x03;data[7]=0xFF;
			sendResponse(0,ID_SYS_STAT_DATA,BIT_RESPONSE,data,8,2);
			//packet 3
			// SString Messwertbezeichnung \0 Init range (0.
			data[0]='T'; data[1]='R'; data[2]='A'; data[3]='C'; data[4]='K';data[5]=0;data[6]='0';data[7]='.';
			sendResponse(0,ID_SYS_STAT_DATA,BIT_RESPONSE,data,8,3);
			//packet 4
			//Init range\0+End range. To my knowledge it isnot fully correct. As Protocol says number of decimals and potencia must be the same
			data[0]='0'; data[1]='0'; data[2]=0; data[3]='5'; data[4]='.';data[5]='0';data[6]='0';data[7]=0;
			sendResponse(0,ID_SYS_STAT_DATA,BIT_RESPONSE,data,8,4);
			//packet 5
			//String Unit\0
			data[0]='A'; data[1]=0; data[2]=0; data[3]=0; data[4]=0;data[5]=0;data[6]=0;data[7]=0;
			sendResponse(0,ID_SYS_STAT_DATA,BIT_RESPONSE,data,8,5);
			break;
		case 2:
		//Voltage... Actually. RPIC can not read Voltage. Any definition could fit
			data[5]=5; //5 packets
			sendResponse(0,ID_SYS_STAT_DATA,BIT_RESPONSE,data,6,NOHASH);
			//packet 1
			//canal 3, potencia -3=0xFD; 4 colors,Nullpunkt
			data[0]=3; data[1]=0xFD; data[2]=0xC0; data[3]=0x0C; data[4]=0x30;data[5]=0xC0;data[6]=0;data[7]=0;
			sendResponse(0,ID_SYS_STAT_DATA,BIT_RESPONSE,data,8,1);
			//packet 2
			// Limits for colours and max value (last two bytes)
			data[0]=0x02; data[1]=0x66; data[2]=0x03; data[3]=0x0A; data[4]=0x03;data[5]=0x85;data[6]=0x03;data[7]=0xFF;
			sendResponse(0,ID_SYS_STAT_DATA,BIT_RESPONSE,data,8,2);
			//packet 3
			//String Messwertbezeichnung \0 Init range (0.
			data[0]='V'; data[1]='O'; data[2]='L'; data[3]='T'; data[4]=0;data[5]='0';data[6]='.';data[7]='0';
			sendResponse(0,ID_SYS_STAT_DATA,BIT_RESPONSE,data,8,3);
			//packet 4
			//Init range\0+End range\0+Unit. To my knowledge it is not fully correct. As Protocol says number of decimals and potencia must be the same
			data[0]='0'; data[1]=0; data[2]='5'; data[3]='.'; data[4]='0';data[5]='0';data[6]=0;data[7]='V';
			sendResponse(0,ID_SYS_STAT_DATA,BIT_RESPONSE,data,8,4);
			//packet 5
			//End String Unit\0
			data[0]='o'; data[1]='l'; data[2]='t'; data[3]=0; data[4]=0;data[5]=0;data[6]=0;data[7]=0;
			sendResponse(0,ID_SYS_STAT_DATA,BIT_RESPONSE,data,8,5);
			break;
		case 3:
			//Temperature. This seams the unique measure that maybe odroid or rpi can take
			data[5]=5; //4 packets
			sendResponse(0,ID_SYS_STAT_DATA,BIT_RESPONSE,data,6,NOHASH);
			//packet 1
			// canal 4, potencia 4; 4 colors,Nullpunkt
			data[0]=4; data[1]=0; data[2]=0x0C; data[3]=0x08; data[4]=0xF0;data[5]=0xC0;data[6]=0;data[7]=0;
			sendResponse(0,ID_SYS_STAT_DATA,BIT_RESPONSE,data,8,1);
			//packet 2
			//Limits for colours and limits ov values
			data[0]=0x00; data[1]=0x1e; data[2]=0x00; data[3]=0x32; data[4]=0x00;data[5]=0x64;data[6]=0x00;data[7]=0x7D;  //7D=125
			sendResponse(0,ID_SYS_STAT_DATA,BIT_RESPONSE,data,8,2);
			//packet 3
			//String Messwertbezeichnung \0 Init range \0.
			data[0]='T'; data[1]='E'; data[2]='M'; data[3]='P'; data[4]=0;data[5]='0';data[6]='.';data[7]='0';
			sendResponse(0,ID_SYS_STAT_DATA,BIT_RESPONSE,data,8,3);
			//packet 4
			//Init range\0+End range +uNIT. To my knowledge it is not fully correct. As Protocol says number of decimals and potencia must be the same
			data[0]=0; data[1]='1'; data[2]='2'; data[3]='5'; data[4]='.';data[5]='0';data[6]=0;data[7]='C';
			sendResponse(0,ID_SYS_STAT_DATA,BIT_RESPONSE,data,8,4);
			//packet 5
			data[0]=0; data[1]=0; data[2]=0; data[3]=0; data[4]=0;data[5]=0;data[6]=0;data[7]=0;
			sendResponse(0,ID_SYS_STAT_DATA,BIT_RESPONSE,data,8,5);
			break;
		default:
			printf("MCS2PARSER: Status Config, index not defined\n");
			return INDEKNOTKNOWN;
	}
return OK;

}
