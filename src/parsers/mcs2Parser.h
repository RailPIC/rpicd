/*
 * mcs2.h
 *
 *  Created on: 28/03/2016
 *      Author: agonia
 */

#ifndef MCS2_H_
#define MCS2_H_

#include <arpa/inet.h>
#include <map>
#include "CommandParser.h"

#define LOCOMMOFFSET  0x0000
#define LOCOMMXOFFSET 0x4000
#define LOCODCCOFFSET 0xC000
#define ACCMMOFFSET   0x3000
#define ACCDCCOFFSET  0x3800
#define PTACCADDR     0x1C00		//This address is reserved in the protocol. However, it seems to be used to activate the Programming track


//From      ROCRAIL/rocdigs/impl/mcs2/mcs2-const.h

// not all commands are implemented

/*
 * CAN over TCP/IP 13 byte format:
 *
 *  |  0   |  1    | 2 | 3 |  4  | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 |
 *  | PRIO CMD RSP | HASH  | DLC |             DATA                 |
 *
 */

#define BIT_RESPONSE 0x01
#define NOHASH     0
#define ESPONTANEUSPING 1

/* SYSTEM */
#define CMD_SYSTEM            0x00
#define ID_SYSTEM             0x00
#define CMD_SYSSUB_STOP       0x00
#define CMD_SYSSUB_GO         0x01
#define CMD_SYSSUB_HALT       0x02
#define CMD_SYSSUB_EMBREAK    0x03  //Lok NOthalt
#define CMD_SYSSUB_STOPCYCLE  0x04
#define CMD_SYSSUB_LOCOPROT   0x05
#define CMD_SYSSUB_SWTIME     0x06
#define CMD_SYSSUB_ENAPROT    0x08
#define CMD_SYSSUB_NEWREGNR   0x09
#define CMD_SYSSUB_OVERLOAD   0x0A
#define CMD_SYSSUB_STATUS     0x0B
#define CMD_SYSSUB_GKENNUNG   0x0C	//ID
#define CMD_SYSSUB_CLOCK      0x20
#define CMD_SYSSUB_RESET      0x80

/* PROTOCOLS */
#define PROT_MM2              0x01
#define PROT_MFX              0x02
#define PROT_DCC              0x04

/* LOC PROTOCOLS */
#define LOCO_PROT_DCC_28      0x00
#define LOCO_PROT_DCC_14      0x01
#define LOCO_PROT_DCC_128     0x02
#define LOCO_PROT_DCC_L_28    0x03
#define LOCO_PROT_DCC_L_128   0x04

/* LOCOs */
#define CMD_LOCO_DISCOVERY    0x01
#define ID_LOCO_DISCOVERY     0x02

#define CMD_LOCO_BIND         0x02
#define ID_LOCO_BIND          0x04

#define CMD_LOCO_VERIFY       0x03
#define ID_LOCO_VERIFY        0x06

#define CMD_LOCO_VELOCITY     0x04
#define ID_LOCO_VELOCITY      0x08

#define CMD_LOCO_DIRECTION    0x05
#define ID_LOCO_DIRECTION     0x0A

#define CMD_LOCO_FUNCTION     0x06
#define ID_LOCO_FUNCTION      0x0C

#define CMD_LOCO_READ_CONFIG  0x07
#define ID_LOCO_READ_CONFIG   0x0E

#define CMD_LOCO_WRITE_CONFIG 0x08
#define ID_LOCO_WRITE_CONFIG  0x10


/* ACCESSORIES */
#define CMD_ACC_SWITCH        0x0B
#define ID_ACC_SWITCH         0x16
#define ID_ACC_SWITCH_RSP     0x17
#define CMD_ACC_SENSOR        0x11


/* SOFTWARE */
#define CAN_CMD_PING         0x18
#define CAN_ID_PING          0x30

/* Sensors */
#define CAN_S88_REPORT       0x21
#define CAN_SENSOR_EVENT     0x23

/* CAN device control */
# define CMD_CAN_BOOT_BOUND  0x1B
# define ID_CAN_BOOT_BOUND   0x36

/* System Status data */
#define CMD_SYS_STAT_DATA    0x1D
#define ID_SYS_STAT_DATA     0x3A

/* System config data */
#define CMD_SYS_ASK_CDATA    0x20
#define ID_SYS_ASK_CDATA     0x40
#define CMD_SYS_CONF_DATA    0x21
#define ID_SYS_CONF_DATA     0x42

//coming from manual and http://www.microchip.com/forums/m701812-print.aspx

#define testbit(var, bit)   ((var) & (1 <<(bit)))
#define setbit(var, bit)    ((var) |= (1 << (bit)))
#define clrbit(var, bit)    ((var) &= ~(1 << (bit)))
#define bittgl(var,bit)     ((var) ^= (1<<(bit)))

#define byte_of(your_var,byte_num) (*(((unsigned char*)&your_var)+byte_num))



class mcs2Parser: public CommandParser{
public:
	mcs2Parser();
	virtual ~mcs2Parser();
	virtual void openConnection();
	int lok_discoveryCMD (unsigned char *msg);
private:
	struct sockaddr_in addr;
	struct sockaddr_in addrTX;
	unsigned int portTX;
	int mcs2sock;
	int mcs2sockTX;
	//std::map<int,string> addr2ID;
	unsigned char zentralUID[4];
	unsigned long zUID;
	unsigned char storedHash[2];
	unsigned int minutesLastStatus;
	unsigned int minutesActive;
	void connection_handler();
	//int sendResponse(unsigned char *msg, unsigned char length);
	int sendResponse(unsigned char prio, unsigned char cmd, unsigned char rsp, unsigned char *data, unsigned char length,unsigned int hash=NOHASH);
	//void mapIDGen();
	bool message4me(unsigned char* msg);
	void parseMgr(unsigned char *msg);
	int sysCMD (unsigned char *msg);
	int speedCMD (unsigned char *msg);
	int fnCMD (unsigned char *msg);
	int dirCMD (unsigned char *msg);
	int accCMD (unsigned char *msg);
	int readCMD (unsigned char *msg);
	int writeCMD (unsigned char *msg);
	int lok_discoveryRR (unsigned char *UID,unsigned char range);
	int sendStatusConfig(unsigned char index);

	//	int lok_discoveryCMD (unsigned char *msg);

};
#endif /* MCS2_H_ */
