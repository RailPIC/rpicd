/*
 * loconetParser.h
 *
 *  Created on: 08/05/2017
 *      Author: agonia
 */

#ifndef LOCONETPARSER_H_
#define LOCONETPARSER_H_


#include <arpa/inet.h>
#include <map>
#include "CommandParser.h"

class loconetParser: public CommandParser {
public:
	loconetParser();
	virtual ~loconetParser();
	long port;
	void openConnection();
private:
	int loconetsock;
	struct sockaddr_in addr;
	void connection_handler();
	void parseMgr(unsigned char *msg);


};

#endif /* LOCONET_H_ */
