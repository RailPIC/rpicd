/*
 * EcosParser.cpp
 *
 *  Created on: 10/2/2015
 *      Author: mserrano
 */

#include "EcosParser.h"
#include "CommandParser.h"
#include "../Logger.h"
#include "../LocoDepot.h"
#include "../Config.h"
#include <iostream>
#include <unistd.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <thread>         // std::thread
using namespace std;


#define BUFFSIZE 2048

EcosParser::EcosParser() {

	//DEFAULT VALUES
	maxclients= MAXCLIENTS;
	port=ECOSPORT;
	locopath="";
}

EcosParser::~EcosParser() {
	// TODO Auto-generated destructor stub
}

void EcosParser::openConnection(){

	Logger::insertLog("********************ECOS Parser***********************");

    if (locopath.compare("")){
    	//do something to recover locopath
    	Logger::insertLog("Parsing locopath: "+locopath);
    }

    if (!(LocoDepot::init)){
    	Logger::insertLog("ERROR: ECOS Parser requires a LocoDepot plan");
    	exit(EXIT_FAILURE);
    }
	 if ((ecossock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
	     	Logger::insertLog("ERROR: Not possible to create a socket");
	     	exit(EXIT_FAILURE);
	 }

	 /* Construct the server sockaddr_in structure */
	 memset(&addr, 0, sizeof(addr));       /* Clear struct */
	 addr.sin_family = AF_INET;                  /* Internet/IP */
	 //addr.sin_addr.s_addr = inet_addr(RocClient::IP.c_str());  /* IP address */
	 addr.sin_addr.s_addr = htonl(INADDR_ANY); /* N.B.: differs from sender */
	 addr.sin_port = htons(port);       /* server port */

     /* bind to receive address */
     if (bind(ecossock,(struct sockaddr *) &addr,sizeof(addr)) < 0) {
		Logger::insertLog("ERROR: Not possible to bind socket");
		exit(EXIT_FAILURE);
	 }

     //http://stackoverflow.com/questions/13888453/stdthread-unresolved-overloaded-function-type-error
	 std::thread ecosthread (&EcosParser::connection_handler,this);

	 ecosthread.detach();

}

void EcosParser::connection_handler() {
    int clilen, nbytes, i, client_sock;
    char buffer[BUFFSIZE];

    i=0;
	/* now just enter a read-print loop */

    listen(ecossock , maxclients);

	Logger::insertLog("ECOS Connection ready to receive data");

//	 while (true) {

	 struct sockaddr_in client;
	 clilen=sizeof(client);

	 while( (client_sock = accept(ecossock, (struct sockaddr *)&client, (socklen_t*)&clilen)) )
	 {

		 //In here there should go soemthing to handle several connections... currently it is only accepting one.

		 while (true){
		   if ((nbytes = read(client_sock, buffer, BUFFSIZE-1)) > 0) {

		//************************************************************Print for testing
		         printf("Mensaje %d\n",i);
		       	 printf("%s\n",buffer);
		//       	 parseMgr(buffer);
		       	 i++;
		       }

		// Logger::insertLog(to_string(i)+": "+string(buffer));
		 memset(buffer, 0, BUFFSIZE-1);
		 //buffer="";
		 }

    }

	 close(ecossock);

}
