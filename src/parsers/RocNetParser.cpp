/*
 * RocNetParser.cpp
 *
 *  Created on: 10/2/2015
 *      Author: mserrano
 */

#include "RocNetParser.h"
#include "CommandParser.h"
#include "../Logger.h"
#include "../LocoDepot.h"
#include "../Config.h"
#include <iostream>
#include <unistd.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <thread>         // std::thread
using namespace std;

#define BUFFSIZE 512

RocNetParser::RocNetParser() {

	//DEFAULT VALUES
	maxclients= MAXCLIENTS;
	port=ROCNETPORT;
	locopath="";
}

RocNetParser::~RocNetParser() {
	// TODO Auto-generated destructor stub
}
void RocNetParser::openConnection(){

	Logger::insertLog("********************Rocnet Parser***********************");

    if (locopath.compare("")){
    	//do something to recover locopath
    	Logger::insertLog("Parsing locopath: "+locopath);
    }

    if (!(LocoDepot::init)){
    	Logger::insertLog("ERROR: Rocnet Parser requires a LocoDepot plan");
    	exit(EXIT_FAILURE);
    }

	struct ip_mreq mreq;


     //u_int yes=1;            /*** MODIFICATION TO ORIGINAL */
       int yes=1;

     /* create what looks like an ordinary UDP socket */
     if ((rocnetsock=socket(AF_INET,SOCK_DGRAM,0)) < 0) {
     	Logger::insertLog("ERROR: Not possible to create a socket");
     	exit(EXIT_FAILURE);
     }

/**** MODIFICATION TO ORIGINAL */
    /* allow multiple sockets to use the same PORT number */
    if (setsockopt(rocnetsock,SOL_SOCKET,SO_REUSEADDR,&yes,sizeof(yes)) < 0) {
    	Logger::insertLog("ERROR: Not possible to reuse address");
    	exit(EXIT_FAILURE);
       }
/*** END OF MODIFICATION TO ORIGINAL */

     /* set up destination address */
     memset(&addr,0,sizeof(addr));
     addr.sin_family=AF_INET;
     addr.sin_addr.s_addr=htonl(INADDR_ANY); /* N.B.: differs from sender */
     addr.sin_port=htons(port);

     /* bind to receive address */
     if (bind(rocnetsock,(struct sockaddr *) &addr,sizeof(addr)) < 0) {
		Logger::insertLog("ERROR: Not possible to bind socket");
		exit(EXIT_FAILURE);
	 }

     /* use setsockopt() to request that the kernel join a multicast group */
     mreq.imr_multiaddr.s_addr=inet_addr("224.0.0.1");
     mreq.imr_interface.s_addr=htonl(INADDR_ANY);
     if (setsockopt(rocnetsock,IPPROTO_IP,IP_ADD_MEMBERSHIP,&mreq,sizeof(mreq)) < 0) {
 		Logger::insertLog("ERROR: Not possible to set socket");
 		exit(EXIT_FAILURE);
     }
     //http://stackoverflow.com/questions/13888453/stdthread-unresolved-overloaded-function-type-error
	 std::thread rocnetthread (&RocNetParser::connection_handler,this);

	 rocnetthread.detach();

}

void RocNetParser::connection_handler() {
    int addrlen, nbytes, i;
    char buffer[BUFFSIZE];

	/* now just enter a read-print loop */

	Logger::insertLog("ROCNET Connection ready to receive data");

	 while (true) {


     addrlen=sizeof(addr);
	  if ((nbytes=recvfrom(rocnetsock,buffer,BUFFSIZE,0,(struct sockaddr *) &addr,(socklen_t*)&addrlen)) < 0) {
			Logger::insertLog("ERROR receiving data");
			exit(EXIT_FAILURE);
	  }

	  printf("Data: ");
	  for (i=0;i<nbytes;i++){
		  printf("%X ",buffer[i]);
	  }
	  printf("\n");
    }

}
