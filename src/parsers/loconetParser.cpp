/*
 * Loconet.cpp
 *
 *  Created on: 08/05/2017
 *      Author: agonia
 */

#include "loconetParser.h"
#include "../Config.h"
#include "../Logger.h"
#include <string.h>

#include <thread>         // std::thread


#define BUFFSIZE 20


loconetParser::loconetParser() {

	port=LOCONETPORT;

}

loconetParser::~loconetParser() {
	// TODO Auto-generated destructor stub
}

void loconetParser::openConnection(){

	Logger::insertLog("**********************MCS2 Parser***********************");

	struct ip_mreq mreq;

     //u_int yes=1;            /*** MODIFICATION TO ORIGINAL */
      int yes=1;

     /* create what looks like an ordinary UDP socket */
     if ((loconetsock=socket(AF_INET,SOCK_DGRAM,0)) < 0) {
     	Logger::insertLog("ERROR: Not possible to create a socket");
     	exit(EXIT_FAILURE);
     }

/**** MODIFICATION TO ORIGINAL */
    /* allow multiple sockets to use the same PORT number */
    if (setsockopt(loconetsock,SOL_SOCKET,SO_REUSEADDR,&yes,sizeof(yes)) < 0) {
    	Logger::insertLog("ERROR: Not possible to reuse address");
    	exit(EXIT_FAILURE);
       }
/*** END OF MODIFICATION TO ORIGINAL */

     /* set up destination address */
     memset(&addr,0,sizeof(addr));
     addr.sin_family=AF_INET;
     addr.sin_addr.s_addr=htonl(INADDR_ANY); /* N.B.: differs from sender */
     addr.sin_port=htons(port);

     /* bind to receive address */
     if (bind(loconetsock,(struct sockaddr *) &addr,sizeof(addr)) < 0) {
		Logger::insertLog("ERROR: Not possible to bind socket");
		exit(EXIT_FAILURE);
	 }

     /* use setsockopt() to request that the kernel join a multicast group */
     mreq.imr_multiaddr.s_addr=inet_addr("224.0.0.1");
     mreq.imr_interface.s_addr=htonl(INADDR_ANY);
     if (setsockopt(loconetsock,IPPROTO_IP,IP_ADD_MEMBERSHIP,&mreq,sizeof(mreq)) < 0) {
 		Logger::insertLog("ERROR: Not possible to set socket");
 		exit(EXIT_FAILURE);
     }
     //http://stackoverflow.com/questions/13888453/stdthread-unresolved-overloaded-function-type-error
	 std::thread loconetthread (&loconetParser::connection_handler,this);

	 loconetthread.detach();

}


void loconetParser::connection_handler() {
    int addrlen, nbytes, i, j;
    unsigned char buffer[BUFFSIZE];

	/* now just enter a read-print loop */

	Logger::insertLog("Loconet Connection ready to receive data");

	j=0;
	while (true) {


     addrlen=sizeof(addr);
	  if ((nbytes=recvfrom(loconetsock,buffer,BUFFSIZE,0,(struct sockaddr *) &addr,(socklen_t*)&addrlen)) < 0) {
			Logger::insertLog("ERROR receiving data");
			exit(EXIT_FAILURE);
	  }

	  printf("Data %i:",j);
	  for (i=0;i<nbytes;i++){
		  printf("%X ",buffer[i]);
	  }
	  printf("\n");
	  j++;
	  parseMgr(buffer);

    }

}


void loconetParser::parseMgr(unsigned char *msg) {

//	int result=OK;
	char bufhex[2];

	sprintf(bufhex, "%X", msg[1]);

	//I am not treating priotity nor neching integreity

}
