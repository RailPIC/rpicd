/*
 * EcosParser.h
 *
 *  Created on: 10/2/2015
 *      Author: mserrano
 */

#ifndef ECOSPARSER_H_
#define ECOSPARSER_H_

#include "CommandParser.h"

#include <arpa/inet.h>



#include "CommandParser.h"

class EcosParser: public CommandParser {
public:
	EcosParser();
	virtual ~EcosParser();
	virtual void openConnection();
private:
	struct sockaddr_in addr;
	int ecossock;
	void connection_handler();
};

#endif /* ECOSPARSER_H_ */
