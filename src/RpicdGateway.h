/*
 * RpicdGateway.h
 *
 *  Created on: 11/08/2015
 *      Author: agonia
 */

//PROTOTYPE... DEFINITION WILL DPEENDS VERY MUCH ON WHAT IS NEEDED BY OTHER PARSES

#ifndef RPICDGATEWAY_H_
#define RPICDGATEWAY_H_
#define MAXBYTES 12

#include "Loco.h"

class RpicdGateway {
public:
	RpicdGateway();
	virtual ~RpicdGateway();
	static unsigned int sendfxcommand(Loco *lc,int fgroup);
	//static void sendspeedcommand(Loco *lc,int fchanged);
	static unsigned int sendspeedcommand(Loco *lc);
	static unsigned int sendacccommand(int address, int port, int gate, bool value, int protfamily);
	static unsigned int sendsyscommand(int command, unsigned char * sys_msg);
	static unsigned int sendsyscommand(int command);
	static unsigned int sendconfcommand(int command, unsigned char * conf_msg);
	static unsigned int sendconfcommand(int command);
	static unsigned int sendprogcommand(int command,int cv, int value, int addr);
	//static void sendpomcommand(int command,int cv, int value, int addr, int protfamily);
	//I keep it public in case a parser wants to send direcly the raw data
	static unsigned int sendrpiccommand(unsigned char buffer[], unsigned char command, unsigned char type,unsigned char commandlength);
	static unsigned char * getresponse();
private:
	static unsigned char checksum(unsigned char toverify[], unsigned char firstbyte, unsigned char lastbyte);
	static bool sendready;
	static unsigned char response[MAXBYTES];
};

#endif /* RPICDGATEWAY_H_ */
