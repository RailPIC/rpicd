/*
 * Config.cpp
 *
 *  Created on: 19/2/2015
 *      Author: mserrano
 */

#include "Config.h"
#include "Logger.h"
#include "Rpi.h"
#include "Webber.h"
#include "RocClient.h"
#include "InitParser.h"
#include "parsers/CommandParser.h"
#include "rpic/commands.h"
#include "Loco.h"
#include "LocoDepot.h"
#include "S88.h"

#include <stdlib.h>
#include "xml/tinyxml.h"

#include <iostream>
#include <string>
#include <stdio.h>  /* defines FILENAME_MAX */


#ifdef WINDOWS
    #include <direct.h>
    #define GetCurrentDir _getcwd

	#include "tools/fakewiringPi.h"
#else
    #include <unistd.h>
    #define GetCurrentDir getcwd
#endif

using namespace std;

//char Config::logpath[20]="rpicd.log";

Config::Config() {
	// TODO Auto-generated constructor stub

}

Config::~Config() {
	// TODO Auto-generated destructor stub
}

shared_ptr<CommandParser> Config::read(string confFile){


	char cCurrentPath[FILENAME_MAX];
	char *result=new char[FILENAME_MAX];

	std::shared_ptr<CommandParser> parsers=nullptr;
	std::shared_ptr<CommandParser> secondparser=nullptr;

	GetCurrentDir(cCurrentPath, sizeof(cCurrentPath));
	//TiXmlDocument doc(CONFIGFILE);
	TiXmlDocument doc(confFile.c_str());

	TiXmlElement *pElem;
	//http://www.grinninglizard.com/tinyxmldocs/tutorial0.html

	TiXmlHandle hDoc(&doc);
	TiXmlHandle hRoot(0);
	if (!doc.LoadFile()) exit(EXIT_FAILURE);

	pElem=hDoc.FirstChildElement().Element();
	if (!pElem) exit(EXIT_FAILURE);			//If there is no first child...exit
	hRoot=TiXmlHandle(pElem);

	if (pElem->Attribute("log")) Logger::logpath =pElem->Attribute("log");
	//if (pElem->Attribute("port")) Webber::s_http_port=pElem->Attribute("port");
	if (pElem->Attribute("port")) Webber::s_http_port=atoi(pElem->Attribute("port"));

	if (confFile.compare(CONFIGFILE)){
		sprintf(result, "Config file in use %s", confFile.c_str());
	}else{
		sprintf(result, "Config file in use %s/%s", cCurrentPath,CONFIGFILE);
	}
	Logger::initLog(result);

	TiXmlElement* pNode=hRoot.FirstChild( "rpiconfig" ).Element();
	if (pNode){
		if (pNode->Attribute("rpiserial")) Rpi::rpiserial =pNode->Attribute("rpiserial");
//		if (pNode->Attribute("rpibaud")) Rpi::rpibaud =atol(pNode->Attribute("rpibaud"));
		if (pNode->Attribute("maxserialtry")) Rpi::maxserialtry =atoi(pNode->Attribute("maxserialtry"));
		if (pNode->Attribute("msecserialtry")) Rpi::usecserialtry =atoi(pNode->Attribute("msecserialtry"))*1000;

		//if (pNode->Attribute("resetpicpin")) Rpi::resetpicpin =atoi(pNode->Attribute("resetpicpin"));
		if (pNode->Attribute("progtrackpin")) Rpi::progtrackpin =atoi(pNode->Attribute("progtrackpin"));
		if (pNode->Attribute("resetoffpin")) Rpi::resetoffpin =atoi(pNode->Attribute("resetoffpin"));
		//if (pNode->Attribute("action1pin")) Rpi::action1pin =atoi(pNode->Attribute("action1pin"));
		//if (pNode->Attribute("action1action")) Rpi::action1action =pNode->Attribute("action1action");
		//if (pNode->Attribute("action1message")) Rpi::action1message =pNode->Attribute("action1message");
		//if (pNode->Attribute("action2pin")) Rpi::action2pin =atoi(pNode->Attribute("action2pin"));
		//if (pNode->Attribute("action2action")) Rpi::action2action =pNode->Attribute("action2action");
		//if (pNode->Attribute("action2message")) Rpi::action2message =pNode->Attribute("action2message");
		if (pNode->Attribute("ledreadyrestpin")) Rpi::ledreadyrestpin =atoi(pNode->Attribute("ledreadyrestpin"));
		if (pNode->Attribute("ledsteadypin")) Rpi::ledsteadypin =atoi(pNode->Attribute("ledsteadypin"));
		if (pNode->Attribute("sec4halt")) Rpi::sec4halt =atoi(pNode->Attribute("sec4halt"));
		//if (pNode->Attribute("ms4resetpic")) Rpi::ms4resetpic =atoi(pNode->Attribute("ms4resetpic"));
		if (pNode->Attribute("sec4resetpic")) Rpi::sec4resetpic =atoi(pNode->Attribute("sec4resetpic"));
	}


	pNode=hRoot.FirstChild( "s88config" ).Element();

	if (pNode){
		if (pNode->Attribute("active")){
			string s88status=pNode->Attribute("active");
			S88::active=!(s88status.compare("true"));
		}
		if (pNode->Attribute("datapin")) S88::datapin =atoi(pNode->Attribute("datapin"));
		if (pNode->Attribute("clkpin")) S88::clkpin =atoi(pNode->Attribute("clkpin"));
		if (pNode->Attribute("loadpin")) S88::loadpin =atoi(pNode->Attribute("loadpin"));
		if (pNode->Attribute("resetpin")) S88::resetpin =atoi(pNode->Attribute("resetpin"));
	}

	/*
	pNode=hRoot.FirstChild( "rocclient" ).Element();

	if (pNode){
		if (pNode->Attribute("IP")) RocClient::IP =pNode->Attribute("IP");
		if (pNode->Attribute("port")) RocClient::port =atol(pNode->Attribute("port"));
		if (pNode->Attribute("parser")) {
			string rocparser=pNode->Attribute("parser");
			RocClient::parser=!(rocparser.compare("true"));
		}
		if (pNode->Attribute("locodepot")) {
			string locodepot=pNode->Attribute("locodepot");
			RocClient::locodepot=!(locodepot.compare("true"));
		}
	}
*/
	pNode=hRoot.FirstChild( "commandparser" ).Element();

	if (pNode){
		if (pNode->Attribute("type")){
			parsers= InitParser::CreateInstance(pNode->Attribute("type"));
			if (pNode->Attribute("port")) parsers->port =atol(pNode->Attribute("port"));
			if (pNode->Attribute("maxclients")) parsers->maxclients =atoi(pNode->Attribute("maxclients"));
			if (pNode->Attribute("locopath")) parsers->locopath =pNode->Attribute("locopath");
			parsers->next=nullptr;
		}
	}

	pNode=hRoot.FirstChild( "logger" ).Element();

	if (pNode){
		if (pNode->Attribute("ping")) Logger::setflag(PING_LOG, atoi(pNode->Attribute("ping")));
		if (pNode->Attribute("status")) Logger::setflag(STATUS_LOG, atoi(pNode->Attribute("status")));
		if (pNode->Attribute("clock")) Logger::setflag(PING_LOG, atoi(pNode->Attribute("clock")));
	}

	pNode=pNode->NextSiblingElement( "commandparser" );

	while (pNode){
		if (pNode->Attribute("type")){
				secondparser= InitParser::CreateInstance(pNode->Attribute("type"));
				if (pNode->Attribute("port")) secondparser->port =atol(pNode->Attribute("port"));
				if (pNode->Attribute("maxclients")) secondparser->maxclients =atoi(pNode->Attribute("maxclients"));
				if (pNode->Attribute("locopath")) parsers->locopath =pNode->Attribute("locopath");
				secondparser->next=nullptr;
				parsers->next=secondparser;
		}
		pNode=pNode->NextSiblingElement( "commandparser" );
	}

	pNode=hRoot.FirstChild( "speedcurve" ).Element();
	if (pNode){
		TiXmlElement* loco=pNode->FirstChildElement( "lc");
		while (loco){
			if (loco->Attribute("id")){
				TiXmlElement* speedcurvestep=loco->FirstChildElement( "speedcurvestep");
				speedstep *speedcurvesteps=nullptr;
				speedstep *lastspeedcurvestep;
				while (speedcurvestep){
					speedstep *currentspeedcurve;
					if ((speedcurvestep->Attribute("step"))&&(speedcurvestep->Attribute("nsleep"))&&(speedcurvestep->Attribute("psleep"))){
						currentspeedcurve=new speedstep{atoi(speedcurvestep->Attribute("step")),atoi(speedcurvestep->Attribute("nsleep")),atoi(speedcurvestep->Attribute("psleep")),nullptr};
						if (speedcurvesteps){
							lastspeedcurvestep->next=currentspeedcurve;
							lastspeedcurvestep=lastspeedcurvestep->next;
						}else{
							speedcurvesteps=currentspeedcurve;	//first value
							lastspeedcurvestep=speedcurvesteps;
						}
					}
					speedcurvestep=speedcurvestep->NextSiblingElement( "speedcurvestep");
				}
				if (speedcurvesteps) LocoDepot::newLoco(loco->Attribute("id"), speedcurvesteps);
			}
			loco=loco->NextSiblingElement( "lc");
		}
	}


	pNode=hRoot.FirstChild( "mmx" ).Element();
	if (pNode){
		TiXmlElement* loco=pNode->FirstChildElement( "lc");
		while (loco){
			if ((loco->Attribute("id"))&&(loco->Attribute("UID"))){
				unsigned char UID[4];
				unsigned long int hexUID=strtoul(loco->Attribute("UID"), NULL, 16);
				if (hexUID){  //Only new UID goes ahead
					for (int i=0;i<4;i++) UID[i]=(hexUID>>(8*i))&0xff;
					LocoDepot::newLoco(loco->Attribute("id"), UID);
				}
			}
			loco=loco->NextSiblingElement( "lc");
		}
	}


	//if ((parsers==nullptr)&&(!RocClient::parser)){
	if (parsers==nullptr){
		Logger::insertLog("ERROR: rpicd needs at least one parser to run");
		exit(EXIT_FAILURE);
	}
	return parsers;
}

void Config::compare(){
	string result;
	Logger::insertLog("********************Configuration***********************");

	if (Logger::logpath.compare(LOGPATH)){
		result="*CUSTOM";
	}else{
		result="DEFAULT";
	};
	Logger::insertLog(result+" LOGPATH: "+Logger::logpath);

	//if (strcmp(Webber::s_http_port, WEBPORT)){
	if (Webber::s_http_port==WEBPORT){
		result="*CUSTOM";
	}else{
		result="DEFAULT";
	};
	Logger::insertLog(result+" WEBPORT: " + std::to_string(Webber::s_http_port));


	if (Rpi::rpiserial.compare(RPISERIAL)){
		result="*CUSTOM";
	}else{
		result="DEFAULT";
	};
	Logger::insertLog(result+" RPISERIAL: "+Rpi::rpiserial);

/*	if (Rpi::rpibaud!=RPIBAUD){
		result="*CUSTOM";
	}else{
		result="DEFAULT";
	};
	Logger::insertLog(result+" RPIBAUD: "+to_string(Rpi::rpibaud));
*/

	if (Rpi::maxserialtry!=MAXSERIALTRY){
			result="*CUSTOM";
		}else{
			result="DEFAULT";
		};
	Logger::insertLog(result+" MAXSERIALTRY: "+to_string(Rpi::maxserialtry));

	if (Rpi::usecserialtry!=USECSERIALTRY/1000){
			result="*CUSTOM";
		}else{
			result="DEFAULT";
		};
	Logger::insertLog(result+" MSECSERIALTRY: "+to_string(Rpi::usecserialtry/1000));

/*
	if (Rpi::resetpicpin!=RESETPICPIN){
			result="*CUSTOM";
		}else{
			result="DEFAULT";
		};
	Logger::insertLog(result+" RESETPICPIN: "+to_string(Rpi::resetpicpin));

	if (Rpi::resetoffpin!=RESETOFFPIN){
			result="*CUSTOM";
		}else{
			result="DEFAULT";
		};
	Logger::insertLog(result+" RESETOFFPIN: "+to_string(Rpi::resetoffpin));

	if (Rpi::action1pin!=ACTION1PIN){
			result="*CUSTOM";
		}else{
			result="DEFAULT";
		};
	Logger::insertLog(result+" ACTION1PIN: "+to_string(Rpi::action1pin));

	if (Rpi::action1action.compare("")) {
		Logger::insertLog("*CUSTOM ACTION 1: "+ Rpi::action1message+" will execute "+Rpi::action1action);
	}else{
		Logger::insertLog("DEFAULT ACTION 1: NO ACTION");
	}

	if (Rpi::action2pin!=ACTION2PIN){
			result="*CUSTOM";
		}else{
			result="DEFAULT";
		};
	Logger::insertLog(result+" ACTION2PIN: "+to_string(Rpi::action2pin));

	if (Rpi::action2action.compare("")) {
		Logger::insertLog("*CUSTOM ACTION 2: "+ Rpi::action2message+" will execute "+Rpi::action2action);
	}else{
		Logger::insertLog("DEFAULT ACTION 2: NO ACTION");
	}
*/
	if (Rpi::ledreadyrestpin!=LEDREADYRESTPIN){
			result="*CUSTOM";
		}else{
			result="DEFAULT";
		};
	Logger::insertLog(result+" LEDREADYRESTPIN: "+to_string(Rpi::ledreadyrestpin));

	if (Rpi::ledsteadypin!=LEDSTEADYPIN){
			result="*CUSTOM";
		}else{
			result="DEFAULT";
		};
	Logger::insertLog(result+" LEDSTEADYPIN: "+to_string(Rpi::ledsteadypin));

	if (Rpi::progtrackpin!=PROGTRACKPIN){
			result="*CUSTOM";
		}else{
			result="DEFAULT";
		};
	Logger::insertLog(result+" PROGTRACKPIN: "+to_string(Rpi::progtrackpin));

	if (Rpi::sec4halt!=SEC4HALT){
			result="*CUSTOM";
		}else{
			result="DEFAULT";
		};
	Logger::insertLog(result+" SEC4HALT: "+to_string(Rpi::sec4halt));

/*	if (Rpi::ms4resetpic!=MS4RESETPIC){
			result="*CUSTOM";
		}else{
			result="DEFAULT";
		};
	Logger::insertLog(result+" MS4RESETPIC: "+to_string(Rpi::ms4resetpic));
*/
	if (Rpi::sec4resetpic!=SEC4RESETPIC){
			result="*CUSTOM";
		}else{
			result="DEFAULT";
		};
	Logger::insertLog(result+" SEC4RESETPIC: "+to_string(Rpi::sec4resetpic));

	/*
	if (RocClient::IP.compare(ROCCLIENTIP)){
		result="*CUSTOM";
	}else{
		result="DEFAULT";
	};
	Logger::insertLog(result+" ROCCLIENTIP: "+RocClient::IP);

	if (RocClient::port!=ROCCLIENTPORT){
		result="*CUSTOM";
	}else{
		result="DEFAULT";
	};
	Logger::insertLog(result+" ROCCLIENTPORT: "+to_string(RocClient::port));

	if (RocClient::parser) {
		Logger::insertLog("*CUSTOM Rocrail parser active");
	}else{
		Logger::insertLog("DEFAULT Rocrail parser not active");
	}
*/
	if (S88::active) {
		Logger::insertLog("*CUSTOM s88 active");

		if (S88::clkpin!=S88CLKPIN){
			result="*CUSTOM";
		}else{
			result="DEFAULT";
		};
		Logger::insertLog(result+" S88CLKPIN: "+to_string(S88::clkpin));

		if (S88::datapin!=S88DATAPIN){
			result="*CUSTOM";
		}else{
			result="DEFAULT";
		};
		Logger::insertLog(result+" S88DATAPIN: "+to_string(S88::datapin));

		if (S88::loadpin!=S88LOADPIN){
			result="*CUSTOM";
		}else{
			result="DEFAULT";
		};
		Logger::insertLog(result+" S88LOADPIN: "+to_string(S88::loadpin));

		if (S88::resetpin!=S88RESETPIN){
			result="*CUSTOM";
		}else{
			result="DEFAULT";
		};
		Logger::insertLog(result+" S88RESETPIN: "+to_string(S88::resetpin));


	}else{
		Logger::insertLog("DEFAULT s88 not active");
	}

}

void Config::printLocoPlan(){
	string result;
	Loco *last;
	speedstep *speedcurve;

	//Probably to me move somewhere else
	Logger::insertLog("**********************Loco Plan*************************");

	last=LocoDepot::getFirst();
	while (last){
		//sprintf(result, "LOCO %s", last->getID());
		result="Loco "+last->getID();
		int count=result.length();
		for (int i=count;i<25;i++) result=result+" ";
		result=result+" A: "+to_string(last->getAddress())+"\tP:";
		switch (last->getProtocol()){
			case LOCOMM1: result=result+"LOCOMM1"; break;
			case LOCOMM2: result=result+"LOCOMM2"; break;
			case LOCOMM3: result=result+"LOCOMM3"; break;
			case LOCOMM4: result=result+"LOCOMM4"; break;
			case LOCOMM5: result=result+"LOCOMM5"; break;
			//case LOCOMMX: result=result+"LOCOMMX"; break;
/*			case LOCOMMX_S3_A7_F4: result=result+"LOCOMMX_S3_A7_F4"; break;
			case LOCOMMX_S3_A7_F8: result=result+"LOCOMMX_S3_A7_F8"; break;
			case LOCOMMX_S3_A7_F16: result=result+"LOCOMMX_S3_A7_F16"; break;
			case LOCOMMX_S3_A9_F4: result=result+"LOCOMMX_S3_A9_F4"; break;
			case LOCOMMX_S3_A9_F8: result=result+"LOCOMMX_S3_A9_F8"; break;
			case LOCOMMX_S3_A9_F16: result=result+"LOCOMMX_S3_A9_F16"; break;
			case LOCOMMX_S3_A11_F4: result=result+"LOCOMMX_S3_A11_F4"; break;
			case LOCOMMX_S3_A11_F8: result=result+"LOCOMMX_S3_A11_F8"; break;
			case LOCOMMX_S3_A11_F16: result=result+"LOCOMMX_S3_A11_F16"; break;
			case LOCOMMX_S3_A14_F4: result=result+"LOCOMMX_S3_A14_F4"; break;
			case LOCOMMX_S3_A14_F8: result=result+"LOCOMMX_S3_A14_F8"; break;
			case LOCOMMX_S3_A14_F16: result=result+"LOCOMMX_S3_A14_F16"; break;
			case LOCOMMX_S7_A7_F4: result=result+"LOCOMMX_S7_A7_F4"; break;
			case LOCOMMX_S7_A7_F8: result=result+"LOCOMMX_S7_A7_F8"; break;
			case LOCOMMX_S7_A7_F16: result=result+"LOCOMMX_S7_A7_F16"; break;
			case LOCOMMX_S7_A9_F4: result=result+"LOCOMMX_S7_A9_F4"; break;
			case LOCOMMX_S7_A9_F8: result=result+"LOCOMMX_S7_A9_F8"; break;
			case LOCOMMX_S7_A9_F16: result=result+"LOCOMMX_S7_A9_F16"; break;
			case LOCOMMX_S7_A11_F4: result=result+"LOCOMMX_S7_A11_F4"; break;
			case LOCOMMX_S7_A11_F8: result=result+"LOCOMMX_S7_A11_F8"; break;
			case LOCOMMX_S7_A11_F16: result=result+"LOCOMMX_S7_A11_F16"; break;
			case LOCOMMX_S7_A14_F4: result=result+"LOCOMMX_S7_A14_F4"; break;
			case LOCOMMX_S7_A14_F8: result=result+"LOCOMMX_S7_A14_F8"; break;
			case LOCOMMX_S7_A14_F16: result=result+"LOCOMMX_S7_A14_F16"; break;
	*/		case LOCODCCN14: result=result+"LOCODCCN14"; break;
			case LOCODCCN28: result=result+"LOCODCCN28"; break;
			case LOCODCCN128: result=result+"LOCODCCN128"; break;
			case LOCODCCL14: result=result+"LOCODCCN14"; break;
			case LOCODCCL28: result=result+"LOCODCCN28"; break;
			case LOCODCCL128: result=result+"LOCODCCN128"; break;
			case LOCOMMX_F4: result=result+"LOCOMMX_F4"; break;
			case LOCOMMX_F8: result=result+"LOCOMMX_F8"; break;
			case LOCOMMX_F16: result=result+"LOCOMMX_F16"; break;
			case LOCOMMX_FN: result=result+"LOCOMMX_FN"; break;
			default: result=result+"NOT DEFINED";
		}
		if (last->getProtocol()>LOCODCCN128){
			//mmx locos
			char hex[3];
			unsigned char *UID=last->getmmxUID();
			result=result+"\tUID: 0x";
			for (int i = 0; i < 4; i++) {
				//	printf("Command sent buffer %d: %c %X \n", i, buffer[i],buffer[i]);
				sprintf(hex,"%.2X", *(UID+3-i));
				result=result+hex+" ";
			}
		}
		Logger::insertLog(result);
		speedcurve=last->getSpeedCurve();
		while (speedcurve){
			result=" step: "+to_string(speedcurve->step)+" up sleep: "+to_string(speedcurve->speedup)+" down sleep: "+to_string(speedcurve->speedown);
			Logger::insertLog(result);
			speedcurve=speedcurve->next;
		}
		last=last->next;
	}
	Logger::insertLog("********************************************************");
}

