/*
 * Webber.cpp
 *
 *  Created on: 10/05/2016
 *      Author: agonia
 */

#include "Webber.h"
#include "Config.h"
#include "Logger.h"
#include "errors.h"
#include "Loco.h"
#include "LocoDepot.h"
#include "RocClient.h"
#include "parsers/mcs2Parser.h"
#include "rpic/commands.h"
#include "RpicdGateway.h"
#include "Rpi.h"
extern "C" {
#include "web/mongoose.h"
}

#ifdef WINDOWS
#include <direct.h>
#define GetCurrentDir _getcwd

#include "tools/fakewiringPi.h"
#else
#include <unistd.h>
#define GetCurrentDir getcwd
#endif

//const char *Webber::s_http_port = WEBPORT;
int Webber::s_http_port = WEBPORT;
sig_atomic_t Webber::s_signal_received = 0;
mg_serve_http_opts Webber::s_http_server_opts;
mcs2Parser *Webber::mcs24MMX;
struct mg_connection *Webber::nc;
bool Webber::init = false;
bool Webber::delayed = false;
unsigned char Webber::bufferindex = 0;
string Webber::messages[MAXMESSAGEBUFFER];

Webber::Webber() {
	mcs24MMX = new mcs2Parser;

	char cCurrentPath[FILENAME_MAX];
	GetCurrentDir(cCurrentPath, sizeof(cCurrentPath));
	char *result = new char[FILENAME_MAX];
	sprintf(result, "%s/%s", cCurrentPath, "web_root");

	struct mg_mgr mgr;

	mg_mgr_init(&mgr, NULL);
	//nc = mg_bind(&mgr, s_http_port, ev_handler);
	nc = mg_bind(&mgr, std::to_string(s_http_port).c_str(), ev_handler);
	mg_set_protocol_http_websocket(nc);
	s_http_server_opts.document_root = result;
	s_http_server_opts.auth_domain = "example.com";

	Logger::insertLog(
			"********************************************************");
	Logger::insertLog("WEBBER: Init Logger on port " + std::to_string(s_http_port));
	Logger::insertLog(
			"********************************************************");
	init = true;
	//Accepting messages
	for (;;) {
		mg_mgr_poll(&mgr, 1000);
	}
	mg_mgr_free(&mgr);
}

Webber::~Webber() {
	// TODO Auto-generated destructor stub
}

void Webber::handle_lok_discovery(struct mg_connection *nc,
		struct http_message *hm) {

	int result;

	/* Send response Independant of execution... response so far is OK.
	 * It can not be after logger::insertlg, since logger is sending a new message*/
	mg_printf(nc, "HTTP/1.1 200 OK\r\nContent-Length: %lu\r\n\r\n%.*s",
			(unsigned long) hm->body.len, (int) hm->body.len, hm->body.p);

//	result=mcs24MMX->lok_discoveryCMD(NULL); //All commands algorithms... to be reviewd

	mcs2Parser *tempoMCS2=new mcs2Parser;
	result=tempoMCS2->lok_discoveryCMD(NULL);
/*	unsigned char sys_message[6];
	sys_message[0] = 6;
	sys_message[1] = 0;
	sys_message[2] = 0;
	sys_message[3] = 0;
	sys_message[4] = 0;
	sys_message[5] = 0;
	result = RpicdGateway::sendsyscommand(DISCOVERY, sys_message);
*/
	if (result == OK) {
		Logger::insertLog("WEBBER: Lok Discovery succesfully sent");
	}
}

void Webber::handle_cycleON(struct mg_connection *nc, struct http_message *hm) {
	int result;

	mg_printf(nc, "HTTP/1.1 200 OK\r\nContent-Length: %lu\r\n\r\n%.*s",
			(unsigned long) hm->body.len, (int) hm->body.len, hm->body.p);

	//result = RpicdGateway::sendsyscommand(CYCLEON, NULL);
	result = RpicdGateway::sendsyscommand(CYCLEON);
	Rpi::setOUTsteady();
	if (result == OK) {
		Logger::insertLog("WEBBER: CYCLEON succesfully sent");
	}
}
void Webber::handle_cycleOFF(struct mg_connection *nc,
		struct http_message *hm) {
	int result;
	/* Send response Independant of execution... response so far is OK.
	 * It can not be after logger::insertlg, since logger is sending a new message*/
	mg_printf(nc, "HTTP/1.1 200 OK\r\nContent-Length: %lu\r\n\r\n%.*s",
			(unsigned long) hm->body.len, (int) hm->body.len, hm->body.p);

	//result = RpicdGateway::sendsyscommand(CYCLEOFF, NULL);
	result = RpicdGateway::sendsyscommand(CYCLEOFF);
	if (result == OK) {
		Logger::insertLog("WEBBER: CYCLEOFF succesfully sent");
	}
}
void Webber::handle_testON(struct mg_connection *nc, struct http_message *hm) {
	int result;
	/* Send response Independant of execution... response so far is OK.
	 * It can not be after logger::insertlg, since logger is sending a new message*/
	mg_printf(nc, "HTTP/1.1 200 OK\r\nContent-Length: %lu\r\n\r\n%.*s",
			(unsigned long) hm->body.len, (int) hm->body.len, hm->body.p);

	//result = RpicdGateway::sendconfcommand(TESTON, NULL);
	result = RpicdGateway::sendconfcommand(TESTON);
	if (result == OK) {
		Logger::insertLog("WEBBER: TESTON succesfully sent");
	}
}

void Webber::handle_testOFF(struct mg_connection *nc, struct http_message *hm) {
	int result;
	/* Send response Independant of execution... response so far is OK.
	 * It can not be after logger::insertlg, since logger is sending a new message*/
	mg_printf(nc, "HTTP/1.1 200 OK\r\nContent-Length: %lu\r\n\r\n%.*s",
			(unsigned long) hm->body.len, (int) hm->body.len, hm->body.p);

	//result = RpicdGateway::sendconfcommand(TESTOFF, NULL);
	result = RpicdGateway::sendconfcommand(TESTOFF);
	if (result == OK) {
		Logger::insertLog("WEBBER: TESTOFF succesfully sent");
	}
}

void Webber::handle_PTON(struct mg_connection *nc, struct http_message *hm) {
//	int result;
	/* Send response Independant of execution... response so far is OK.
	 * It can not be after logger::insertlg, since logger is sending a new message*/
	mg_printf(nc, "HTTP/1.1 200 OK\r\nContent-Length: %lu\r\n\r\n%.*s",
			(unsigned long) hm->body.len, (int) hm->body.len, hm->body.p);

	//result = RpicdGateway::sendsyscommand(PTON, NULL);
	Rpi::setprogtrack();
//	if (result == OK) {
		//Logger::insertLog("WEBBER: PTON succesfully sent");
	Logger::insertLog("WEBBER: Prog. TRACK ON");
//	}
}

void Webber::handle_PTOFF(struct mg_connection *nc, struct http_message *hm) {
//	int result;
	/* Send response Independant of execution... response so far is OK.
	 * It can not be after logger::insertlg, since logger is sending a new message*/
	mg_printf(nc, "HTTP/1.1 200 OK\r\nContent-Length: %lu\r\n\r\n%.*s",
			(unsigned long) hm->body.len, (int) hm->body.len, hm->body.p);

	//result = RpicdGateway::sendsyscommand(PTOFF, NULL);
	Rpi::clearprogtrack();//
//	if (result == OK) {
//		Logger::insertLog("WEBBER: PTOFF succesfully sent");
//	}

	Logger::insertLog("WEBBER: Prog. TRACK OFF");
}

void Webber::handle_RESET(struct mg_connection *nc, struct http_message *hm) {

	/* Send response Independant of execution... response so far is OK.
	 * It can not be after logger::insertlg, since logger is sending a new message*/
	mg_printf(nc, "HTTP/1.1 200 OK\r\nContent-Length: %lu\r\n\r\n%.*s",
			(unsigned long) hm->body.len, (int) hm->body.len, hm->body.p);

	//result=RpicdGateway::sendsyscommand(PTOFF,NULL);
	Logger::insertLog("WEBBER: Reset PIC requested");
	Rpi::setOUTiddle();
	Rpi::setpicready();
	LocoDepot::initMMX();
}

void Webber::handle_VERSION(struct mg_connection *nc, struct http_message *hm) {

	int result;
	string jsonResponse="";

	/* Send headers */
	mg_printf(nc, "%s",	"HTTP/1.1 200 OK\r\nTransfer-Encoding: chunked\r\n\r\n");

	delayed = true;
	//result = RpicdGateway::sendconfcommand(VERSION_READ, NULL);
	result = RpicdGateway::sendconfcommand(VERSION_READ);
	if (result == OK) {
		Logger::insertLog("WEBBER: VERSION sent OK");
		unsigned char *UID = RpicdGateway::getresponse();
		jsonResponse="{\"ver\":\""+std::to_string(*(UID + 1))+"."+std::to_string(*(UID + 2))+ "\"}";
	}

	mg_printf_http_chunk(nc, jsonResponse.c_str());
	mg_send_http_chunk(nc, "", 0); /* Send empty chunk, the end of response */

	delayed = false;
	Logger::insertLog("WEBBER: getVersion sent");

}

void Webber::handle_UC(struct mg_connection *nc, struct http_message *hm) {

	int result;
	string jsonResponse="";

	/* Send headers */
	mg_printf(nc, "%s",	"HTTP/1.1 200 OK\r\nTransfer-Encoding: chunked\r\n\r\n");

	delayed = true;
	//result = RpicdGateway::sendconfcommand(VERSION_READ, NULL);
	result = RpicdGateway::sendconfcommand(UCONTROLLER);
	if (result == OK) {
		Logger::insertLog("WEBBER: UCONTROLLER sent OK");
		unsigned char *UID = RpicdGateway::getresponse();
		jsonResponse="{\"uc\":"+std::to_string(*(UID + 1))+"}";
	}

	mg_printf_http_chunk(nc, jsonResponse.c_str());
	mg_send_http_chunk(nc, "", 0); /* Send empty chunk, the end of response */

	delayed = false;
	Logger::insertLog("WEBBER: getuController sent");

}

void Webber::handle_mmxLOAD(struct mg_connection *nc, struct http_message *hm) {
//	char n1[100], n2[100];
	string mmxlocos = "";
	char hex[3];

	/* Get form variables */
//	mg_get_http_var(&hm->body, "n1", n1, sizeof(n1));
//	mg_get_http_var(&hm->body, "n2", n2, sizeof(n2));

	/* Send headers */
	mg_printf(nc, "%s",	"HTTP/1.1 200 OK\r\nTransfer-Encoding: chunked\r\n\r\n");

	Loco *last = LocoDepot::getFirst();
	while (last) {
		if (last->getProtocolFamily() == LOCMMXCMD) {
			mmxlocos = mmxlocos + "{\"ID\":\"" + last->getID() + "\",\"A\":"
					+ std::to_string(last->getAddress()) + ",\"UID\":\"0x";

			unsigned char *UID = last->getmmxUID();
			for (int i = 0; i < 4; i++) {
				sprintf(hex, "%.2X", *(UID + 3 - i));
				mmxlocos = mmxlocos + hex;
			}
			mmxlocos = mmxlocos + "\"},";
		}
		last = last->next;
	}

	if (mmxlocos.compare("")) {
		mmxlocos.erase(mmxlocos.size() - 1); //remove extra ","
		mmxlocos = "{\"locos\": [" + mmxlocos + "]}";
	}

	mg_printf_http_chunk(nc, mmxlocos.c_str());
	mg_send_http_chunk(nc, "", 0); /* Send empty chunk, the end of response */
}

void Webber::handle_mmxBIND(struct mg_connection *nc, struct http_message *hm) {
	char n1[100], n2[100];
	int result = OK;
	string response = "{\"result\":\"ERROR\"}";

	/* Get form variables */
	mg_get_http_var(&hm->body, "ID", n1, sizeof(n1));
	mg_get_http_var(&hm->body, "A", n2, sizeof(n2));

	string ID = string(n1);
	int addr = atoi(n2);

	Loco *loc = LocoDepot::findLoco(ID);
	unsigned char *UID = loc->getmmxUID();

	//In the future use same procedure to build the bind coming from external message
	//result=mcs24MMX->lok_discoveryCMD(NULL);

	unsigned char sys_message[7];
	sys_message[0] = 7;
	sys_message[1] = (addr >> 8) & 0xff;
	sys_message[2] = addr & 0xff;
	;
	for (int i = 0; i < 4; i++) {
		sys_message[3 + i] = *(UID + 3 - i);
	}

	delayed = true;

	result = RpicdGateway::sendsyscommand(BIND, sys_message);

	if (result == OK) {
		Logger::insertLog("WEBBER: Bind Lok OK");
		response = "{\"result\":\"OK\"}";
		loc->setAddress(addr);
	}

	/* Send headers */

	mg_printf(nc, "%s",	"HTTP/1.1 200 OK\r\nTransfer-Encoding: chunked\r\n\r\n");
	mg_printf_http_chunk(nc, response.c_str());
	mg_send_http_chunk(nc, "", 0); /* Send empty chunk, the end of response */
	delayed = false;
	Logger::insertLog("WEBBER: Bind Lok sent");
}


void Webber::handle_mmxPING(struct mg_connection *nc, struct http_message *hm) {
	char n1[100], n2[100];
	int result = OK;
	string response = "{\"result\":\"ERROR\"}";

	/* Get form variables */
	mg_get_http_var(&hm->body, "ID", n1, sizeof(n1));
	mg_get_http_var(&hm->body, "A", n2, sizeof(n2));

	string ID = string(n1);
	int addr = atoi(n2);

	Loco *loc = LocoDepot::findLoco(ID);
	unsigned char *UID = loc->getmmxUID();

	//In the future use same procedure to build the bind coming from external message
	//result=mcs24MMX->lok_discoveryCMD(NULL);

	unsigned char sys_message[7];
	sys_message[0] = 7;
	sys_message[1] = (addr >> 8) & 0xff;
	sys_message[2] = addr & 0xff;
	;
	for (int i = 0; i < 4; i++) {
		sys_message[3 + i] = *(UID + 3 - i);
	}

	delayed = true;

	result = RpicdGateway::sendsyscommand(PING, sys_message);

	if (result == OK) {
		Logger::insertLog("WEBBER: PING Lok OK");
		response = "{\"result\":\"OK\"}";
		//loc->setAddress(addr);
	}

	/* Send headers */

	mg_printf(nc, "%s",	"HTTP/1.1 200 OK\r\nTransfer-Encoding: chunked\r\n\r\n");
	mg_printf_http_chunk(nc, response.c_str());
	mg_send_http_chunk(nc, "", 0); /* Send empty chunk, the end of response */
	delayed = false;
	Logger::insertLog("WEBBER: PING Lok sent");
}


void Webber::handle_getUID(struct mg_connection *nc, struct http_message *hm) {

	char hex[3];
	int result;
	string jsonResponse="";

	/* Send headers */
	mg_printf(nc, "%s",	"HTTP/1.1 200 OK\r\nTransfer-Encoding: chunked\r\n\r\n");

	delayed = true;
	//result = RpicdGateway::sendconfcommand(ZENTRAL_READ, NULL);
	result = RpicdGateway::sendconfcommand(ZENTRAL_READ);
	if (result == OK) {
		Logger::insertLog("WEBBER: getUID sent OK");
		unsigned char *UID = RpicdGateway::getresponse();
		jsonResponse="{\"UID\":\"0x";
		for (int i = 0; i < 4; i++) {
			//sprintf(hex, "%.2X", *(UID + 4 - i));	//UID 0 is checksum... the rest is the message?
			sprintf(hex, "%.2X", *(UID + i+1));	//UID 0 is checksum... the rest is the message?
			jsonResponse = jsonResponse + hex;
		}
		jsonResponse = jsonResponse + "\"}";
	}

	mg_printf_http_chunk(nc, jsonResponse.c_str());
	mg_send_http_chunk(nc, "", 0); /* Send empty chunk, the end of response */

	delayed = false;
	Logger::insertLog("WEBBER: getUID sent");
}

void Webber::handle_setUID(struct mg_connection *nc, struct http_message *hm) {

//WIP olf script from load---

	char n1[100];
	int result = OK;
	string response = "{\"result\":\"ERROR\"}";

	/* Get form variables */
	mg_get_http_var(&hm->body, "UID", n1, sizeof(n1));

	unsigned long int hexUID = strtoul(n1, NULL, 16);;

	/* Send headers */
	mg_printf(nc, "%s",	"HTTP/1.1 200 OK\r\nTransfer-Encoding: chunked\r\n\r\n");

	delayed = true;


	unsigned char sys_message[5];
	sys_message[0]=5;
	for (int i=0;i<4;i++) sys_message[3-i+1]=(hexUID>>(8*i))&0xff;


	result = RpicdGateway::sendconfcommand(ZENTRAL_WRITE, sys_message);
	if (result == OK) {
		Logger::insertLog("WEBBER: setUID OK");
		response = "{\"result\":\"OK\"}";
		//loc->setAddress(addr);
	}

	/* Send headers */

	mg_printf(nc, "%s",	"HTTP/1.1 200 OK\r\nTransfer-Encoding: chunked\r\n\r\n");
	mg_printf_http_chunk(nc, response.c_str());
	mg_send_http_chunk(nc, "", 0); /* Send empty chunk, the end of response */
	delayed = false;
	Logger::insertLog("WEBBER: setUID sent");
	Logger::insertLog("WEBBER: ******Note: setting a new UID requires rpicd and rpic to reset. Probably SW control will also need to reregister rpic");
}


void Webber::handle_getNS(struct mg_connection *nc, struct http_message *hm) {

	int result;
	string jsonResponse="";

	/* Send headers */
	mg_printf(nc, "%s",	"HTTP/1.1 200 OK\r\nTransfer-Encoding: chunked\r\n\r\n");

	delayed = true;
	//result = RpicdGateway::sendconfcommand(SESSION_N_READ, NULL);
	result = RpicdGateway::sendconfcommand(SESSION_N_READ);
	if (result == OK) {
		Logger::insertLog("WEBBER: getNS sent OK");
		unsigned char *UID = RpicdGateway::getresponse();
		jsonResponse="{\"NS\":"+std::to_string((*(UID+1))*128+*(UID+2))+ "}";
	}

	mg_printf_http_chunk(nc, jsonResponse.c_str());
	mg_send_http_chunk(nc, "", 0); /* Send empty chunk, the end of response */

	delayed = false;
	Logger::insertLog("WEBBER: getNS sent");
}

void Webber::handle_setNS(struct mg_connection *nc, struct http_message *hm) {

//WIP olf script from load---

	char n1[100];
	int result = OK;
	string response = "{\"result\":\"ERROR\"}";

	/* Get form variables */
	mg_get_http_var(&hm->body, "NS", n1, sizeof(n1));

	unsigned int NS = strtoul(n1, NULL, 10);;

	/* Send headers */
	mg_printf(nc, "%s",	"HTTP/1.1 200 OK\r\nTransfer-Encoding: chunked\r\n\r\n");

	delayed = true;


	unsigned char sys_message[3];
	sys_message[0] = 3;
	sys_message[1] = (NS >> 8) & 0xff;
	sys_message[2] = NS & 0xff;


	result = RpicdGateway::sendconfcommand(SESSION_N_WRITE, sys_message);
	if (result == OK) {
		Logger::insertLog("WEBBER: setNS OK");
		response = "{\"result\":\"OK\"}";
		//loc->setAddress(addr);
	}

	/* Send headers */

	mg_printf(nc, "%s",	"HTTP/1.1 200 OK\r\nTransfer-Encoding: chunked\r\n\r\n");
	mg_printf_http_chunk(nc, response.c_str());
	mg_send_http_chunk(nc, "", 0); /* Send empty chunk, the end of response */
	delayed = false;
	Logger::insertLog("WEBBER: setNS sent");
}

void Webber::handle_getEnable(struct mg_connection *nc, struct http_message *hm) {

	int result;
	string jsonResponse="";

	/* Send headers */
	mg_printf(nc, "%s",	"HTTP/1.1 200 OK\r\nTransfer-Encoding: chunked\r\n\r\n");

	delayed = true;
	//result = RpicdGateway::sendconfcommand(SESSION_N_READ, NULL);
	result = RpicdGateway::sendconfcommand(ENABLEPROT_READ);
	if (result == OK) {
		Logger::insertLog("WEBBER: getEnable sent OK");
		unsigned char *UID = RpicdGateway::getresponse();
		jsonResponse="{\"Enable\":"+std::to_string((*(UID+1)))+ "}";
	}

	mg_printf_http_chunk(nc, jsonResponse.c_str());
	mg_send_http_chunk(nc, "", 0); /* Send empty chunk, the end of response */

	delayed = false;
	Logger::insertLog("WEBBER: getEnable sent");
}

void Webber::handle_setEnable(struct mg_connection *nc, struct http_message *hm) {

//WIP olf script from load---

	char n1[100];
	int result = OK;
	string response = "{\"result\":\"ERROR\"}";

	/* Get form variables */
	mg_get_http_var(&hm->body, "Enable", n1, sizeof(n1));

	unsigned int Enable = strtoul(n1, NULL, 10);;

	/* Send headers */
	mg_printf(nc, "%s",	"HTTP/1.1 200 OK\r\nTransfer-Encoding: chunked\r\n\r\n");

	delayed = true;


	unsigned char sys_message[2];
	sys_message[0] = 2;
	sys_message[1] = Enable;

	result = RpicdGateway::sendconfcommand(ENABLEPROT_WRITE, sys_message);
	if (result == OK) {
		Logger::insertLog("WEBBER: setEnable OK");
		response = "{\"result\":\"OK\"}";
		//loc->setAddress(addr);
	}

	/* Send headers */

	mg_printf(nc, "%s",	"HTTP/1.1 200 OK\r\nTransfer-Encoding: chunked\r\n\r\n");
	mg_printf_http_chunk(nc, response.c_str());
	mg_send_http_chunk(nc, "", 0); /* Send empty chunk, the end of response */
	delayed = false;
	Logger::insertLog("WEBBER: setEnable sent");
}

void Webber::handle_getConfig(struct mg_connection *nc, struct http_message *hm) {

	int result;
	string jsonResponse="";

	/* Send headers */
	mg_printf(nc, "%s",	"HTTP/1.1 200 OK\r\nTransfer-Encoding: chunked\r\n\r\n");

	delayed = true;
	//result = RpicdGateway::sendconfcommand(SESSION_N_READ, NULL);
	result = RpicdGateway::sendconfcommand(CONFIGRAIL_READ);
	if (result == OK) {
		Logger::insertLog("WEBBER: getConfig sent OK");
		unsigned char *UID = RpicdGateway::getresponse();
		jsonResponse="{\"Config\":"+std::to_string((*(UID+1)))+ "}";
	}

	mg_printf_http_chunk(nc, jsonResponse.c_str());
	mg_send_http_chunk(nc, "", 0); /* Send empty chunk, the end of response */

	delayed = false;
	Logger::insertLog("WEBBER: getConfig sent");
}

void Webber::handle_setConfig(struct mg_connection *nc, struct http_message *hm) {

//WIP olf script from load---

	char n1[100];
	int result = OK;
	string response = "{\"result\":\"ERROR\"}";

	/* Get form variables */
	mg_get_http_var(&hm->body, "Config", n1, sizeof(n1));

	unsigned int Config = strtoul(n1, NULL, 10);;

	/* Send headers */
	mg_printf(nc, "%s",	"HTTP/1.1 200 OK\r\nTransfer-Encoding: chunked\r\n\r\n");

	delayed = true;


	unsigned char sys_message[2];
	sys_message[0] = 2;
	sys_message[1] = Config;

	result = RpicdGateway::sendconfcommand(CONFIGRAIL_WRITE, sys_message);
	if (result == OK) {
		Logger::insertLog("WEBBER: setEConfig OK");
		response = "{\"result\":\"OK\"}";
		//loc->setAddress(addr);
	}

	/* Send headers */

	mg_printf(nc, "%s",	"HTTP/1.1 200 OK\r\nTransfer-Encoding: chunked\r\n\r\n");
	mg_printf_http_chunk(nc, response.c_str());
	mg_send_http_chunk(nc, "", 0); /* Send empty chunk, the end of response */
	delayed = false;
	Logger::insertLog("WEBBER: setEnable sent");
}

void Webber::handle_pingLOGON(struct mg_connection *nc, struct http_message *hm) {

	mg_printf(nc, "HTTP/1.1 200 OK\r\nContent-Length: %lu\r\n\r\n%.*s",
			(unsigned long) hm->body.len, (int) hm->body.len, hm->body.p);

	Logger::setflag(PING_LOG, true);
	Logger::insertLog("WEBBER: activating PING LOG");
}

void Webber::handle_pingLOGOFF(struct mg_connection *nc, struct http_message *hm) {

	mg_printf(nc, "HTTP/1.1 200 OK\r\nContent-Length: %lu\r\n\r\n%.*s",
			(unsigned long) hm->body.len, (int) hm->body.len, hm->body.p);

	Logger::setflag(PING_LOG, false);
	Logger::insertLog("WEBBER: deactivating PING LOG");
}

void Webber::handle_statusLOGON(struct mg_connection *nc, struct http_message *hm) {

	mg_printf(nc, "HTTP/1.1 200 OK\r\nContent-Length: %lu\r\n\r\n%.*s",
			(unsigned long) hm->body.len, (int) hm->body.len, hm->body.p);

	Logger::setflag(STATUS_LOG, true);
	Logger::insertLog("WEBBER: activating STATUS LOG");
}

void Webber::handle_statusLOGOFF(struct mg_connection *nc, struct http_message *hm) {

	mg_printf(nc, "HTTP/1.1 200 OK\r\nContent-Length: %lu\r\n\r\n%.*s",
			(unsigned long) hm->body.len, (int) hm->body.len, hm->body.p);

	Logger::setflag(STATUS_LOG, false);
	Logger::insertLog("WEBBER: deactivating STATUS LOG");
}

void Webber::handle_clockLOGON(struct mg_connection *nc, struct http_message *hm) {

	mg_printf(nc, "HTTP/1.1 200 OK\r\nContent-Length: %lu\r\n\r\n%.*s",
			(unsigned long) hm->body.len, (int) hm->body.len, hm->body.p);

	Logger::setflag(CLOCK_LOG, true);
	Logger::insertLog("WEBBER: activating CLOCK LOG");
}

void Webber::handle_clockLOGOFF(struct mg_connection *nc, struct http_message *hm) {

	mg_printf(nc, "HTTP/1.1 200 OK\r\nContent-Length: %lu\r\n\r\n%.*s",
			(unsigned long) hm->body.len, (int) hm->body.len, hm->body.p);

	Logger::setflag(CLOCK_LOG, false);
	Logger::insertLog("WEBBER: deactivating CLOCK LOG");
}
/*
void Webber::handle_LOKSYNC(struct mg_connection *nc, struct http_message *hm) {

	mg_printf(nc, "HTTP/1.1 200 OK\r\nContent-Length: %lu\r\n\r\n%.*s",
			(unsigned long) hm->body.len, (int) hm->body.len, hm->body.p);

	Logger::insertLog("WEBBER: Requesting Loc. sync with Rocrail");
	RocClient::lokSync();

}

void Webber::handle_CARSYNC(struct mg_connection *nc, struct http_message *hm) {

	mg_printf(nc, "HTTP/1.1 200 OK\r\nContent-Length: %lu\r\n\r\n%.*s",
			(unsigned long) hm->body.len, (int) hm->body.len, hm->body.p);

	Logger::insertLog("WEBBER: Requesting car sync with Rocrail");
	RocClient::carSync();

}
*/

void Webber::ev_handler(struct mg_connection *nc, int ev, void *ev_data) {
	struct http_message *hm = (struct http_message *) ev_data;

	switch (ev) {
	case MG_EV_HTTP_REQUEST:
		if (mg_vcmp(&hm->uri, "/lok_discovery") == 0) {
			handle_lok_discovery(nc, hm); /* Handle RESTful call */
		} else if (mg_vcmp(&hm->uri, "/cycleON") == 0) {
			handle_cycleON(nc, hm);
		} else if (mg_vcmp(&hm->uri, "/cycleOFF") == 0) {
			handle_cycleOFF(nc, hm);
		} else if (mg_vcmp(&hm->uri, "/testON") == 0) {
			handle_testON(nc, hm);
		} else if (mg_vcmp(&hm->uri, "/testOFF") == 0) {
			handle_testOFF(nc, hm);
		} else if (mg_vcmp(&hm->uri, "/PTON") == 0) {
			handle_PTON(nc, hm);
		} else if (mg_vcmp(&hm->uri, "/PTOFF") == 0) {
			handle_PTOFF(nc, hm);
		} else if (mg_vcmp(&hm->uri, "/RESET") == 0) {
			handle_RESET(nc, hm);
		} else if (mg_vcmp(&hm->uri, "/VERSION") == 0) {
			handle_VERSION(nc, hm);
		} else if (mg_vcmp(&hm->uri, "/UC") == 0) {
			handle_UC(nc, hm);
		} else if (mg_vcmp(&hm->uri, "/mmxLOAD") == 0) {
			handle_mmxLOAD(nc, hm);
		} else if (mg_vcmp(&hm->uri, "/mmxBIND") == 0) {
			handle_mmxBIND(nc, hm);
		} else if (mg_vcmp(&hm->uri, "/mmxPING") == 0) {
			handle_mmxPING(nc, hm);
		} else if (mg_vcmp(&hm->uri, "/getUID") == 0) {
			handle_getUID(nc, hm);
		} else if (mg_vcmp(&hm->uri, "/setUID") == 0) {
			handle_setUID(nc, hm);
		} else if (mg_vcmp(&hm->uri, "/getNS") == 0) {
			handle_getNS(nc, hm);
		} else if (mg_vcmp(&hm->uri, "/setNS") == 0) {
			handle_setNS(nc, hm);
		} else if (mg_vcmp(&hm->uri, "/getEnable") == 0) {
			handle_getEnable(nc, hm);
		} else if (mg_vcmp(&hm->uri, "/setEnable") == 0) {
			handle_setEnable(nc, hm);
		} else if (mg_vcmp(&hm->uri, "/getConfig") == 0) {
			handle_getConfig(nc, hm);
		} else if (mg_vcmp(&hm->uri, "/setConfig") == 0) {
			handle_setConfig(nc, hm);
		} else if (mg_vcmp(&hm->uri, "/pingLOGON") == 0) {
			handle_pingLOGON(nc, hm);
		} else if (mg_vcmp(&hm->uri, "/pingLOGOFF") == 0) {
			handle_pingLOGOFF(nc, hm);
		} else if (mg_vcmp(&hm->uri, "/statusLOGON") == 0) {
			handle_statusLOGON(nc, hm);
		} else if (mg_vcmp(&hm->uri, "/statusLOGOFF") == 0) {
			handle_statusLOGOFF(nc, hm);
		} else if (mg_vcmp(&hm->uri, "/clockLOGON") == 0) {
			handle_clockLOGON(nc, hm);
		} else if (mg_vcmp(&hm->uri, "/clockLOGOFF") == 0) {
			handle_clockLOGOFF(nc, hm);
//		} else if (mg_vcmp(&hm->uri, "/LOKSYNC") == 0) {
//			handle_LOKSYNC(nc, hm);
//		} else if (mg_vcmp(&hm->uri, "/CARSYNC") == 0) {
//			handle_CARSYNC(nc, hm);
		} else {
			mg_serve_http(nc, hm, s_http_server_opts); /* Serve static content */
		}
		break;
	case MG_EV_WEBSOCKET_HANDSHAKE_DONE: {
		char addr[32];
		mg_sock_addr_to_str(&nc->sa, addr, sizeof(addr),
		MG_SOCK_STRINGIFY_IP | MG_SOCK_STRINGIFY_PORT);
		Logger::insertLog("WEBBER: Serving information to " + string(addr));

		char buf[500] = "Welcome";

		FILE *weblog = Logger::getReader();

		while (fgets(buf, sizeof buf, weblog) != NULL) // read a line
		{
			mg_send_websocket_frame(nc, WEBSOCKET_OP_TEXT, buf, strlen(buf));
		}
		fclose(weblog);
		break;
	}
	case MG_EV_CLOSE: {
		if (is_websocket(nc)) {
			char addr[32];
			mg_sock_addr_to_str(&nc->sa, addr, sizeof(addr),
			MG_SOCK_STRINGIFY_IP | MG_SOCK_STRINGIFY_PORT);
			Logger::insertLog(
					"WEBBER: Stop serving information to " + string(addr));
		}
		break;
	}
	default:
		break;
	}
}

int Webber::is_websocket(const struct mg_connection *nc) {
	return nc->flags & MG_F_IS_WEBSOCKET;
}

void Webber::broadcast(struct mg_connection *nc, const struct mg_str msg) {

	struct mg_connection *c;
	char buf[500];
	snprintf(buf, sizeof(buf), "%.*s", (int) msg.len, msg.p);
	for (c = mg_next(nc->mgr, NULL); c != NULL; c = mg_next(nc->mgr, c)) {
		mg_send_websocket_frame(c, WEBSOCKET_OP_TEXT, buf, strlen(buf));
	}
}

void Webber::inserWebLog(string message) {
	messages[bufferindex] = message;
	if (init)
		bufferindex++;

	string test = messages[bufferindex];

	if ((!delayed) && init) {
		do {
			struct mg_str d = { messages[bufferindex - 1].c_str(),
					messages[bufferindex - 1].length() };
			bufferindex--;
			broadcast(nc, d);
		} while (bufferindex);
	}
}

