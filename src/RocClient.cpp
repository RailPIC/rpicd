/*
 * RocClient.cpp
 *
 *  Created on: 07/06/2015
 *      Author: agonia
 */


#include "RocClient.h"
#include "Config.h"
#include "Logger.h"
#include "Loco.h"
#include "LocoDepot.h"
#include "RpicdGateway.h"
extern "C" {
#include "errors.h"
}
#include "rpic/commands.h"
//#include <sys/socket.h>
//#include <arpa/inet.h>
//#include <netinet/in.h>
//#include <netdb.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
//#include <thread>         // std::thread
//#include <bitset>
#include <sys/time.h>

//#include <iostream>


//string RocClient::IP=ROCCLIENTIP;
//long RocClient::port=ROCCLIENTPORT;
//int RocClient::rocsock=0;
//bool RocClient::parser=true;
//bool RocClient::locodepot=true;
//string RocClient::msgIN="";
//string RocClient::msgOUT="";

RocClient::RocClient() {
	// TODO Auto-generated constructor stub
}

RocClient::~RocClient() {
	// TODO Auto-generated destructor stub
}

int RocClient::lclistCMD(TiXmlElement *msg) {

	int result=OK;
	//printf("into lclist\n");
	//TiXmlElement *decode;
	msg=msg->FirstChildElement( "lc" );
	while (msg){
		result=result+lcCMD(msg);
		msg=msg->NextSiblingElement("lc");
	}

	if (!result){
		if (LocoDepot::init){
			Config *planConfig=new Config;
			Logger::insertLog("ROCCLIENT: Updated Locos Plan from Rocrail");
			planConfig->printLocoPlan();
			delete planConfig;
		}else{
			LocoDepot::init=true; //First Initialitation print the first list of locos
		}

	}else{
		result=MULTIPLELCLIST;
	}
return result;

}

int RocClient::carlistCMD(TiXmlElement *msg) {

	int result=OK;
	msg=msg->FirstChildElement( "car" );
	while (msg){
		result=result+lcCMD(msg);
		//Treat car commands as lcCMD
		msg=msg->NextSiblingElement("car");
	}

	if (!result){
		if (LocoDepot::init){
			Config *planConfig=new Config;
			Logger::insertLog("ROCCLIENT: Updated Locos Plan with cars from Rocrail");
			planConfig->printLocoPlan();
			delete planConfig;
		}else{
			LocoDepot::init=true; //First Initialitation print the first list of locos
		}

	}else{
		result=MULTIPLELCLIST;
	}

return result;

}

int RocClient::lcCMD(TiXmlElement *msg) {
//Command to init a lc plan or to update a locoplan
//never launches a rpicommand. speed is managed with the exception CMD and functions with the FN commands

/*
Ideally speed should also be captured. But the "calculated speed" is not stored. In the documentation Vraw is mentioned,
but it seems a possibility just to push a speed, not to get it from the server.
since I do not want to recalculate the speed - rpicd uses the steps, not the V from rocrai and rocrail already makes the
calculations - I can not retrieve the current speed.
For NMRA is not a problem, since any speed command (exception) will init the speed to its right value.
The problem may arise with MM, since a fucntion command and a speed command in rpicd are the same, so a fn command from rocrail
may trigger an update of speed to a non init value.
This is not a big deal, since rocrail can init the field
*/

	Loco* lc;

	string ID;
	int address;
	int protocol;
	bool f[MAXFUNCTIONS];

	string prot;
	int protver;
	int spcnt;
	int fncnt;
	int mmxFnbytes=0;

	//test we have all data, other wise... wrong command
	if (msg->Attribute("id")) ID=msg->Attribute("id"); else return MISSINGID;
//	printf("ID: %s\n",ID.c_str());
	if (msg->Attribute("addr")) address=atoi(msg->Attribute("addr")); else return MISSINGADDR;
	if (msg->Attribute("prot")) prot=msg->Attribute("prot"); else return MISSINGPROT;
	if (msg->Attribute("protver")) protver=atoi(msg->Attribute("protver")); else return MISSINGPROT;
	if (msg->Attribute("spcnt")) spcnt=atoi(msg->Attribute("spcnt")); else return MISSINGSPCNT;
	if (msg->Attribute("fncnt")) fncnt=atoi(msg->Attribute("fncnt")); else return MISSINGFCOUNT;

	for (int i=0;i<(MAXFUNCTIONS);i++) f[i]=false;//return MISSINGFN; //Similar to previous point

	if (!prot.compare("M")){
		  switch (protver) {
		  //potentially simpler with a better definition of types in command.h: if LOCOMMR 1 and LOCOMM1 is 2: protocol=protver*2;
		  //the problem would be for the definition of
		      case 1: protocol=LOCOMM1; break;
		      case 2: protocol=LOCOMM2; break;
		      case 3: protocol=LOCOMM3; break;
		      case 4: protocol=LOCOMM4; break;
		      case 5: protocol=LOCOMM5; break;

		      //CAREFULL 2 EXTRA protocols for MMX: 6 and 7.. 3 bits for speed or seven.
		      //case 6: protocol=LOCOMMX_S3_A7_F4; break;	// s3 PROTOCOL
		      //case 7: protocol=LOCOMMX_S7_A7_F4; break;	// s7 PROTOCOL
//		      case 6: protocol=LOCOMMX_F4; break;	// s3 PROTOCOL
//		      case 7: protocol=LOCOMMX_F8; break;	// s3 PROTOCOL
//		      case 8: protocol=LOCOMMX_F16; break;	// s3 PROTOCOL
//		      case 9: protocol=LOCOMMX_FN; break;	// s3 PROTOCOL

		      default: return UNKWOWNPROTOCOL;
		  }
	}else if (!prot.compare("N")){
		  switch (spcnt) {
		      case 14: protocol=LOCODCCN14; break;
		      case 28: protocol=LOCODCCN28; break;
		      case 128: protocol=LOCODCCN128; break;
		      default: return UNKWOWNSPEEDCNT;
		  }
	}else if (!prot.compare("L")){
		  switch (spcnt) {
		      case 14: protocol=LOCODCCL14; break;
		      case 28: protocol=LOCODCCL28; break;
		      case 128: protocol=LOCODCCL128; break;
		      default: return UNKWOWNSPEEDCNT;
		  }
	//}else if (!prot.compare("P")){
	}else if (!prot.compare("F")){
		protocol=LOCOMMX_F4;	//Used by Rocrail for MMX
		//return UNKWOWNPROTOCOL;
	}else{
		return UNKWOWNPROTOCOL;
	}

	//Rest of the definition of MMX protocols depends on number of fucntions and length of address...
	if (protocol==LOCOMMX_F4){
			if (fncnt<=4) protocol=protocol+0; //fncnt=4;//F4
			else if (fncnt<8) protocol=protocol+1;//fncnt=8; //F8
			else if (fncnt<16) protocol=protocol+2;//fncnt=16; //F16
			else{
				protocol=protocol+3;
				//depending of fncnt.. init number of bytes
				mmxFnbytes=fncnt/8;
				if (fncnt%8) mmxFnbytes++;
			}//fncnt=fn;  //Fn
			if (msg->Attribute("uid")){
				unsigned char UID[4];
				unsigned long int hexUID=atol(msg->Attribute("uid"));
				for (int i=0;i<4;i++) UID[i]=(hexUID>>(8*i))&0xff;
				LocoDepot::newLoco(ID, UID);
				//Potential error create two locos same ID, UID each time a loksync is executed
			}
	}


	/*	if ((protocol==LOCOMMX_S3_A7_F4)||(protocol==LOCOMMX_S7_A7_F4)){
		if (fncnt<=4) protocol=protocol+0; //fncnt=4;//F4
		else if (fncnt<=8) protocol=protocol+1;//fncnt=8; //F8
		else protocol=protocol+2;//fncnt=16;  //F16

		if (address<128) protocol=protocol+0; //A7
		else if (address<512) protocol=protocol+3;	//A9
		else if (address<2048) protocol=protocol+6;	//A11
		else  protocol=protocol+9;	//A14

	}
*/
	lc=LocoDepot::findLoco(ID);
	if(lc){
		Logger::insertLog("ROCCLIENT: Loco ["+ID+"]  found. Updating address, protocol, speed count and functions");
		lc->setAddress(address);
		lc->setProtocol(protocol);
		lc->setSpeedCount(spcnt);
		lc->setmmxFnbytes(mmxFnbytes); //only needed actually for MMX Fn locos
		for (int i=0;i<MAXFUNCTIONS;i++) lc->setF(i,f[i]);

	/*	std::cout << fx << std::endl;
		for (int i=(MAXFUNCTIONS-1);i>=0;i--) std::cout << lc->getF(i);
		std::cout << std::endl;*/
	}else{
		//At this stage notpossible to init from Rocclient a speedcurve or a mmxUID (with mmxFnbytes);
		LocoDepot::newLoco(ID,address,protocol,spcnt,f);
		Logger::insertLog("ROCCLIENT: New Loco ["+ID+"]");

		/*lc=LocoDepot::findLoco(ID);
		std::cout << fx << std::endl;
		for (int i=(MAXFUNCTIONS-1);i>=0;i--) std::cout << lc->getF(i);
		std::cout << std::endl;*/
	}

return OK;

}
