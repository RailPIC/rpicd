//============================================================================
// Name        : initParser.cpp
// Author      : Manuel Serrano
// Version     :
// Copyright   : tren.enmicasa.net
// Description : Alternative solution to reflective selection of parser
//============================================================================

#include "InitParser.h"

//To include as many parsers as needed
#include "parsers/CommandParser.h"
#include "parsers/RocNetParser.h"
#include "parsers/EcosParser.h"
#include "parsers/mcs2Parser.h"
#include "parsers/loconetParser.h"

shared_ptr<CommandParser> InitParser::CreateInstance(string type) {
	CommandParser * instance=nullptr;

	if(type == "rocnet")
	    instance = new RocNetParser();

    if(type == "ecos")
    	instance = new EcosParser();

    if(type == "mcs2")
    	instance = new mcs2Parser();

    if(type == "loconet")
    	instance = new loconetParser();

    if(instance == nullptr)
	   	//default CommandParsers
	   	instance = new CommandParser();

	return std::shared_ptr<CommandParser>(instance);
}
