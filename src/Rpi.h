/*
 * RpiConfig.h
 *
 *  Created on: 18/2/2015
 *      Author: mserrano
 */

#ifndef RPI_H_
#define RPI_H_

#include <string>
#include "SerialPort.h"

using namespace std;

class Rpi {
private:

public:
//	static int  serial_desc;
	static string rpiserial;
	static int maxserialtry;
	static unsigned long usecserialtry;
	//	static long rpibaud;
	static SerialPort *serialc;
	static int ledreadyrestpin;
	static int resetoffpin;
	//static int resetpicpin;
	//static int action1pin;		//Ideally launch rocview
	//static string action1message;
	//static string action1action;
	//static int action2pin;		//Ideally launch keyboard
	//static string action2message;
	//static string action2action;
	static int progtrackpin;
	static int ledsteadypin;
	static int sec4halt;
	//static int ms4resetpic;
	static int sec4resetpic;
//	static bool status;

	virtual ~Rpi();
	Rpi();
	static void initWiringPi();
	static void setpicready();
//	static void setpiciddle();
	static void setOUTsteady();
	static void setOUTiddle();
	static void setprogtrack();
	static void clearprogtrack();
	static int readprogtrack();
	static void resetbutton();
	//static bool getstatus();
	//static void resetpicbutton();
	//static void action1button();
	//static void action2button();
};

#endif /* RPI_H_ */
