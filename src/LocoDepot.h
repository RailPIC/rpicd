/*
 * LocoDepot.h
 *
 *  Created on: 08/07/2015
 *      Author: agonia
 */

#ifndef LOCODEPOT_H_
#define LOCODEPOT_H_
#include "Loco.h"

class LocoDepot {
public:
	static bool init;

	LocoDepot();
	virtual ~LocoDepot();
	static Loco* findLoco(string ID);
	static Loco* findLoco(int addr, int protf);
	static Loco* findLoco(unsigned char UID[4]);
	static Loco* getFirst();
	static Loco* getLast();
	//static void newLoco(string ID, int address, int protocol, int speed, bool f[MAXFUNCTIONS],speedstep *speedcurve);
	//maybe the speedstep not needed if it is only clalled from different place than speedcuve-.. it could be used for later loco parser gettig eveything
	static void newLoco(string ID, int address, int protocol, speedstep *speedcurve);
	//maybe to be used in the future.. not know
	static void newLoco(string ID, int address, int protocol, unsigned char UID[4],int Fn);

	static void newLoco(string ID, int address, int protocol, int spcnt, bool functions[MAXFUNCTIONS]);
	static void newLoco(string ID, speedstep *speedcurve);  //for just the speedcurvedefinition
	static void newLoco(string ID, unsigned char UID[4]);

	static void initMMX();
private:
	static Loco *locos;

//	static bool needed;

};

#endif /* LOCODEPOT_H_ */
