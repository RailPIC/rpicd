/*
 * Logger.h
 *
 *  Created on: 18/2/2015
 *      Author: mserrano
 */

#define PING_LOG 0
#define STATUS_LOG 1
#define CLOCK_LOG 2
#define NOFLAG_LOG 0xFF

#include <stdio.h>
#include <string>

#ifndef LOGGER_H_
#define LOGGER_H_

using namespace std;

class Logger {
private:
	static FILE *flog;
	static bool flagsLog[3];

public:
	static string logpath;

	Logger();
	virtual ~Logger();
	static void initLog(char * configpath);
//	static void insertLog(const char *message);
	static void insertLog(string message);
	static void insertLog(string message, int flag);
	static void setflag(int flag,int value);
	//static string readline();
	static FILE *getReader();
};

#endif /* LOGGER_H_ */
