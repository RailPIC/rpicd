//Just a fake repository of the WiringPi functions only available at the rpi

#define	INPUT			 0
#define	OUTPUT			 1
#define	INT_EDGE_BOTH	 3
#define	INT_EDGE_RISING	 2
#define INT_EDGE_FALLING 1
/*
#ifndef __linux__
typedef struct timeval {
  long tv_sec;
  long tv_usec;
} timeval;
#else
#include <time.h>
#endif

static struct timeval start;
static struct timeval stop;
*/
extern int  wiringPiSetup(void) ;
extern void pinMode(int pin, int mode) ;
extern int  wiringPiISR(int pin, int mode, void (*function)(void)) ;
extern int  serialOpen(const char *device, const int baud) ;
extern int digitalRead(int pin) ;
extern void digitalWrite(int pin, int value) ;
extern void  serialClose(int serial_desc) ;
extern void  delay(int time) ;
/*
#ifndef __linux__
int gettimeofday(struct timeval *tv, struct timeval *dummy);
char * to_string(long value);
#endif
*/
