//============================================================================
// Name        : rpicd.cpp
// Author      : Manuel Serrano
// Version     :
// Copyright   : tren.enmicasa.net
// Description : Daemon for the RPIC solution
//============================================================================

#include "InitParser.h"
#include "Rpi.h"
#include "Logger.h"
#include "Config.h"
#include "RocClient.h"
#include "LocoDepot.h"
#include "Webber.h"
#include <iostream>
#include <memory>

#include <unistd.h>

using namespace std;

int main(int argc,char* argv[]) {
	string confFile;
	std::shared_ptr<CommandParser> secondParser;

	//Unique parameter that can be included in configfile
	//To change log file it is an anrgument of the configfile.

	if (argc!=2) {confFile=CONFIGFILE;}
	else{confFile=argv[1];}

	//printf(confFile.c_str());

	//Config *rpicdConfig=new Config;
	Config *rpicdConfig=new Config();

	//std::shared_ptr<CommandParser> secondParser;

	//auto mainParser=Config::read();
	//Config::compare();
	auto mainParser=rpicdConfig->read(confFile);
	rpicdConfig->compare();

	//Advance Initwiring since mcs2 is oging to require to set rpicd ready fast... asctually.. let's set rpicdready
	Rpi::initWiringPi();
	Rpi::setpicready();
	Rpi::clearprogtrack();
	Rpi::setOUTiddle();

	Logger::insertLog("***********************Parsers**************************");
	Logger::insertLog("********************************************************");

	if (mainParser==nullptr){
		Logger::insertLog("No Parsers configured. Rocrail client will be used");
	}else{

		mainParser->openConnection();
		secondParser=mainParser->next;
		while (secondParser!=nullptr){
			secondParser->openConnection();
			secondParser=secondParser->next;
		}
	}


//	Rpi::initWiringPi();

	//Logger::insertLog("********************************************************");
/*
	if ((RocClient::parser)||(RocClient::locodepot)){
		RocClient::openConnection();
//		while (!LocoDepot::init){}  //wait until locodepot is init.. if not, there is a problem and it should not be possible to continue.
	}else{
		Logger::insertLog("Rocrail Client will not be used");
		if (mainParser==nullptr){
			Logger::insertLog("No parser defined, rpicd will exit");
			exit(EXIT_FAILURE);
		}
	}
*/
	if (mainParser==nullptr){
		Logger::insertLog("No parser defined, rpicd will exit");
		exit(EXIT_FAILURE);
	}
/*	Rpi::setpicready();
	Rpi::clearprogtrack();
	Rpi::setOUTiddle();
	*/
	//To play to have a log nice
	//usleep(30000);

/*
    while (!LocoDepot::init){}   //to print a final locoplan in case there is one.
	Logger::insertLog("********************************************************");

	rpicdConfig->printLocoPlan();
	delete rpicdConfig;
	*/

	Logger::insertLog("*********Checking for MMX locos and init RPIC queue*****");
	LocoDepot::initMMX();

	//Webber *webapp= new Webber; //Webber includes infinite loop

	Webber();  //Webber already includes and infintie loop
	//while (1){}

	cout << "End" << endl;
	return 0;
}
