/*
 * LocoDepot.cpp
 *
 *  Created on: 08/07/2015
 *      Author: agonia
 */

#include "LocoDepot.h"
#include "Loco.h"
#include "Logger.h"
#include "RpicdGateway.h"
#include "rpic/commands.h"

#include <iostream>

LocoDepot::LocoDepot() {
	// TODO Auto-generated constructor stub

}

LocoDepot::~LocoDepot() {
	// TODO Auto-generated destructor stub
}

bool LocoDepot::init=false; //init desde newloco largo
//bool LocoDepot::needed=false; //init desde newloco largo
Loco *LocoDepot::locos=nullptr;

void LocoDepot::newLoco(string ID, speedstep *speedcurve){
//This is only curve when a speedcurve element is at the config file.
	Loco *tempolc;

	tempolc=findLoco(ID);
	//This should not happen.. but just in case, since it is not chekec before launching the function
	if (tempolc) tempolc->setSpeedCurve(speedcurve);
		else {
			tempolc=new Loco(ID,speedcurve);
			if (locos){
				Loco *last=getLast();
				last->next=tempolc;
			}else{
				locos=tempolc;
			}
		}
}

void LocoDepot::newLoco(string ID, unsigned char UID[4]){
//This is only curve when a mmx element is at the configfile.
	Loco *tempolc;

	tempolc=findLoco(ID);
	//This should not happen.. but just in case, since it is not chekec before launching the function
	if (tempolc) tempolc->setmmxUID(UID);
		else {
			tempolc=new Loco(ID,UID);
			if (locos){
				Loco *last=getLast();
				last->next=tempolc;
			}else{
				locos=tempolc;
			}
		}
}


void LocoDepot::newLoco(string ID, int address, int protocol, int spcnt, bool functions[MAXFUNCTIONS]){

	Loco *tempolc;
	tempolc=new Loco(ID,address,protocol,spcnt, functions);
	if (locos){
		Loco *last=getLast();
		last->next=tempolc;
	}else{
		locos=tempolc;
	}

}

Loco *LocoDepot::findLoco(string ID){
	//Maybe I need to put LocoDepot::locos
	Loco *last=locos;

	while (last){
	//	cout<<last->getID().c_str()<<endl;
		if (ID.compare(last->getID())){
			last=last->next;		//ya corregi el compara
		}else{
		//	cout<<"------"<<endl;
			return last;
		}
	}
	//cout<<"------"<<endl;
	return last;
}

Loco *LocoDepot::findLoco(int addr, int protf){
	//Maybe I need to put LocoDepot::locos
	Loco *last=locos;

	while (last){
		//printf("%d %d\n",last->getAddress(),last->getProtocolFamily());
		if ((last->getAddress()==addr)&&(last->getProtocolFamily()==protf)){


			return last;
		}else{
			last=last->next;		//ya corregi el compara
		}
	}
	return last;
}

Loco *LocoDepot::findLoco(unsigned char UID[4]){
	//Maybe I need to put LocoDepot::locos
	Loco *last=locos;
	bool found=true;
	unsigned char* tempoUID;

	while (last){
		//printf("%d %d\n",last->getAddress(),last->getProtocolFamily());
		tempoUID=(last->getmmxUID());
		for (int i=0;i<4;i++) {
			found=found&(tempoUID[i]==UID[i]);
		}
		if (found)return last;
		else last=last->next;
	}
	return last;
}

Loco *LocoDepot::getFirst(){
	return locos;
}

Loco *LocoDepot::getLast(){
	Loco *last=locos;

	if (last) {
		while (last->next) last=last->next;
	}
	return last;
}

void LocoDepot::initMMX(){
	Loco *last=locos;
	int result=0;
	while (last){
		if (last->getProtocolFamily()==LOCMMXCMD){
			unsigned char sys_message[9];
			sys_message[0]=9;
			int address=last->getAddress();
			sys_message[1]=(address>>8)&0xff;
			sys_message[2]=address&0xff;;
			sys_message[3]=(unsigned char)last->getProtocol();
			sys_message[4]=(unsigned char)last->getmmxFnbytes();
			unsigned char *UID=last->getmmxUID();
			for (int i = 0; i < 4; i++) {
				sys_message[5+i]=*(UID+3-i);
			}
			result=result+RpicdGateway::sendsyscommand(INIT_MMX, sys_message);
		}
		last=last->next;
	}

	if (result==OK) Logger::insertLog("All MMX locos ready to go");

}
