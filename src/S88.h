/*
 * S88.h
 *
 *  Created on: 17/05/2018
 *      Author: agonia
 */

#ifndef S88_H_
#define S88_H_

#include <string>

class S88 {
public:

	static bool active;
	static int datapin;
	static int clkpin;
	static int loadpin;
	static int resetpin;

	S88();
	virtual ~S88();
	static void init();
};

#endif /* S88_H_ */
