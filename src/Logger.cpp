/*
 * Logger.cpp
 *
 *  Created on: 18/2/2015
 *      Author: mserrano
 */

#include "Logger.h"
#include "Config.h"
#include "Webber.h"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

using namespace std;

FILE *Logger::flog=nullptr;
string Logger::logpath=LOGPATH;
bool Logger::flagsLog[]={0,0,0};//by default everything off

Logger::Logger() {
	// TODO Auto-generated constructor stub

}

Logger::~Logger() {
	// TODO Auto-generated destructor stub
	fclose(Logger::flog);
}

void Logger::initLog(char* configpath) {

	time_t now = time(0);
	char dt [33];

	if ((Logger::flog = fopen (Logger::logpath.c_str(), "w+"))==nullptr){
		exit(EXIT_FAILURE);
	    }
	strftime (dt,33,"RPICDaemon: %d/%m/%Y %X ",localtime (&now));
	fprintf(Logger::flog, "%s*****************Logging info rpicd********************\n", dt);
	fprintf(Logger::flog, "%sVersion %s\n", dt,VERSION);
	fprintf(Logger::flog, "%s%s\n", dt, configpath);
	fflush(Logger::flog);

	/* Close out the standard file descriptors */
	close(STDIN_FILENO);
	//close(STDOUT_FILENO);
	//close(STDERR_FILENO);
}

//void Logger::insertLog(const char *message){
void Logger::insertLog(string message){
	time_t now = time(0);
	char dt [33];
	strftime (dt,33,"RPICDaemon: %d/%m/%Y %X ",localtime (&now));
	fprintf(Logger::flog, "%s%s\n", dt,message.c_str());
	fflush(Logger::flog);
	Webber::inserWebLog(string(dt)+message);
}

/*string Logger::readline ( void )
{
	FILE weblog=fopen (Logger::logpath.c_str(), "r");
	char line [ 128 ];
   if (fgets ( line, sizeof line, Logger::flog )!=NULL){
   return string(line);}
   else{
	   return "END";
   }
}*/

void Logger::insertLog(string message, int flag){
	if (Logger::flagsLog[flag]) Logger::insertLog(message);
}

FILE *Logger::getReader(){
	return fopen (Logger::logpath.c_str(), "r");
}

void Logger::setflag(int flag,int value){
	Logger::flagsLog[flag]=(bool)value;
}
