/*
 * RpicdGateway.cpp
 *
 *  Created on: 11/08/2015
 *      Author: agonia
 */

#include "RpicdGateway.h"
#include "Rpi.h"
#include "Logger.h"
#include "rpic/commands.h"
#include <string>
#include <stdio.h>
#include "errors.h"

#include <iostream>
#include <bitset>
#include <sys/time.h>

//#ifdef __arm__
#if defined(__arm__) || defined(__aarch64__)
#include <wiringPi.h>
//#include <wiringSerial.h>
#endif

//#define BUFFSIZE 10

bool RpicdGateway::sendready = true;
unsigned char RpicdGateway::response[MAXBYTES];


RpicdGateway::RpicdGateway() {
	// TODO Auto-generated constructor stub

}

RpicdGateway::~RpicdGateway() {
	// TODO Auto-generated destructor stub
}

unsigned int RpicdGateway::sendfxcommand(Loco *lc, int fgroup) {
	//For NMRA fx commands and MFX FN (rest is treated as speed commands.
	//The fgroup follows the fgroup by rocrail...not by dcc... rpic is later on rebuilding the packets.
	unsigned char buffer[MAXBYTES];
	unsigned int response = RPICDGATEWAYERROR;
	unsigned char fbyte;
	int addr = lc->getAddress();

	if (lc->getProtocolFamily() == LOCNMRACMD) {
		switch (fgroup) {

		case 0:
			fbyte = lc->getF(0) + lc->getF(1) * 2 + lc->getF(2) * 4
					+ lc->getF(3) * 8 + lc->getF(4) * 16; //Note I send the fbyte as naturally ordered.. not as requested by dcc... central station will take care
			break;
		case 1:
			fbyte = lc->getF(5) + lc->getF(6) * 2 + lc->getF(7) * 4
					+ lc->getF(8) * 8;
			break;
		case 2:
			fbyte = lc->getF(9) + lc->getF(10) * 2 + lc->getF(11) * 4
					+ lc->getF(12) * 8;
			break;
		case 3:
		case 4:
			fbyte = lc->getF(13) + lc->getF(14) * 2 + lc->getF(15) * 4
					+ lc->getF(16) * 8 + lc->getF(17) * 16 + lc->getF(18) * 32
					+ lc->getF(19) * 64 + lc->getF(20) * 128;
			break;
		case 5:
		case 6:
			fbyte = lc->getF(21) + lc->getF(22) * 2 + lc->getF(23) * 4
					+ lc->getF(24) * 8 + lc->getF(25) * 16 + lc->getF(26) * 32
					+ lc->getF(27) * 64 + lc->getF(28) * 128;
			break;
		default:
			fgroup = 0;
			fbyte = lc->getF(0) + lc->getF(1) * 2 + lc->getF(2) * 4
					+ lc->getF(3) * 8 + lc->getF(4) * 16;
		}
	}

	if (lc->getProtocol() < LOCODCCL14) {
		buffer[2] = (char) addr;
		buffer[3] = fbyte;
		buffer[4] = (char) fgroup;
		response = sendrpiccommand(buffer, LOCNMRACMD, LOCODCCNfx, 5);
	} else { //DCC LonG and MMX_FN
		buffer[3] = (char) addr;
		if (addr < 256) {
			buffer[2] = 0;
		} else {
			buffer[2] = addr / 256; //8 shifts to right to ger the high byte... otherwise buffer[1]=1...NMRA address go from 0 to 511... so the only posibility is bufffer[1]=1
		}
		if (lc->getProtocolFamily() == LOCNMRACMD) {
			buffer[4] = fbyte;
			buffer[5] = fgroup;
			response = sendrpiccommand(buffer, LOCNMRACMD, LOCODCCLfx, 6);
		} else {
			int cspeed = lc->getSpeed();
			bool dir = lc->getDirForward();
			if (!dir)
				buffer[4] = cspeed * (-1) + 128;
			else
				buffer[4] = (char) cspeed;
			buffer[5] = lc->getF(fgroup) * 128 + fgroup; //fgroups contains the number of cuntion that changes
			response = sendrpiccommand(buffer, LOCMMXCMD, lc->getProtocol(), 6);
		}
	}
	return response;
}

unsigned int RpicdGateway::sendspeedcommand(Loco *lc) {
//For speed commands
	unsigned char buffer[MAXBYTES];
	unsigned int response = RPICDGATEWAYERROR;
	unsigned char speed;
	int cspeed = lc->getSpeed();
	int addr = lc->getAddress();
	bool dir = lc->getDirForward();

	if (!dir)
		speed = cspeed * (-1) + 128;
	else
		speed = (char) cspeed;

	if (lc->getProtocol() < LOCODCCN14) {
		buffer[2] = (char) addr;
		buffer[3] = speed;
		buffer[4] = lc->getF(0) + lc->getF(1) * 2 + lc->getF(2) * 4
				+ lc->getF(3) * 8 + lc->getF(4) * 16;
		response = sendrpiccommand(buffer, LOCMMCMD, lc->getProtocol(), 5);
	} else if (lc->getProtocol() < LOCODCCL14) {
		buffer[2] = (char) addr;
		buffer[3] = speed;
		response = sendrpiccommand(buffer, LOCNMRACMD, lc->getProtocol(), 4);

	} else { //NMRA LONG or MMX
		buffer[3] = (char) addr;
		if (addr < 256) {
			buffer[2] = 0;
		} else {
			buffer[2] = addr / 256; //8 shifts to right to ger the high byte... otherwise buffer[1]=1...NMRA address go from 0 to 511... so the only posibility is bufffer[1]=1
		}
		buffer[4] = speed;
		if (lc->getProtocol() < LOCOMMX_F4) {
			response = sendrpiccommand(buffer, LOCNMRACMD, lc->getProtocol(),
					5);
		} else {
			//MMX
			if (lc->getProtocol() < LOCOMMX_F16) {
				buffer[5] = lc->getF(0) + lc->getF(1) * 2 + lc->getF(2) * 4
						+ lc->getF(3) * 8 + lc->getF(4) * 16 + lc->getF(5) * 32
						+ lc->getF(6) * 64 + lc->getF(7) * 128;
				response = sendrpiccommand(buffer, LOCMMXCMD, lc->getProtocol(),
						6);
			} else if (lc->getProtocol() < LOCOMMX_FN) {
				buffer[5] = lc->getF(8) + lc->getF(9) * 2 + lc->getF(10) * 4
						+ lc->getF(11) * 8 + lc->getF(12) * 16
						+ lc->getF(13) * 32 + lc->getF(14) * 64
						+ lc->getF(15) * 128;
				buffer[6] = lc->getF(0) + lc->getF(1) * 2 + lc->getF(2) * 4
						+ lc->getF(3) * 8 + lc->getF(4) * 16 + lc->getF(5) * 32
						+ lc->getF(6) * 64 + lc->getF(7) * 128;
				response = sendrpiccommand(buffer, LOCMMXCMD, lc->getProtocol(),
						7);
			} else {
				//LOCOMMX_FN by default on a speed command, it updates f(0)
				buffer[5] = lc->getF(0) * 128;
				response = sendrpiccommand(buffer, LOCMMXCMD, lc->getProtocol(),
						6);
			}
		}

	}

	return response;

	//Launching autopilot if there is sppedcurve.. ver donde se actualiaza old_speed

	/*	std::bitset<8> bits(speed);
	 for (int i=(7);i>=0;i--) std::cout << bits[i];
	 std::cout << std::endl;*/

}

unsigned int RpicdGateway::sendacccommand(int address, int port, int gate,
		bool value, int protfamily) {
	unsigned char buffer[MAXBYTES];
	unsigned int response = RPICDGATEWAYERROR;
	unsigned char accfunction;
	unsigned char length;
	unsigned char type;

	unsigned char command = protfamily;

	port--; //port 0-3
	//so far MM acc are not programmed in pic, so I treat the coding of the function byte as NMRA
	//shifting the information to its right place   CDDD C=action DD=port D=gate
	accfunction = gate + port * 2 + value * 8;
	//Both ACCMM and ACCNMRA are = 0;
	type = 0;

	switch (command) {
	case ACCMMCMD:
		buffer[2] = (char) address;
		buffer[3] = accfunction;
		length = 4;
		break;
	case ACCNMRACMD:
	default:
		buffer[3] = (char) address;
		if (address < 256)
			buffer[2] = 0;
		else
			buffer[2] = address / 256;
		buffer[4] = accfunction;
		length = 5;
	}
	response = sendrpiccommand(buffer, command, type, length);
	return response;
}
unsigned int RpicdGateway::sendsyscommand(int command){
	unsigned char tempo[1];
	tempo[0]=0;
	return sendsyscommand(command,tempo);
}

unsigned int RpicdGateway::sendsyscommand(int command, unsigned char *sys_msg) {

	unsigned char buffer[MAXBYTES];
	unsigned int response = RPICDGATEWAYERROR;

	if (sys_msg[0]==0){
	//if (sys_msg == NULL) {
		buffer[2] = rand() % 255;
		response = sendrpiccommand(buffer, SYSTEMCMD, command, 3);
	} else {
		int i;
		for (i = 1; i < (sys_msg[0]); i++) {
			buffer[1 + i] = sys_msg[i];
		}
		response = sendrpiccommand(buffer, SYSTEMCMD, command, sys_msg[0] + 1);	//message+1 lenght already in sys_msg[0];+1 for type
	}
	return response;
}

unsigned int RpicdGateway::sendconfcommand(int command){
	unsigned char tempo[1];
	tempo[0]=0;
	return sendconfcommand(command,tempo);

}

unsigned int RpicdGateway::sendconfcommand(int command,
		unsigned char * conf_msg) {
	unsigned char buffer[MAXBYTES];
	unsigned int response = RPICDGATEWAYERROR;

	if (conf_msg[0]==0){
	//if (conf_msg == NULL) {
		buffer[2] = rand() % 255;
		response = sendrpiccommand(buffer, CONFIGCMD, command, 3);
	} else {
		int i;
		for (i = 1; i < (conf_msg[0]); i++) {
			buffer[1 + i] = conf_msg[i];
		}
		response = sendrpiccommand(buffer, CONFIGCMD, command, conf_msg[0] + 1);//message+1 lenght already in sys_msg[0]+1 of type in bugger[1]
	}
	return response;

}

unsigned int RpicdGateway::sendprogcommand(int command, int cv, int value,
		int addr) {
	unsigned char buffer[MAXBYTES];
	unsigned int response = RPICDGATEWAYERROR;

	switch (command) {
	case NMRA_SERVICE_MODE_ADDRESS_write:
	case NMRA_SERVICE_MODE_ADDRESS_read:
		buffer[2] = (char) value;
		response = sendrpiccommand(buffer, PROGCMD, command, 3);
		break;
	case NMRA_SERVICE_MODE_REGISTER_write:
	case NMRA_SERVICE_MODE_REGISTER_read:
		buffer[2] = (char) cv;
		buffer[3] = (char) value;
		response = sendrpiccommand(buffer, PROGCMD, command, 4);
		break;
	case NMRA_SERVICE_MODE_DIRECT_write:
	case NMRA_SERVICE_MODE_DIRECT_verify:
	case NMRA_SERVICE_MODE_DIRECT_bit:
		buffer[3] = (char) cv;
		buffer[2] = cv / 256;       //8 shifts to the right
		buffer[4] = value;	//bit
		response = sendrpiccommand(buffer, PROGCMD, command, 5);
		break;
	case LOCODCCNwrite:
	case LOCODCCNverify:
	case LOCODCCNbit:
		buffer[2] = (char) addr;
		buffer[4] = (char) cv;
		buffer[3] = cv / 256;       //8 shifts to the right
		buffer[5] = (char) value;
		response = sendrpiccommand(buffer, PROGCMD, command, 6);
		break;
	case ACCNMRAwrite:
	case ACCNMRAverify:
	case ACCNMRAbit:
	case LOCODCCLwrite:
	case LOCODCCLverify:
	case LOCODCCLbit:
		buffer[3] = (char) addr;
		buffer[3] = addr / 256;
		buffer[5] = (char) cv;
		buffer[4] = cv / 256;       //8 shifts to the right
		buffer[6] = value;
		response = sendrpiccommand(buffer, PROGCMD, command, 7);
		break;
	default:
		Logger::insertLog("RPICDGATEWAY: PROG Command not yet recognised");
	}
	return response;
}

unsigned int RpicdGateway::sendrpiccommand(unsigned char buffer[],
		unsigned char command, unsigned char type,
		unsigned char commandlength) {
	while (!sendready)
		; //wait until send ready

	sendready = false;
	unsigned char i = 0;
	bool sendsuccess = false;
	bool successget = false;
	int trysend = 0;
	//unsigned char response[BUFFSIZE];
	//unsigned char response[MAXBYTES];
	unsigned char resplength;
	//	int tryget=0;
	struct timeval initget, timeoutget;
	unsigned long int dif;

	string hexmessage = "";
	char hex[3]; //2+null character by sprintf

	//buffer[0]=((command<<5)&0b11100000)|type;
	buffer[0] = ((commandlength << 4) & 0b11110000) | command; //length=commandlength withouth chcksum.. whis is what is used by PIC
	buffer[1] = type;
	buffer[commandlength] = checksum(buffer, 0, commandlength - 1);

	while ((!sendsuccess) && (trysend < Rpi::maxserialtry)) {
		successget = false;
//#ifdef __arm__
#if defined(__arm__) || defined(__aarch64__)
		i=Rpi::serialc->sendBuffer(buffer,commandlength+1);
#else
		i = commandlength + 1;		//No actual sending... everything OK
#endif
		if (i != (commandlength + 1)) {
			usleep(Rpi::usecserialtry * trysend);
			trysend++;
			Rpi::serialc->clear();
		} else {
			//Transmit OK
			sendsuccess = true;
			//trysend=0;
			//In case a bad response... we can try again to send the messsage
			hexmessage = "Command sent: ";
			for (i = 0; i <= commandlength; i++) {
				//	printf("Command sent buffer %d: %c %X \n", i, buffer[i],buffer[i]);
				sprintf(hex, "%.2X", buffer[i]);
				hexmessage = hexmessage + hex + " ";
			}
			gettimeofday(&initget, NULL);
			gettimeofday(&timeoutget, NULL);
			i = 0;
			dif = (timeoutget.tv_sec - initget.tv_sec) * 1000000
					+ (timeoutget.tv_usec - initget.tv_usec);
			//Init get response

			/*It seems odroid is to ofast and tries to read before the bufferin is ready.
			 * 1ms should be enough. Receiving 10 bytes takes 0,8ms...
			 * I coul duse a timeval structure to delay from the moment serial was sent...
			 * Otherwisek, with the current approach, no matter how fast the rpi/odroid is,
			 * I will always wait one additional ms.
			 *
			 * Also, alternatives to the timeout with timeval:
			 * https://stackoverflow.com/questions/2917881/how-to-implement-a-timeout-in-read-function-call
			 *
			 * */
			//usleep(1000);//1ms of wait for response to arrive
			usleep(100000);//100ms on tests seems the correct

			while ((!successget) && (dif < Rpi::usecserialtry)) {
//#ifdef __arm__
#if defined(__arm__) || defined(__aarch64__)
				resplength=Rpi::serialc->bytesToRead();
#else
				resplength = 1;
#endif
				if (resplength > 0) {
//#ifdef __arm__
#if defined(__arm__) || defined(__aarch64__)
					i=Rpi::serialc->getBuffer(response,resplength);
#else
					i = resplength;
					response[0] = buffer[commandlength];
#endif
					if (i != resplength) {
						//we have a problem... try to receive again
						Logger::insertLog(
								"RPICDGATEWAY: PROBLEM RECEIVING DATA. If possible trying to send it again");
					} else {
						successget = true;
						if (response[0] != buffer[commandlength]) {
							//Checksum not OK
							//If necessary I could treart error massages received here... or if a response is comming... bufer[0[ could infomr that there is more bytes to read
							//So Far I only assume bas cheksum and error
							Rpi::serialc->clear(); //We clear trasnmission and send again.. if there are sendtry left
							trysend++;
							sendsuccess = false;
							Logger::insertLog(
									"RPICDGATEWAY: WRONG ACK RECEIVED. If possible trying to send it again");
						} else {
							//cHEKSUM ok, PRINT eNTIRE RESPONSE
							printf("Response %d byte\n", resplength);
							hexmessage = hexmessage + ".Response: ";
							for (i = 0; i < resplength; i++) {
								//	printf("Command sent buffer %d: %c %X \n", i, buffer[i],buffer[i]);
								sprintf(hex, "%.2X", response[i]);
								hexmessage = hexmessage + hex + " ";
							}

							//	sprintf(hex,"%X", response[0]);
							//	hexmessage=hexmessage+".Response: "+hex;
							Logger::insertLog("RPICDGATEWAY: " + hexmessage);
						}
					}
				}
				gettimeofday(&timeoutget, NULL);
				dif = (timeoutget.tv_sec - initget.tv_sec) * 1000000
						+ (timeoutget.tv_usec - initget.tv_usec);
			}
			if (!successget) {
				//Timeout get, try to send again if possible
				Logger::insertLog(
						"RPICDGATEWAY: TIMEOUT RECEVING DATA. If possible trying to send it again");
				sendsuccess = false;
				trysend++;
			}
		}
	}

	if (dif >= Rpi::usecserialtry) {
		Logger::insertLog(
				"RPICDGATEWAY: MSG NOT DELIVERED Timeout receiving data");
		sendready = true;
		return SERIALTIMEOUT;
	}

	if (trysend == Rpi::maxserialtry) {
		Logger::insertLog(
				"RPICDGATEWAY: MSG NOT DELIVERED. Max send trys without success");
		sendready = true;
		return MAXSERIALTRYS;
	}

	sendready = true;
	return OK;
}

unsigned char * RpicdGateway::getresponse(){

	return response;

}


unsigned char RpicdGateway::checksum(unsigned char toverify[],
		unsigned char firstbyte, unsigned char lastbyte) {

	unsigned char i;
	unsigned char sum = toverify[firstbyte];

	for (i = firstbyte; i != lastbyte; i++) {
		sum = sum ^ toverify[i + 1];
	}
	return sum;
}
