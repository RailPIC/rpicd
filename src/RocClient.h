/*
 * RocClient.h
 *
 *  Created on: 07/06/2015
 *      Author: agonia
 */


#ifndef ROCCLIENT_H_
#define ROCCLIENT_H_

#include <string>
#include "xml/tinyxml.h"


//#define BUFFSIZE 50000
#define BUFFSIZE 125000

using namespace std;


class RocClient {
public:
//	static string IP;
//	static long port;
//	static bool parser;
//	static bool locodepot;

	RocClient();
	virtual ~RocClient();
//	static void openConnection();
	//static void lokSync();
	//static void carSync();
	static int lclistCMD(TiXmlElement *msg);
	static int carlistCMD(TiXmlElement *msg);

private:
//	static int rocsock;
//	static string msgIN;
//	static string msgOUT;
//	static void connection_handler();
//	static int parseMgr(char const *msg);
//	static int lclistCMD(TiXmlElement *msg);
//	static int carlistCMD(TiXmlElement *msg);
//	static int lclistCMD(TiXmlElement *msg);
//	static int carlistCMD(TiXmlElement *msg, bool plan);
	static int lcCMD (TiXmlElement *msg);
//	static int fnCMD (TiXmlElement *msg);
//	static int programCMD (TiXmlElement *msg);
//	static int exceptionCMD(TiXmlElement *msg);
//	static int stateCMD(TiXmlElement *msg);
//	static int clockCMD(TiXmlElement *msg);
};

#endif /* ROCCLIENT_H_ */
