/*
 * Loco.h
 *
 *  Created on: 08/07/2015
 *      Author: agonia
 */

#ifndef LOCO_H_
#define LOCO_H_
//#define MAXFUNCTIONS 29  //28 FUNCTIONS+f0
#define MAXFUNCTIONS 128  //127 FUNCTIONS+f0
#include <string>
#include <thread>         // std::thread

using namespace std;

struct speedstep
{
    int step;
    int speedup;
    int speedown;
    speedstep *next;
};

typedef struct speedstep speedstep;

class Loco {
private:
	string ID;
	int address;
	int protocol;
	int protfamily;	    //needed for mmcs2 to look for a Loco in LocoDepot
	int speed;
	bool dirforward;  //not only in speed.. when speed=0 still need to flag the direction
	int speedcnt;		//needed for mmcs2
	bool f[MAXFUNCTIONS];
	unsigned char mmxUID[4];
	int mmxFnbytes;	//needed to init mmx
	speedstep *speedcurve;
	int tspeed;
	bool tdir;
	bool autopilot;
	std::thread driver;
	void speedDriver();
	int nextStepSleep(int speed);
	int previousStepSleep(int speed);

public:
			//(&Loco::speedCurve,this) a repasar
	Loco *next;
	Loco();
	Loco(string ID, speedstep *speedcurve);
	Loco(string ID, unsigned char UID[4]);
	Loco(string ID, int address, int protocol,int spcnt, bool functions[MAXFUNCTIONS]);
	virtual ~Loco();


	string getID();
	int getAddress();
	void setAddress(int addr);
	int getProtocol();
	void setProtocol(int prot);
	int getProtocolFamily();
	void setProtocolFamily(int protf);
	void setSpeed(int speed);
	int getSpeed();
	int getTSpeed();
	void setSpeedCount(int spcnt);
	int getSpeedCount();
	bool getDirForward();
	bool getTDir();
	void setF(int function, bool value);
	bool getF(int function);
	void setmmxFnbytes(int Fnbytes);
	int getmmxFnbytes();
	void setmmxUID(unsigned char UID[4]);
	unsigned char * getmmxUID();
	void setSpeedCurve(speedstep *speedcurve);
	speedstep *getSpeedCurve();
	void updateDriver(int target, bool dir);

	//setID();
	//getID...etc... to be played based on private or public
};

#endif /* LOCO_H_ */
