/*
 * Rpi.cpp
 *
 *  Created on: 18/2/2015
 *      Author: mserrano
 */

#include "Rpi.h"
#include "RpicdGateway.h"
#include "rpic/commands.h"
#include "Logger.h"
#include "Config.h"
#include "S88.h"
#include "LocoDepot.h"
#include <stdlib.h>
#include <stdio.h>
extern "C" {
//#ifdef __arm__
#if defined(__arm__) || defined(__aarch64__)
#include <wiringPi.h>
//#include <wiringSerial.h>
#else
#include "tools/fakewiringPi.h"
#endif

//static struct timeval start;
//static struct timeval stop;

}

#include <sys/time.h>

string Rpi::rpiserial = RPISERIAL;
//long Rpi::rpibaud = RPIBAUD;
int Rpi::maxserialtry=MAXSERIALTRY;
unsigned long Rpi::usecserialtry=USECSERIALTRY;
//int Rpi::resetpicpin = RESETPICPIN;
int Rpi::resetoffpin = RESETOFFPIN;
//int Rpi::action1pin = ACTION1PIN;
//string Rpi::action1action = ACTION1ACTION;
//string Rpi::action1message = ACTION1MESSAGE;
//int Rpi::action2pin = ACTION2PIN;
//string Rpi::action2action = ACTION2ACTION;
//string Rpi::action2message = ACTION2MESSAGE;
int Rpi::ledreadyrestpin = LEDREADYRESTPIN;
int Rpi::ledsteadypin=LEDSTEADYPIN;
int Rpi::progtrackpin=PROGTRACKPIN;

int Rpi::sec4halt = SEC4HALT;
//int Rpi::ms4resetpic = MS4RESETPIC;
int Rpi::sec4resetpic = SEC4RESETPIC;
//int Rpi::serial_desc = 0;
SerialPort *Rpi::serialc=new SerialPort();
//bool Rpi::status=false;

Rpi::~Rpi() {
	// TODO Auto-generated destructor stub
}

Rpi::Rpi() {
	// TODO Auto-generated constructor stub

}

void Rpi::initWiringPi() {
//#ifdef __arm__
	Logger::insertLog("*****************RPI Initialization*********************");
	if (Rpi::serialc->connect(Rpi::rpiserial.c_str()) < 0) {
		Logger::insertLog("ERROR: Unable to open serial port");
		exit(EXIT_FAILURE);
	}

	if (wiringPiSetup() == -1) {
		Logger::insertLog("ERROR: Unable to start wiringPi");
		exit(EXIT_FAILURE);
	}

//	if (wiringPiISR(Rpi::resetoffpin, INT_EDGE_BOTH, &(Rpi::resetbutton)) < 0) {
	if (wiringPiISR(Rpi::resetoffpin, INT_EDGE_FALLING, &(Rpi::resetbutton)) < 0) {
		Logger::insertLog("ERROR: Unable to set up the reset rpic / OFF button");
		exit(EXIT_FAILURE);
	} else {
		Logger::insertLog("Reset rpic / OFF Button Ready");
	}
/*
	if (wiringPiISR(Rpi::resetpicpin, INT_EDGE_BOTH, &resetpicbutton) < 0) {
		Logger::insertLog("ERROR: Unable to set up the reset pic button");
		exit(EXIT_FAILURE);
	} else {
		Logger::insertLog("Reset Pic Button Ready");
	}

	if (Rpi::action1action.compare("")) {
		if (wiringPiISR(Rpi::action1pin, INT_EDGE_RISING, &action1button)	< 0) {
			Logger::insertLog("ERROR: Unable to set up the "+Rpi::action1message+" button");
			exit(EXIT_FAILURE);
		} else {
			Logger::insertLog(Rpi::action1message+ " pic button ready");
		}
	}

	if (Rpi::action2action.compare("")) {
		if (wiringPiISR(Rpi::action2pin, INT_EDGE_RISING, &action2button)	< 0) {
			Logger::insertLog("ERROR: Unable to set up the "+Rpi::action2message+" button");
			exit(EXIT_FAILURE);
		} else {
			Logger::insertLog(Rpi::action2message+ " pic button ready");
		}
	}
*/
	pinMode(Rpi::ledreadyrestpin, OUTPUT);
	pinMode(Rpi::ledsteadypin, OUTPUT);
	pinMode(Rpi::progtrackpin, OUTPUT);
	//Independtly on having S88 active or not...it is goot to configure all pins
	//Problem is for RPI, the pins in S88 are not in the GPIO and cgrashes the board.
	//Then, if not going to be used, not init.
	if (S88::active){
		S88::init();
	}

//#endif
}
void Rpi::resetbutton() {

	//This is now a blocking call. If the button is pressed, priority to what is hapenning in the button

	int readPin;

	int result;
	int delaywindowus=500000; //500ms
	int maxtry=Rpi::sec4halt; //4 sec by defaul  500000*2 each cycle=1s
	int tries=0;

	readPin = digitalRead(Rpi::resetoffpin);

	//result = RpicdGateway::sendsyscommand(CYCLEOFF, NULL);
	result = RpicdGateway::sendsyscommand(CYCLEOFF);
	if (result == OK) {
		Logger::insertLog("RPI: CYCLEOFF succesfully sent");
	}


	while ((!readPin)&&(tries<maxtry)){
		//If button press an not timeout blink enable led every 0,5 secs
		Rpi::setOUTiddle();
		usleep(delaywindowus);
		Rpi::setOUTsteady();
		usleep(delaywindowus);
		readPin = digitalRead(Rpi::resetoffpin);
		tries++;
	}

	if (readPin) {
		Rpi::setOUTiddle();
		//while ended becasuse button was released.. even if at the same time the number of tries is done
		//Logger::insertLog("Reset Rocrail requested.");
		//system("sudo systemctl restart rocrail.service");	//reset ROCRAIAL
		Logger::insertLog("Reset rpic requested.");
		Rpi::setpicready();
		Rpi::serialc->disconnect();
		if (Rpi::serialc->connect(Rpi::rpiserial.c_str()) < 0) {
			Logger::insertLog("ERROR: Unable to open serial port");
			exit(EXIT_FAILURE);
		} else {
			Logger::insertLog("Serial Port reset");
		}
		LocoDepot::initMMX();
	}else{
		//while ended because we are shutting down
		//Logger::insertLog("Halt requested");
		Logger::insertLog("Power off requested");
		tries=0;
		delaywindowus=200000; //200ms
		while (tries<maxtry){
			//signal we are closing
			Rpi::setOUTiddle();
			usleep(delaywindowus);
			Rpi::setOUTsteady();
			usleep(delaywindowus);
			tries++;
		}
		//PowerOF will turn off the reset and steady leds.
		//system("sudo poweroff");
		system("echo true > /config/shutdown_signal");

	}

/*
	int sec;
	if (!readPin) {
		gettimeofday(&start, NULL);
	} else {
		gettimeofday(&stop, NULL);
		sec = (int) (stop.tv_sec - start.tv_sec);
		if (sec > Rpi::sec4halt) {
			//Logger::insertLog("Halt requested");
			Logger::insertLog("Power off requested");
			//system("sudo halt");
			system("sudo poweroff");
		} else {
			Logger::insertLog("Reset Rocrail requested.");
			//system("sudo /etc/init.d/rocraild reset");	//reset ROCRAIAL
			system("sudo systemctl restart rocrail.service");	//reset ROCRAIAL
//			digitalWrite(Rpi::ledreadyrestpin, 0); //RESET pic
//			delay(Rpi::ms4resetpic); //wait before relaunching rocrail.. which will enable rpic
			Rpi::setpicready();
			//Discard waiting data to be sent
			// serialFlush (serial_desc) ;		//Let's see
	//		serialClose(serial_desc);
			Rpi::serialc->disconnect();
			if (Rpi::serialc->connect(Rpi::rpiserial.c_str()) < 0) {
				Logger::insertLog("ERROR: Unable to open serial port");
				exit(EXIT_FAILURE);
			} else {
				Logger::insertLog("Serial Port reset");
			}
		}
	}*/
}

void Rpi::setpicready() {
//#ifdef __arm__
	digitalWrite(Rpi::ledreadyrestpin, 0); //RESET pic
	sleep(Rpi::sec4resetpic);
	//	delay(Rpi::ms4resetpic);	    		//wait before relaunching pic
	digitalWrite(Rpi::ledreadyrestpin, 1); //Ready pic
//	delay(Rpi::ms4resetpic);	    		//wait before relaunching pic
	sleep(Rpi::sec4resetpic);
	Rpi::serialc->clear();					//Clear serial port
	//#endif
//	Rpi::status=true;
}
/*
void Rpi::setpiciddle(){
	digitalWrite(Rpi::ledreadyrestpin, 0); //RESET pic
//	Rpi::status=false;
}
*/
void Rpi::setOUTsteady() {
//#ifdef __arm__
	digitalWrite(Rpi::ledsteadypin, 1); //Steady OUT
	//#endif
}

void Rpi::setOUTiddle() {
//#ifdef __arm__
	digitalWrite(Rpi::ledsteadypin, 0); //Steady iddle
	//#endif
}

void Rpi::setprogtrack() {
//#ifdef __arm__
	//Relay board is active low
	digitalWrite(Rpi::progtrackpin, 0); //ProgTRACK ON
	//#endif
}

void Rpi::clearprogtrack() {
//#ifdef __arm__
	//Relay board is active low
	digitalWrite(Rpi::progtrackpin, 1); //ProgTRACK OFF
	//#endif
}

int Rpi::readprogtrack() {
//#ifdef __arm__
	//Relay board is active low
	return digitalRead(Rpi::progtrackpin); //ProgTRACK OFF
	//#endif
}

/*
void Rpi::resetpicbutton() {
//#ifdef __arm__
	//To avoid rebounds.. if key is not press more than ms4resetpic,
	int readPin;
	int msec;
	readPin = digitalRead(Rpi::resetpicpin);

	if (!readPin) {
		gettimeofday(&start, NULL);
	} else {
		gettimeofday(&stop, NULL);
		msec = (int) (stop.tv_usec*1000 - start.tv_usec*1000);
		if (msec>Rpi::ms4resetpic){
			Logger::insertLog("Reset PIC requested");
			Rpi::setpicready();
		}
	}

//#endif
}
void Rpi::action1button() {
	system(Rpi::action1action.c_str());
}
void Rpi::action2button() {
	system(Rpi::action2action.c_str());
}
*/
