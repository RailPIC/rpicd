/*
 * errors.c
 *
 *  Created on: 11/08/2015
 *      Author: agonia
 */

#include "errors.h"

const char * errorHandler(int errorcode){

	switch (errorcode) {
      case OK: return "OK"; break;		//it should never happen
      case MISSINGID: return "MISSING ID"; break;
      case MISSINGADDR: return "MISSING ADDRESS"; break;
      case MISSINGPROT: return "MISSING PROTOCOL"; break;
      case MISSINGSPCNT: return "MISSING SPEED COUNT"; break;
      case UNKWOWNPROTOCOL: return "UNKNOWN PROTOCOL"; break;
      case UNKWOWNSPEEDCNT: return "UNKNOWN SPEED COUNT"; break;
      case MISSINGFGROUP:  return "MISSING F GROUP"; break;
      case MISSINGFCHANGED: return "MISSING F CHANGED"; break;
      case MISSINGFN: return "MISSING FUNCTIONS INIT"; break;
      case MISSINGSPEED: return "MISSING SPEED"; break;
      case MISSINGFCOUNT: return "MISSING FUNCTION COUNT"; break;
      case UNKWNOWEXCEPTION: return "UNKWNOW EXCEPTION"; break;
      case MULTIPLELCLIST: return "MULTIPLE ERRORS PARSING LCLIST"; break;
      case LOCONOTFOUND: return "LOCO NOT FOUND IN LOCO DEPOT"; break;
      case UNKWNOWCMD: return "COMMAND NOT AVAILABLE"; break;
      case PROGMODEMMNOTSUPPORTED: return "PROGRAM MM LOCOS IS NOT AVAILABLE"; break;
      case PROGMODEACCMMNOTSUPPORTED: return "PROGRAM MM ACC IS NOT AVAILABLE"; break;
      case PROGMODEMMXNOTSUPPORTED: return "PROGRAM MMX LOCO IS NOT AVAILABLE"; break;
      case MAXSERIALTRYS: return "NOT POSSIBLE TO SEND COMMAND. MAX SERIAL TRYES"; break;
      case SERIALTIMEOUT: return "NOT POSSIBLE TO SEND COMMAND. RESPONSE TIMEOUT"; break;
      case RPICDGATEWAYERROR: return "NOT POSSIBLE TO SEND COMMAND. GATEWAY ERROR"; break;
      case MMXDISCOVERYMODENA: return "LOK DISCOVERY MODE NOT AVAILABLE"; break;
      case INDEKNOTKNOWN: return "INDEX FOR SYS_DATA NOT KNOWN"; break;
      default: return "UNKNOWN ERROR";
	}

}
