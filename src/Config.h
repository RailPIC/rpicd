/*
 * Config.h
 *
 *  Created on: 19/2/2015
 *      Author: mserrano
 */

#ifndef CONFIG_H_
#define CONFIG_H_

//Variables that are hard-coded
#define VERSION "0.5"
#define CONFIGFILE "rpicd.xml"
#define PIDPATH "rpicd.pid"

//Variables that can be changed at the configFILE
#define LOGPATH "rpicd.log"
//#define WEBPORT "8010"
#define WEBPORT 8010
#define RPISERIAL "/dev/ttyAMA0"
#define MAXSERIALTRY 3
#define USECSERIALTRY 100000
//#define RPIBAUD 115200
//#define RESETPICPIN 6
#define RESETOFFPIN 0
//#define ACTION1PIN 5
//#define ACTION1ACTION ""
//#define ACTION1MESSAGE "Rocview"
//#define ACTION2PIN 4
//#define ACTION2ACTION ""
//#define ACTION2MESSAGE "Keyboard"
#define LEDREADYRESTPIN 3
#define LEDSTEADYPIN 2
#define PROGTRACKPIN 11
#define SEC4HALT 4
//#define MS4RESETPIC 300
#define SEC4RESETPIC 2

#define S88ACTIVE false
#define S88DATAPIN 24
#define	S88LOADPIN 23
#define S88CLKPIN 22
#define S88RESETPIN 21

#define ROCCLIENTIP "127.0.0.1"
#define ROCCLIENTPORT 8051

#define MAXCLIENTS 1
#define ROCNETPORT 4321
#define MCS2PORTrx 15731
#define MCS2PORTtx MCS2PORTrx-1
//#define MCS2PORTtx 15730
#define ECOSPORT 15471
#define LOCONETPORT 1235


#include <stdio.h>
#include "parsers/CommandParser.h"
#include <memory>

class Config {

public:

	Config();
	virtual ~Config();
	shared_ptr<CommandParser> read(string confFile);
	//static CommandParser read();
	void compare();
	void printLocoPlan();
private:

};

#endif /* CONFIG_H_ */
