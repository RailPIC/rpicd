/*
 * Loco.cpp
 *
 *  Created on: 08/07/2015
 *      Author: agonia
 */

#include "Loco.h"
#include "Logger.h"
#include "RpicdGateway.h"
#include "rpic/commands.h"

#include <unistd.h>

Loco::Loco() {
	// TODO Auto-generated constructor stub

}

Loco::Loco(string newID, unsigned char UID[4]){

	ID=newID;
	for (int i=0;i<4;i++) mmxUID[i]=UID[i];
	next=nullptr;
	speedcurve=nullptr;
}

Loco::Loco(string newID, speedstep *newspeedcurve){
	ID=newID;
	address=0;
	protocol=EMPTY;				//Loco not valid withoud a protocol

	speed=0;
	dirforward=true;
	speedcnt=0;
	protfamily=0;
	for (int i=0;i<(MAXFUNCTIONS);i++) f[i]=false;
	speedcurve=newspeedcurve;
	next=nullptr;
	autopilot=false;
	tspeed=0;
	tdir=true;
}

Loco::Loco(string newID, int newaddress, int newprotocol, int spcnt, bool functions[MAXFUNCTIONS]){
	ID=newID;
	address=newaddress;
	protocol=newprotocol;

	speed=0;
	dirforward=true;
	speedcnt=spcnt;
	for (int i=0;i<(MAXFUNCTIONS);i++) f[i]=functions[i];
	speedcurve=nullptr;
	next=nullptr;
	autopilot=false;
	tspeed=0;
	tdir=true;

	if (protocol<LOCODCCN14){
		protfamily=LOCMMCMD;
	}else if (protocol<LOCOMMX_F4){
		protfamily=LOCNMRACMD;
	}else{
		protfamily=LOCMMXCMD;
	}
}

Loco::~Loco() {
	// TODO Auto-generated destructor stub
}

void Loco::setSpeedCurve(speedstep *newspeedcurve){
	speedcurve=newspeedcurve;
}

string Loco::getID(){
	return ID;
}

int Loco::getAddress(){
	return address;
}
void Loco::setAddress(int addr){
	address=addr;
}

int Loco::getProtocol(){
	return protocol;
}

void Loco::setProtocol(int prot){
	protocol=prot;
	if (protocol<LOCODCCN14){
		protfamily=LOCMMCMD;
	}else if (protocol<LOCOMMX_F4){
		protfamily=LOCNMRACMD;
	}else{
		protfamily=LOCMMXCMD;
	}
}

int Loco::getProtocolFamily(){
	return protfamily;
}
void Loco::setProtocolFamily(int protf){
	protfamily=protf;
}


speedstep *Loco::getSpeedCurve(){
	return speedcurve;
}

void Loco::setF(int function, bool value){
	f[function]=value;
}

bool Loco::getF(int function){
	return f[function];
}

void Loco::setSpeed(int sp){
	speed=sp;
}

int Loco::getSpeed(){
	return speed;
}

int Loco::getTSpeed(){
	return tspeed;
}

bool Loco::getDirForward(){
	return dirforward;
}

bool Loco::getTDir(){
	return tdir;
}

int Loco::getSpeedCount(){
	return speedcnt;
}
void Loco::setSpeedCount(int spcnt){
	speedcnt=spcnt;
}

void Loco::setmmxFnbytes(int Fnbytes){
	mmxFnbytes=Fnbytes;
}

int Loco::getmmxFnbytes(){
	return mmxFnbytes;
}

void Loco::setmmxUID(unsigned char UID[4]){

	for (int i=0;i<4;i++) mmxUID[i]=UID[i];

}

unsigned char * Loco::getmmxUID(){

	return mmxUID;

}

int Loco::nextStepSleep(int step){
	//Current implementation of xml speedcurve is symetric (equal curve forward and reverese). To look for the steps, look for abs
	//step=abs(step);
	speedstep *finder;
	finder=speedcurve;

	while (finder){
		if (finder->step==step) return finder->speedup;
		else finder=finder->next;
	}
	return 0;
}
int Loco::previousStepSleep(int step){
	//Current implementation of xml speedcurve is symetric (equal curve forward and reverese). To look for the steps, look for abs
	//step=abs(step);
	speedstep *finder;
	finder=speedcurve;

	while (finder){
		if (finder->step==step) return finder->speedown;
		else finder=finder->next;
	}
	return 0;
}


void Loco::updateDriver(int target, bool dir){
	unsigned int response;
	int oldspeed;
	bool olddir;
	tspeed=target;
	tdir=dir;
	if (speedcurve){
		//launch thread... si no existe ya.
		//tspeed=target;
		//tdir=dir;
		if (!(autopilot)){
			Logger::insertLog("LOCODRIVER: Launching SpeedCurve autopilot for "+ID);
			//destroy(driver);

			autopilot=true;
			driver= std::thread(&Loco::speedDriver, this);
			driver.detach();
		}
	}else{
//		if (target!=speed){ //speed and change direction produces always 3 commands: direction, speed, fn... ONly speed or direction is affected... not both... no need to send two packages speed also contains dir	
	
		oldspeed=speed;
		olddir=dirforward;
		speed=target;
		dirforward=dir;
		//if (protocol<LOCODCCN14) RpicdGateway::sendspeedcommand(this,255);
		//else RpicdGateway::sendspeedcommand(this);
		
		
		response=RpicdGateway::sendspeedcommand(this);
	//	printf("No speed %d, dir %d\n",speed,dirforward);

		if (response!=OK) {
			Logger::insertLog("LOCODRIVER: Failed setting speed command for  "+ID);
			speed=oldspeed;
			dirforward=olddir;
		}
//	}
	}

}

void Loco::speedDriver(){
	//printf("pilot \n");
	//Current implementation of xml speedcurve is symetric (equal curve forward and reverese). To look for the steps, look for abs
	unsigned int response;

	int cspeed=abs(speed);
	int sfinal;
	//	bool dirfinal;
	bool doubleMessage=false;

	//if change dir  some decoders need a double message... this is governed by sleepp[0] a parameter never used... if it is !=0 we force a new message
	if ((previousStepSleep(0))&&(tdir!=dirforward)) doubleMessage=true;

	bool cycle=true;
	//at least one message needs to be sent when changdir
	int delay=0;

	if ((tdir!=dirforward)&&(cspeed!=0)){ //in speed=0 and change direction no need to force to go to v=0
		sfinal=0;     //if there is a change of direction, we force first to go to v=0;
		// dirfinal=dirforward; //dirforward is current dir
	}else{
		sfinal=abs(tspeed); //otherwise the final speed is the one received from the command;
		//dirfinal=tdir;
		dirforward=tdir;	//update already dirforward (for case cspeed=0;
	}

	while (cycle){
		if (sfinal>cspeed){    //if we want to accelerate
			delay= nextStepSleep(cspeed);
			cspeed++; //and, we prepare to send the next step;
		}else if (sfinal<cspeed) {//if we want to deccelerate
			delay= previousStepSleep(cspeed);
			cspeed--;            //and we prepare to send the previous step;
			if (cspeed==0){dirforward=tdir;} //when we arrive to speed =0 we recover the right direction, needed in case of a changedir
		}else{ //otherwise, sfinal==cspeed happens in a change of direction. It could be the intermediate v=0 (forced by the speedcurve) or the final step
			//sfinal==cspeed happens also if we force the repeated message.
			//sfinal=abs(tspeed);     //we update to the real final speed..it also helps if n udate happens during update

			if ((tdir!=dirforward)&&(cspeed!=0)){ //in speed=0 and change direction no need to force to go to v=0
				sfinal=0;     //if there is a change of direction, we force first to go to v=0;
				// dirfinal=dirforward; //dirforward is current dir
			}else{
				sfinal=abs(tspeed); //otherwise the final speed is the one received from the command;
				//dirfinal=tdir;
			//	dirforward=tdir;	//update already dirforward (for case cspeed=0;
			}


			if ((tspeed!=0)&&(sfinal==0)){     //if we are in a change direction from v!=0 (from v==0 no need to do nothing)
				//sfinal=abs(tspeed);     //we update to the real final speed
				delay= nextStepSleep(cspeed);
				cspeed++;
			}
		}

		if ((delay!=0)||(cspeed==sfinal)){         //Only messages that do not require a sleep... this is something that could be configured by sleepn(vmax)..a parameter never used
			//printf("Delay %d\n",delay);

			usleep(delay*1000);                 //Or the final message.

			//update speed to the right value before sending message....
			if (dirforward) speed=cspeed;
			else speed=cspeed*(-1);
		//	if (protocol<LOCODCCN14) RpicdGateway::sendspeedcommand(this,255);
		//	else RpicdGateway::sendspeedcommand(this);
			response=RpicdGateway::sendspeedcommand(this);
//			printf("Speed %d, dir %d\n",speed,dirforward);
			if (response!=OK) Logger::insertLog("LOCODRIVER: ERROR during speed curve command for  "+ID+". Inconsistent seped");
			else Logger::insertLog("LOCODRIVER: ID "+ ID+ " delay "+to_string(delay)+" speed "+to_string(speed)+" dir "+to_string(dirforward));
		}
		delay=0;
		if ((speed==tspeed) && (doubleMessage)){
			delay=previousStepSleep(0);     //if we have (sleepp[0]!=0) we request to duplicate the last message with a sleep=sleepp[0]
		}
		if (speed==tspeed){
			cycle=false;
			if (doubleMessage){
				doubleMessage=false;                       //We change it to get out the cycle next time
				cycle=true;
			}
		}
	}
	Logger::insertLog("LOCODRIVER: Ending SpeedCurve autopilot for "+ID);
	autopilot=false;
}
