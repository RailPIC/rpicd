/*
 * serialPort.h
 *
 *  Created on: 17/04/2016
 *      Author: agonia
 */

/* Based on
 *
http://softwaresouls.com/softwaresouls/2013/07/29/programming-serial-port-communications/
http://softwaresouls.com/softwaresouls/2012/03/05/linux-c-dynamixel-reading-and-writing-example/
http://linux.die.net/man/2/write
http://linux.die.net/man/2/read
http://man7.org/linux/man-pages/man3/tcsetattr.3.html

http://stackoverflow.com/questions/9609781/porting-ioctl-calls-from-unix-to-linux-error-with-fionbio

http://stackoverflow.com/questions/4184468/sleep-for-milliseconds
 */

#ifndef SERIALPORT_H_
#define SERIALPORT_H_

#include <stdio.h>
#include <stdlib.h>

#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <cstring>
#include <sys/ioctl.h>

using namespace std;

class SerialPort {
private:
int fileDescriptor;

public:
virtual ~SerialPort();
SerialPort();

int connect (const char * device);
void disconnect(void);

int sendBuffer(unsigned char *buffer, int len);
int getBuffer (unsigned char *buffer, int len);

int bytesToRead();
void clear();
};

#endif /* SERIALPORT_H_ */
