/*
 * S88.cpp
 *
 *  Created on: 17/05/2018
 *      Author: agonia
 */

#include "S88.h"
#include "Config.h"

extern "C" {
//#ifdef __arm__
#if defined(__arm__) || defined(__aarch64__)
#include <wiringPi.h>
//#include <wiringSerial.h>
#else
#include "tools/fakewiringPi.h"
#endif

}

S88::S88() {
	// TODO Auto-generated constructor stub

}

S88::~S88() {
	// TODO Auto-generated destructor stub
}

bool S88::active=S88ACTIVE;
int S88::datapin=S88DATAPIN;
int S88::clkpin=S88CLKPIN;
int S88::loadpin=S88LOADPIN;
int S88::resetpin=S88RESETPIN;

void S88::init(){

	pinMode(S88::clkpin, OUTPUT);
	digitalWrite(S88::clkpin, 0);
	pinMode(S88::loadpin, OUTPUT);
	digitalWrite(S88::loadpin, 0);
	pinMode(S88::resetpin, OUTPUT);
	digitalWrite(S88::resetpin, 0);
	if (S88::active){
		pinMode(S88::datapin, INPUT);
	}else{
		pinMode(S88::datapin, OUTPUT);
		digitalWrite(S88::datapin, 0);
	}
}
