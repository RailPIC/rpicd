/*
 * Webber.h
 *
 *  Created on: 10/05/2016
 *      Author: agonia
 */

#ifndef WEBBER_H_
#define WEBBER_H_

#define MAXMESSAGEBUFFER  20

extern "C" {
#include "web/mongoose.h"
}

#include "parsers/mcs2Parser.h"

/*struct device_settings {
  char setting1[100];
  char setting2[100];
};
*/

class Webber {
private:
//	static const char *s_http_port;
	static struct mg_serve_http_opts s_http_server_opts;
	static sig_atomic_t s_signal_received;
	static mcs2Parser *mcs24MMX;
	static struct mg_connection *nc;
	static bool init;
	static bool delayed;
	static unsigned char bufferindex;
	static string messages[MAXMESSAGEBUFFER];


	//static void handle_save(struct mg_connection *nc, struct http_message *hm);
	static void handle_lok_discovery(struct mg_connection *nc, struct http_message *hm);
	static void handle_cycleON(struct mg_connection *nc, struct http_message *hm);
	static void handle_cycleOFF(struct mg_connection *nc, struct http_message *hm);
	static void handle_testON(struct mg_connection *nc, struct http_message *hm);
	static void handle_testOFF(struct mg_connection *nc, struct http_message *hm);
	static void handle_PTON(struct mg_connection *nc, struct http_message *hm);
	static void handle_PTOFF(struct mg_connection *nc, struct http_message *hm);
	static void handle_RESET(struct mg_connection *nc, struct http_message *hm);
	static void handle_VERSION(struct mg_connection *nc, struct http_message *hm);
	static void handle_UC(struct mg_connection *nc, struct http_message *hm);
	static void handle_mmxLOAD(struct mg_connection *nc, struct http_message *hm);
	static void handle_mmxBIND(struct mg_connection *nc, struct http_message *hm);
	static void handle_mmxPING(struct mg_connection *nc, struct http_message *hm);
	static void handle_getUID(struct mg_connection *nc, struct http_message *hm);
	static void handle_setUID(struct mg_connection *nc, struct http_message *hm);
	static void handle_getNS(struct mg_connection *nc, struct http_message *hm);
	static void handle_setNS(struct mg_connection *nc, struct http_message *hm);
	static void handle_getEnable(struct mg_connection *nc, struct http_message *hm);
	static void handle_setEnable(struct mg_connection *nc, struct http_message *hm);
	static void handle_getConfig(struct mg_connection *nc, struct http_message *hm);
	static void handle_setConfig(struct mg_connection *nc, struct http_message *hm);
	static void handle_pingLOGON(struct mg_connection *nc, struct http_message *hm);
	static void handle_pingLOGOFF(struct mg_connection *nc, struct http_message *hm);
	static void handle_statusLOGON(struct mg_connection *nc, struct http_message *hm);
	static void handle_statusLOGOFF(struct mg_connection *nc, struct http_message *hm);
	static void handle_clockLOGON(struct mg_connection *nc, struct http_message *hm);
	static void handle_clockLOGOFF(struct mg_connection *nc, struct http_message *hm);
	static void handle_LOKSYNC(struct mg_connection *nc, struct http_message *hm);
	static void handle_CARSYNC(struct mg_connection *nc, struct http_message *hm);


	static void ev_handler(struct mg_connection *nc, int ev, void *ev_data);

	//websocket
	static void signal_handler(int sig_num);
	static int is_websocket(const struct mg_connection *nc);
	static void broadcast(struct mg_connection *nc, const struct mg_str msg);

public:
	Webber();
	virtual ~Webber();
	static void inserWebLog(string message);
	//static const char *s_http_port;
	static int s_http_port;
};

#endif /* WEBBER_H_ */
