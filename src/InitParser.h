/*
 * InitParser.h
 *
 *  Created on: 10/2/2015
 *      Author: mserrano
 */

#ifndef INITPARSER_H_
#define INITPARSER_H_

#include "parsers/CommandParser.h"
#include <memory>
#include <string>

using namespace std;

class InitParser {

public:
    static shared_ptr<CommandParser> CreateInstance(string type);
};



#endif /* INITPARSER_H_ */
