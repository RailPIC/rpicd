#! /bin/sh

CC=gcc
CCX=g++
RM=rm -f
CFLAGS=-c -Wall -std=c++0x
LDLIBS=-lwiringPi -lpthread

all: rpicd

rpicd: SerialPort.o tinystr.o tinyxml.o tinyxmlerror.o tinyxmlparser.o CommandParser.o RocNetParser.o mcs2Parser.o loconetParser.o EcosParser.o Config.o errors.o mongoose.o Webber.o InitParser.o S88.o Loco.o LocoDepot.o Logger.o RocClient.o Rpi.o RpicdGateway.o rpicd.o
	$(CXX) SerialPort.o tinystr.o tinyxml.o tinyxmlerror.o tinyxmlparser.o CommandParser.o RocNetParser.o mcs2Parser.o loconetParser.o EcosParser.o Config.o errors.o mongoose.o Webber.o InitParser.o S88.o Loco.o LocoDepot.o Logger.o RocClient.o Rpi.o RpicdGateway.o rpicd.o -o rpicd $(LDLIBS)

# MAIN

rpicd.o: src/rpicd.cpp
	$(CCX) $(CFLAGS) src/rpicd.cpp

# XML TOOLS

tinystr.o: src/xml/tinystr.cpp
	$(CCX) $(CFLAGS) src/xml/tinystr.cpp

tinyxml.o: src/xml/tinyxml.cpp
	$(CCX) $(CFLAGS) src/xml/tinyxml.cpp

tinyxmlerror.o: src/xml/tinyxmlerror.cpp
	$(CCX) $(CFLAGS) src/xml/tinyxmlerror.cpp

tinyxmlparser.o: src/xml/tinyxmlparser.cpp
	$(CCX) $(CFLAGS) src/xml/tinyxmlparser.cpp

# COMMAND PARSERS

CommandParser.o: src/parsers/CommandParser.cpp 
	$(CCX) $(CFLAGS) src/parsers/CommandParser.cpp

RocNetParser.o: src/parsers/RocNetParser.cpp
	$(CCX) $(CFLAGS) src/parsers/RocNetParser.cpp

mcs2Parser.o: src/parsers/mcs2Parser.cpp
	$(CCX) $(CFLAGS) src/parsers/mcs2Parser.cpp

loconetParser.o: src/parsers/loconetParser.cpp
	$(CCX) $(CFLAGS) src/parsers/loconetParser.cpp

EcosParser.o: src/parsers/EcosParser.cpp
	$(CCX) $(CFLAGS) src/parsers/EcosParser.cpp

InitParser.o: src/InitParser.cpp
	$(CCX) $(CFLAGS) src/InitParser.cpp

S88.o: src/S88.cpp
	$(CCX) $(CFLAGS) src/S88.cpp

# CORE

SerialPort.o: src/SerialPort.cpp
	$(CCX) $(CFLAGS) src/SerialPort.cpp

Rpi.o: src/Rpi.cpp
	$(CCX) $(CFLAGS) src/Rpi.cpp

RpicdGateway.o: src/RpicdGateway.cpp
	$(CCX) $(CFLAGS) src/RpicdGateway.cpp

Loco.o: src/Loco.cpp
	$(CCX) $(CFLAGS) src/Loco.cpp

LocoDepot.o: src/LocoDepot.cpp
	$(CCX) $(CFLAGS) src/LocoDepot.cpp

RocClient.o: src/RocClient.cpp
	$(CCX) $(CFLAGS) src/RocClient.cpp

# SUPPORT CONFIG AND LOG		

Config.o: src/Config.cpp
	$(CCX) $(CFLAGS) src/Config.cpp

#RpiConfig.o: src/RpiConfig.cpp
#	$(CCX) $(CFLAGS) src/RpiConfig.cpp

Logger.o: src/Logger.cpp
	$(CCX) $(CFLAGS) src/Logger.cpp
 
errors.o: src/errors.c
	$(CC) $(CFLAGS) src/errors.c

mongoose.o: src/web/mongoose.c
	$(CC) $(CFLAGS) src/web/mongoose.c

Webber.o: src/Webber.cpp
	$(CCX) $(CFLAGS) src/Webber.cpp

# CLEAN

clean:
	rm *.o rpicd
