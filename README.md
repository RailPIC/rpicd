# RAILPIC PROJECT #

|                       |                                                      |
|:----------------------|:-----------------------------------------------------|
|**Project Owner(s):**  |Manuel Serrano                                        |
|**Author(s):**         |Manuel Serrano                                        |
|**Hardware Platform:** |ODROID, RASPBERRY PI                                  |
|**Debuggers Used:**    |GCC, ECLIPSE                                          |
|**Programmers Used:**  |NA                                                    |
|**MPLAB Version:**     |NA                                                    |
|**C Compiler Version:**|gcc>4.8.4                                             |

**For more information:** [RailPic Homepage](http://tren.enmicasa.net/railpic)  

I recommend to check the home page for examples of configuration files.

********************************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

********************************************************************************

RAILPIC is a project to embed the control of model railways in a PIC ucontroller
connected to an arm device..

Currently the project provides:


*   Support for MM1 and MM2 protocol
*	Support for NMRA (long and short addresses) – 14,28 and 128 steps
*	Support for MMX (MM4) (not yet reading responses, but with the UID of 
the loco, it is possible to register the loco and send commands to it.)
*	Support for NMRA accessories (no extended accessories so far)
*	Support for writing CV.

The definition of the MM protocol was obtained from:

- ["The Manual of the new Märklin Motorola Format"](http://spazioinwind.libero.it/scorzoni/motorola.htm), 
Andrea Scorzoni, 1995-2000.  

The definition of the MFX protocol was obtained from:
- ["Das mfx-Schienenformat"](http://www.skrauss.de/modellbahn/mdigital.html),
Stefan Krauß, Version 1 2009-02-01.  
- [Rainer Mueller web site](http://www.alice-dsl.net/mue473/mfxrahmen.html).

********************************************************************************

The rpicd software is the part of the solution that translates the commands 
received from a control software to the protocol used by
RPIC (the embedded software in a PIC generating the signals to the track).
It emulates a MCS2 station.


## FOLDERS INCLUDED IN THE PROJECT TREE ##

- daemon: service definition to run rpicd as dameon (original purpose).  
- src: source code  
- web_root: Files for the web Interface  
