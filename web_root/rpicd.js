/*
 *LOAD mmx 
 * 
 */

jQuery(function() {
	$(window)
			.on(
					"load",
					function() {
						$
								.ajax({
									url : '/mmxLOAD',
									method : 'POST',
									dataType : 'json',
/*									data : {
										n1 : $('#n1').val(),
										n2 : $('#n2').val()
									},*/
									success : function(json) {
										var div = document
												.querySelector("#mmxCONT"), frag = document
												.createDocumentFragment(), select = document
												.createElement("select");

										var inAddress = document
												.createElement("input");
										var textAddress = document
												.createTextNode("A: ");
										var bindButton = document
												.createElement("input");
										bindButton.type = "button";
										bindButton.className="button";
										bindButton.value = "BIND";
										var pingButton = document
												.createElement("input");
										pingButton.type = "button";
										pingButton.className="button";
										pingButton.value = "PING";
										inAddress.setAttribute("maxlength", 5);
										inAddress.setAttribute("size", 4);
										if (json.locos.length > 0) {
											for (i = 0; i < json.locos.length; i++) {
												select.options
														.add(new Option(
																json.locos[i].ID
																		+ " ("
																		+ json.locos[i].UID
																		+ ")",
																json.locos[i].ID));
											}
											select.onchange = function() {
												inAddress.value = json.locos[select.selectedIndex].A;
											};
											inAddress.value = json.locos[select.selectedIndex].A;
											var dataBUTTON = {
												ID : '',
												A : 0
											};
											bindButton.onclick = function() {
												dataBUTTON.ID = json.locos[select.selectedIndex].ID;
												dataBUTTON.A = inAddress.value;
												$
														.ajax({
															url : '/mmxBIND',
															method : 'POST',
															dataType : 'json',
															data : dataBUTTON,
															success : function(
																	result) {
																if (result.result == "OK") {
																	json.locos[select.selectedIndex].A = inAddress.value;
																} else {
																	alert("ERROR binding loco");
																}
															}
														});
											}
											
											pingButton.onclick = function() {
												dataBUTTON.ID = json.locos[select.selectedIndex].ID;
												dataBUTTON.A = inAddress.value;
												$
														.ajax({
															url : '/mmxPING',
															method : 'POST',
															dataType : 'json',
															data : dataBUTTON,
															success : function(
																	result) {
																if (result.result == "OK") {
																	//json.locos[select.selectedIndex].A = inAddress.value;
																} else {
																	alert("ERROR pinging loco");
																}
															}
														});
											}
											
											frag.appendChild(select);
											frag.appendChild(textAddress);
											frag.appendChild(inAddress);
											frag.appendChild(bindButton);
											frag.appendChild(pingButton);
											div.appendChild(frag);
										}
									}
								});
					});
});

/*
 * BUTTONS
 * 
 */
// DISCOVERY

jQuery(function() {

	$(document).on('click', '#discovery_button', function() {
		$.ajax({
			url : '/lok_discovery',
			method : 'POST',
			dataType : 'html'
		});
		return false;
	});
});

//Power

jQuery(function() {
	$(document).on('click', '#power', function() {
		if (($("#power").is(':checked'))) powerstatus='/cycleON';
		else powerstatus='/cycleOFF';
		$.ajax({
			url : powerstatus,
			method : 'POST',
			dataType : 'html'
		});
		//return false;
	});
});

//PT
jQuery(function() {
	$(document).on('click', '#PT', function() {
		if (($("#PT").is(':checked'))) powerstatus='/PTON';
		else powerstatus='/PTOFF';
		$.ajax({
			url : powerstatus,
			method : 'POST',
			dataType : 'html'
		});
		//return false;
	});
});

//test
jQuery(function() {
	$(document).on('click', '#test', function() {
		if (($("#test").is(':checked'))) powerstatus='/testON';
		else powerstatus='/testOFF';
		$.ajax({
			url : powerstatus,
			method : 'POST',
			dataType : 'html'
		});
		//return false;
	});
});


// RESET
jQuery(function() {
	$(document).on('click', '#RESET', function() {
		$.ajax({
			url : '/RESET',
			method : 'POST',
			dataType : 'html'
		});
		return false;
	});
});
// VERSION
jQuery(function() {

	$(document).on('click', '#VERSION', function() {
		$.ajax({
			url : '/VERSION',
			method : 'POST',
			dataType : 'json',
			success : function(result) {
				document.getElementById("verTEXT").textContent=result.ver;
			}
		});
//		return false;
	});
});

//UC
jQuery(function() {

	$(document).on('click', '#UC', function() {
		$.ajax({
			url : '/UC',
			method : 'POST',
			dataType : 'json',
			success : function(result) {
				if (result.uc==25){
					document.getElementById("ucTEXT").textContent="16F1825";
				}else{
					document.getElementById("ucTEXT").textContent="16F18326";
				}
			}
		});
//		return false;
	});
});

//GETUID
jQuery(function() {

	$(document).on('click', '#getUID', function() {
		$.ajax({
			url : '/getUID',
			method : 'POST',
			dataType : 'json',
			success : function(result) {
				var inUID = document.getElementById("inUID");
				inUID.value=result.UID;
			}
		});
//		return false;
	});
});

//SETUID
jQuery(function() {

	$(document).on('click', '#setUID', function() {
		var dataBUTTON = {
				UID : ''
			};
		var inUID = document.getElementById("inUID");
		dataBUTTON.UID=inUID.value;
		$.ajax({
			url : '/setUID',
			method : 'POST',
			dataType : 'json',
			data: dataBUTTON,
			success : function(result) {
				if (result.result != "OK") {
					alert("ERROR Setting Central UID");
				}
			}
		});
//		return false;
	});
});

//GETNS
jQuery(function() {

	$(document).on('click', '#getNS', function() {
		$.ajax({
			url : '/getNS',
			method : 'POST',
			dataType : 'json',
			success : function(result) {
				var inNS = document.getElementById("inNS");
				inNS.value=result.NS;
			}
		});
//		return false;
	});
});

//SETNS
jQuery(function() {

	$(document).on('click', '#setNS', function() {
		var dataBUTTON = {
				NS : 0
			};
		var inNS = document.getElementById("inNS");
		dataBUTTON.NS=inNS.value;
		$.ajax({
			url : '/setNS',
			method : 'POST',
			dataType : 'json',
			data: dataBUTTON,
			success : function(result) {
				if (result.result != "OK") {
					alert("ERROR Setting NS");
				}
			}
		});
//		return false;
	});
});

//GETENABLE
jQuery(function() {

	$(document).on('click', '#getEnable', function() {
		$.ajax({
			url : '/getEnable',
			method : 'POST',
			dataType : 'json',
			success : function(result) {
				var inEnable = document.getElementById("inEnable");
				inEnable.value=result.Enable;
			}
		});
//		return false;
	});
});

//SETEnable
jQuery(function() {

	$(document).on('click', '#setEnable', function() {
		var dataBUTTON = {
				Enable : 0
			};
		var inEnable = document.getElementById("inEnable");
		dataBUTTON.Enable=inEnable.value;
		$.ajax({
			url : '/setEnable',
			method : 'POST',
			dataType : 'json',
			data: dataBUTTON,
			success : function(result) {
				if (result.result != "OK") {
					alert("ERROR Setting Enable");
				}
			}
		});
//		return false;
	});
});

//GETCONFIG
jQuery(function() {

	$(document).on('click', '#getConfig', function() {
		$.ajax({
			url : '/getConfig',
			method : 'POST',
			dataType : 'json',
			success : function(result) {
				var inConfig = document.getElementById("inConfig");
				inConfig.value=result.Config;
			}
		});
//		return false;
	});
});

//SETConfig
jQuery(function() {

	$(document).on('click', '#setConfig', function() {
		var dataBUTTON = {
				Config : 0
			};
		var inConfig = document.getElementById("inConfig");
		dataBUTTON.Config=inConfig.value;
		$.ajax({
			url : '/setConfig',
			method : 'POST',
			dataType : 'json',
			data: dataBUTTON,
			success : function(result) {
				if (result.result != "OK") {
					alert("ERROR Setting Config");
				}
			}
		});
//		return false;
	});
});

//PING LOG
jQuery(function() {
	$(document).on('click', '#pingLOG', function() {
		if (($("#pingLOG").is(':checked'))) pinglog='/pingLOGON';
		else pinglog='/pingLOGOFF';
		$.ajax({
			url : pinglog,
			method : 'POST',
			dataType : 'html'
		});
		//return false;
	});
});

//STATUSLOG
jQuery(function() {
	$(document).on('click', '#statusLOG', function() {
		if (($("#statusLOG").is(':checked'))) statuslog='/statusLOGON';
		else statuslog='/statusLOGOFF';
		$.ajax({
			url : statuslog,
			method : 'POST',
			dataType : 'html'
		});
		//return false;
	});
});

//CLOCKLOG
jQuery(function() {
	$(document).on('click', '#clockLOG', function() {
		if (($("#clockLOG").is(':checked'))) clocklog='/clockLOGON';
		else clocklog='/clockLOGOFF';
		$.ajax({
			url : clocklog,
			method : 'POST',
			dataType : 'html'
		});
		//return false;
	});
});
/*
//LOKSYNC
jQuery(function() {
	$(document).on('click', '#LOKSYNC', function() {
		$.ajax({
			url : '/LOKSYNC',
			method : 'POST',
			dataType : 'html'
		});
		return false;
	});
})

//CARSYNC
jQuery(function() {
	$(document).on('click', '#CARSYNC', function() {
		$.ajax({
			url : '/CARSYNC',
			method : 'POST',
			dataType : 'html'
		});
		return false;
	});
});;
*/

/*
 * Init UID, NS and VERSION
 * 
 */

$(document).ready(function(){
	$("#getUID").trigger('click');
	$("#getNS").trigger('click');
	$("#getEnable").trigger('click');
	$("#getConfig").trigger('click');
//	$("#verTEXT").textContent="Hello World";
//	document.getElementById("verTEXT").textContent="Hello World";
	$("#VERSION").trigger('click');
	$("#UC").trigger('click');
	
});



/*
 * CONSOLE
 * 
 */

var ws = new WebSocket('ws://' + location.host + '/ws');

if (!window.console) {
	window.console = {
		log : function() {
		}
	}
};

ws.onopen = function(ev) {
	console.log(ev);
};
ws.onerror = function(ev) {
	console.log(ev);
};
ws.onclose = function(ev) {
	console.log(ev);
};
ws.onmessage = function(ev) {
	console.log(ev);
	var div = document.createElement('div');
	div.innerHTML = ev.data;
	var elem = document.getElementById('messages');
	elem.appendChild(div);
	elem.scrollTop = elem.scrollHeight;
};